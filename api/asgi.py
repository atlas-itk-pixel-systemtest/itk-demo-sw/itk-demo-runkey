from itk_demo_runkey.app import create_app
from fastapi.responses import RedirectResponse

app = create_app()

@app.get("/", include_in_schema=False)
async def index():
    return RedirectResponse("/api/docs")