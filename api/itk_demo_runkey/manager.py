from fastapi import APIRouter
from fastapi.responses import Response
import asyncio
from pyconfigdb import ConfigDB
from pyconfigdb.exceptions import ConfigDBResponseError
from itk_demo_runkey.RcfError import RcfError
from itk_demo_runkey.config import config, celery_url
import requests
from itk_demo_runkey.node_configuration import NodeConfiguration
from itk_demo_runkey.async_http import AIOClient as AsyncClient
from itk_demo_runkey.state import RunkeyManagerState
from itk_demo_runkey.event_bus import EventBus
from fastapi import WebSocket, WebSocketException
from pydantic import BaseModel

from runstats import Statistics
import timeit

from itk_demo_runkey.config import log

class SetBody(BaseModel):
    name: str = "latest"

bus = EventBus()
state = RunkeyManagerState(bus)

key = config.configdb_url_key
configdb = ConfigDB(key, config.sr_url + "/")

router = APIRouter(prefix="/api")


@router.websocket("/ws")
async def websocket(websocket: WebSocket):
    await websocket.accept()
    i = 0
    await websocket.send_json(
        {"id": i, "event": "init", "data": state.json_serialize()}
    )
    i += 1

    async def event_listener(event: dict):
        nonlocal i
        event["id"] = i
        await websocket.send_json(event)
        i += 1

    listener_id = bus.subscribe("ws_broadcast", event_listener)

    try:
        async for event in websocket.iter_json():
            if event["event"] == "set_runkey":
                await set_active_runkey(event["data"]["runkey"])
            i += 1
    except WebSocketException:
        websocket.close()
    log.info("Websocket closed")
    bus.unsubscribe("ws_broadcast", listener_id)


@router.get("/health/", summary="check if runkey manager is available")
async def health():
    return Response(status_code=200)


@router.post(
    "/set/",
    tags=["set"],
    summary="set the runkey",
    description="Set the active runkey, that should be used for configuration",
)
async def set_active_runkey(body: SetBody):
    await _set_active_runkey(body.name)
    # cProfile.runctx("_set_active_runkey(name)", globals(), locals(), filename="runkey_manager.prof")


async def _set_active_runkey(name: str = "latest"):
    global state

    if state.is_configuring():
        return RcfError(
            409,
            "Already appliying configuration",
            f"Allready appliying configurations from runkey {state.active_runkey}. Please wait.",
        ).to_response()

    log.info(f"Setting runkey to {name}")
    try:
        requests.get(f"{celery_url}/health").raise_for_status()
    except requests.exceptions.HTTPError:
        log.error(f"Error: Celery middleware not available at {celery_url}")
        return RcfError(
            503, "Celery middleware not available", "Celery middleware not available"
        ).to_response()

    log.debug("Reading runkey from configdb...")
    start = timeit.default_timer()
    try:
        tree = configdb.tag_tree(name, False)
    except ConfigDBResponseError as e:
        log.error(f"Error: {e}")
        return RcfError(404, "Runkey not found", f"{e}").to_response()
    end = timeit.default_timer()
    read_runkey_time = end - start
    log.debug(f"Read runkey in {read_runkey_time * 1000:.2f}ms\nSearching nodes...")

    nodes = tree["objects"]
    services = {}
    while len(nodes) > 0:
        node = nodes.pop()
        metadata = node.get("metadata", {})
        config_endpoint = metadata.get("configEndpoint", {})
        service_key = config_endpoint.get("key", None)
        endpoint = config_endpoint.get("endpoint", "config")
        method = config_endpoint.get("method", "post")
        if service_key:
            services[node["id"]] = (service_key, endpoint, method)
        nodes.extend(node.get("children", []))
    nodes_time = timeit.default_timer()
    walk_runkey_time = nodes_time - end
    log.debug(f"Found {len(services)} nodes in {walk_runkey_time*1000:.2f}ms")

    await state.configure(runkey=name, services=services.keys())
    stats = {
        "request_time_stats": Statistics(),
        "celery_time_stats": Statistics(),
        "group_restored_time_stats": Statistics(),
        "configuration_time_stats": Statistics(),
        "result_time_stats": Statistics(),
        "total_time_stats": Statistics(),
    }

    start_time = timeit.default_timer()
    async with AsyncClient() as session:
        log.debug("Creating statemachines for nodes...")
        start = timeit.default_timer()
        tasks = []
        for component, (service_key, endpoint, method) in services.items():
            # log.info(f"Configuring {service_key} with {component}")
            node = NodeConfiguration(
                service_key, endpoint, method, component, session, bus, stats
            )
            tasks.append(node.request())
        end = timeit.default_timer()
        log.debug(
            f"Created Statemachines in {(end -start)*1000:.2f}ms. Starting configuration..."
        )
        await asyncio.gather(*tasks)
    end_time = timeit.default_timer()

    performance = f"""Read runkey from ConfigDb in {1000*read_runkey_time:.2f}ms
Walke the runkey in {1000*walk_runkey_time:.2f}ms
Finished configuring {len(services)} services in {1000*(end_time - start_time):.2f}ms
{state.successfull_count} services were configured successfully
{state.error_count} services encountered an error
=> Error rate = {state.error_count / (state.error_count + state.successfull_count)}
Mean times per service
| Requesting task from Middleware: ({stats["request_time_stats"].mean()*1000:.2f} pm {stats["request_time_stats"].stddev()*1000:.2f})ms
| Celery Middleware enqueuing task: ({stats["celery_time_stats"].mean()*1000:.2f} pm {stats["celery_time_stats"].stddev()*1000:.2f})ms
| Loading Celery Group Result: ({stats["group_restored_time_stats"].mean()*1000:.2f} pm {stats["group_restored_time_stats"].stddev()*1000:.2f})ms
| Loading Object from Configdb: ({stats["configuration_time_stats"].mean()*1000:.2f} pm {stats["configuration_time_stats"].stddev()*1000:.2f})ms
| Waiting for Result to finish: ({stats["result_time_stats"].mean()*1000:.2f} pm {stats["result_time_stats"].stddev()*1000:.2f})ms
| Total configuration time: ({stats["total_time_stats"].mean()*1000:.2f} pm {stats["total_time_stats"].stddev()*1000:.2f})ms
"""
    log.info(performance)
    try:
        with open("/performance/stats.txt", "+a") as f:
            f.write(performance)
    except Exception as e:
        log.error("While writing performance stats:\n" + e)

    if state.state != "idle_configured":
        log.error(f"Error configuring runkey {name}: {state.state=}")
        return RcfError(
            503, "Error applying configuration", "Error applying configuration"
        ).to_response()

    # with Pool(config.worker_threads) as p:
    #     p.starmap(print, [("test", 1)])


@router.get(
    "/get/",
    tags=["get"],
    summary="get the runkey",
    description="Returns the active runkey",
)
async def get():
    return state.active_runkey
