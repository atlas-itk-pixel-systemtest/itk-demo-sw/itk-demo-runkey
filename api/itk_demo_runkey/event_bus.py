from collections import defaultdict

# from itk_demo_runkey.config import log

class EventBus:
  id = 0

  def __init__(self):
    self._subscribers = defaultdict(dict)
    self.id = 0

  def subscribe(self, event_type: str, callback) -> int:
    id = self.id 
    self._subscribers[event_type][id] = callback
    self.id += 1
    return id
  
  def unsubscribe(self, event_type: str, id: int):
    self._subscribers[event_type].pop(id, None)

  async def _acallbacks(self, event_type: str):
    for callback in self._subscribers[event_type].values():
      yield callback
    
  
  async def emit(self, event_type: str, **kwargs):
    async for callback in self._acallbacks(event_type):
      await callback(**kwargs)