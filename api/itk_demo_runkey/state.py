from transitions.extensions import HierarchicalAsyncMachine
from transitions import EventData
from itk_demo_runkey.event_bus import EventBus

from itk_demo_runkey.config import log

class RunkeyManagerState:
    states = [
        "initial",
        {"name": "configuring", "children": ["working", "error"], "initial": "working"},
        {"name": "idle", "children": ["configured", "error"], "on_enter": "finished_configuring"},
    ]
    transitions = [
        {"trigger": "configure", "source": ["initial", "idle"], "dest": "configuring", "before": "init_configuring"},
        {"trigger": "error", "source": "configuring", "dest": "configuring_error", "after": "finish_error"},
        {"trigger": "finish", "source":"configuring_working", "dest":"idle_configured", "conditions": "all_children_finished", "prepare": "child_finished"},
        {"trigger": "finish", "source":"configuring_error", "dest":"idle_error", "conditions": "all_children_finished", "prepare": "child_finished"},
    ]

    active_runkey = None
    # dictionary[service_id] = finished: bool | error
    services = {}
    successfull_count = 0
    error_count = 0

    bus: EventBus

    def __init__(self, bus: EventBus):
        self.machine = HierarchicalAsyncMachine(
            model=self,
            states=RunkeyManagerState.states,
            transitions=RunkeyManagerState.transitions,
            initial="initial",
            send_event=True,
            queued=True,
            on_exception="handle_error",
            after_state_change="state_changed"
        )
        bus.subscribe("error", self.error)
        bus.subscribe("finish", self.finish)
        self.bus = bus

    async def state_changed(self, event: EventData):
        await self.bus.emit("ws_broadcast", event={"event": "state_changed", "data": self.serialize_state()})

    async def init_configuring(self, event: EventData):
        self.successfull_count = 0
        self.error_count = 0
        self.active_runkey = event.kwargs["runkey"]
        self.services = {}
        for service in event.kwargs["services"]:
            self.services[service] = False
        if len(self.services) == 0:
            await self.to_idle_configured()
        await self.bus.emit("ws_broadcast", event={"event": "init", "data": self.json_serialize()})

    def finished_configuring(self, event: EventData):
        log.info(f"Finished configuring runkey {self.active_runkey}")

    async def finish_error(self, event: EventData):
        node_id = event.kwargs.get("node_id", None)
        if node_id:
            await self.finish(**event.kwargs)

    def handle_error(self, event):
        log.error(f"Error: {event.error}")
        # self.error()
    
    def all_children_finished(self, event):
        waiting_for_services = not bool([v for _, v in self.services.items() if not v])
        return waiting_for_services
    
    async def child_finished(self, event: EventData):
        node_id = event.kwargs["node_id"]
        service_id = event.kwargs["service_id"]
        error_message = event.kwargs.get("error_message", None)
        log.info(f"Node {service_id} finished with status {event.transition.dest.split('_')[1]}")
        self.services[node_id] = error_message if error_message else True
        data = {'success': error_message is None, 'node': node_id}
        if error_message is not None:
            log.error(f"Node {service_id} finished with error: {error_message}")
            data['error'] = error_message
        log.info(f"Data: {data}")
        if data['success']:
            self.successfull_count += 1
        else:
            self.error_count += 1
        await self.bus.emit("ws_broadcast", event={"event": "node_finished", "data": data})
        # if config.demo_mode:
        #     import time
        #     time.sleep(1)

    def serialize_state(self):
        state = ""
        if self.state == "initial":
            state = "unconfigured"
        elif self.state == "configuring_working":
            state = "configuring"
        elif self.state == "idle_configured":
            state = "configured"
        elif self.state == "idle_error":
            state = "configured_error"
        else:
            state = self.state
        return state 

    def json_serialize(self):
        return {
            "state": self.serialize_state(),
            "runkey": self.active_runkey,
            "services": self.services,
        }
