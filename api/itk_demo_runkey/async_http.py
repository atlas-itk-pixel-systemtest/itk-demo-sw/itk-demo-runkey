import aiohttp

import timeit

import aiohttp.client_proto


class Response:
    def __init__(self):
        pass

    def raise_for_status(self):
        pass

    async def json(self):
        pass

class ClientSession:
    def __init__(self):
        pass

    async def post(self, url, json) -> Response:
        pass

    async def get(self, url) -> Response:
        pass

    async def __aenter__(self):
        pass

    async def __aexit__(self, *args, **kwargs):
        pass
# class TimedResponseHandler(aiohttp.client_proto.ResponseHandler):
#     def connection_made(self, transport):
#         AIOClient.connection_made_time = timeit.default_timer()
#         return super().connection_made(transport)

# class TimedTCPConnector(aiohttp.TCPConnector):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         import functools
#         self._factory = functools.partial(TimedResponseHandler, loop=loop)

#     async def _create_connection(self, req):
#         nonlocal connection_started_time
#         connection_started_time = time()
#         return await super()._create_connection(req)

# async with aiohttp.ClientSession(connector=TimedTCPConnector(loop=loop)) as session:
#     async with session.get('https://www.google.com') as response:
#         await response.text()
#     print(f'>>> time taken including connection time: {time() - connection_started_time:0.3f}s')
#     print(f'>>> time taken once connection made: {time() - connection_made_time:0.3f}s')

class AIOClient(ClientSession):
    def __init__(self, *args, **kwargs):
        self.session = aiohttp.ClientSession(connector=aiohttp.TCPConnector(limit=200), *args, **kwargs)

    async def post(self, url, json):
        response = self.session.post(url, json=json)
        response = await response.__aenter__()
        return AIOResponse(response)

    async def get(self, url):
        async with self.session.get(url) as response:
            return AIOResponse(response)

    async def __aenter__(self):
        self.session = await self.session.__aenter__()
        return self

    async def __aexit__(self, *args, **kwargs):
        return await self.session.__aexit__(*args, **kwargs)
class PerformanceAIOClient(AIOClient):
    async def post(self, url, json):
        start_time = timeit.default_timer()
        response = self.session.post(url, json=json)
        # import asyncio
        # loop = asyncio.get_running_loop()
        # loop.time()
        # response = loop.run_until_complete(response.__aenter__())
        response = await response.__aenter__()
        end_time = timeit.default_timer()
        return PerformanceAIOResponse(end_time-start_time, response)
        



class AIOResponse(Response):
    def __init__(self, response: aiohttp.ClientResponse):
        self.response = response

    def raise_for_status(self):
        try:
            self.response.raise_for_status()
        except aiohttp.ClientResponseError as e:
            raise AIOResponseError(e)

    async def json(self):
        return await self.response.json()

    async def __aenter__(self):
        return self
    async def __aexit__(self, *args, **kwargs):
        return await self.response.__aexit__(*args, **kwargs)
class PerformanceAIOResponse(AIOResponse):
    def __init__(self, time: float, response: aiohttp.ClientResponse):
        super().__init__(response)
        self.time = time
    async def __aenter__(self):
        return self
    async def __aexit__(self, *args, **kwargs):
        return await self.response.__aexit__(*args, **kwargs)


class ResponseError(Exception):
    def __init__(self):
        pass


class AIOResponseError(ResponseError):
    def __init__(self, error: aiohttp.ClientResponseError):
        self.error = error
