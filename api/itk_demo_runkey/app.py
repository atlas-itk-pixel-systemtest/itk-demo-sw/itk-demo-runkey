from itk_demo_runkey.manager import router
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware


def create_app():
    app = FastAPI(
        title="Runkey Manager API",
        description="HTTP api to the runkey manager",
        version="1.0.0",
        docs_url="/api/docs",  # custom location for swagger UI
    )
    app.include_router(router)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],  # Allows all origins
        allow_credentials=True,
        allow_methods=["*"],  # Allows all methods
        allow_headers=["*"],  # Allows all headers
    )

    return app