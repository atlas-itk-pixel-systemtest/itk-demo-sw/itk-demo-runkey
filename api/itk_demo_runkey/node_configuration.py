from transitions import EventData
from transitions.extensions.asyncio import AsyncMachine
from itk_demo_runkey.config import celery_url
from itk_demo_runkey.async_http import ClientSession, ResponseError
from itk_demo_runkey.event_bus import EventBus
from itk_demo_runkey.celery import app
from itk_demo_runkey.RcfError import RcfError
import asyncio
import json

import timeit

from itk_demo_runkey.config import log

class NodeConfiguration(object):
    states = [
        "unconfigured",
        {"name": "requesting", "on_enter": "request_task"},
        "configuring",
        {"name": "configured", "final": True},
        {"name": "error", "final": True},
    ]
    transitions = [
        {
            "trigger": "request",
            "source": "unconfigured",
            "dest": "requesting",
        },
        ["await_response", "requesting", "configuring"],
        ["error", "*", "error"],
        ["finish", "configuring", "configured"],
    ]

    def __init__(
        self, service_key: str, endpoint: str, method: str, component: str, session: ClientSession, bus: EventBus, stats: dict
    ):
        assert method == "get" or method == "post", "valid methods are 'get' and 'post'"
        self.service_key = service_key
        self.endpoint = endpoint
        self.method = method
        self.component = component
        self.async_client = session
        self.bus = bus
        self.error_message = None
        self.stats = stats

        self.machine = AsyncMachine(
            model=self,
            states=NodeConfiguration.states,
            transitions=NodeConfiguration.transitions,
            initial="unconfigured",
            on_exception="handle_error",
            on_final="terminate",
            send_event=True,
            queued=True,
        )

    async def request_task(self, event: EventData):
        start_time = timeit.default_timer() 
        celery_task = {
            "type": self.method,
            "path": self.endpoint,
            "key": self.service_key,
        }
        if self.method == "post":
            celery_task["body"] = {"tree": self.component}

        request_body = {
            "tasks": [celery_task],
        }
        # log.info(f"{self.service_key} requesting task: {request_body}")
        init_time = timeit.default_timer()
        async with await self.async_client.post(
            f"{celery_url}/tasks", json=request_body
        ) as response:
            requested_time = timeit.default_timer()
            await self.await_response()
            try:
                response.raise_for_status()
            except ResponseError as e:
                raise Exception(f"Celery error: {e}")
            prev=response
            response = await prev.json()
            # log.error(prev.response._body)
            # log.error(response)
            self.task_id = response.get("tasks")
            celery_time = response.get("total_time")
            # log.info(f"{self.service_key} received task_id: {self.task_id}")
        before_group_restore_time = timeit.default_timer()
        group_result = app.GroupResult.restore(self.task_id)
        result = group_result.children[0]
        group_restored_time = timeit.default_timer()
        async def inf():
            while True:
                yield
        async for _ in inf(): 
            if result.ready():
                break
            await asyncio.sleep(0)
        # print(result)
        result = result.get(interval=0.05)
        # print(result)
        if not isinstance(result, dict):
            try:
                result = json.loads(result)
            except json.JSONDecodeError:
                log.error(f"Celerey result from {self.task_id} is not json: '{result}'")
                result = {}
        status = result.get("status_code", 500)
        response = result.get("response", "Task finished with no response")
        if not isinstance(response, dict):
            try:
                response = json.loads(response)
            except json.JSONDecodeError:
                log.error(f"Response from {self.service_key} is not json: '{response}'")
                response = {"message": response}
        configuration_time = response.get("total_time", 0.0)
        end_time = timeit.default_timer()
        # log.info(f"{self.service_key} received result: {result}")
        self.stats["request_time_stats"].push(requested_time - init_time)
        self.stats["celery_time_stats"].push(celery_time)
        self.stats["group_restored_time_stats"].push(group_restored_time - before_group_restore_time)
        self.stats["configuration_time_stats"].push(configuration_time)
        self.stats["result_time_stats"].push(end_time - group_restored_time)
        self.stats["total_time_stats"].push(end_time - start_time)
        if status // 100 == 2:
            log.info(f"{self.service_key} finished successfully:\n{response}")
            await self.finish()
        else:
            raise RcfError(status, "Error applying configuration", response)

    async def handle_error(self, event: EventData):
        log.error(f"{self.service_key} encountered an\nError: {event.error}")
        self.error_message = f"{event.error}"
        await self.error()

    async def terminate(self, event: EventData):
        log.info(f"Terminating {self.service_key} in state {self.state}")
        if self.state == "error":
            await self.bus.emit("error", node_id=self.component, service_id=self.service_key, error_message=self.error_message)
        elif self.state == "configured":
            await self.bus.emit("finish", node_id=self.component, service_id=self.service_key)
