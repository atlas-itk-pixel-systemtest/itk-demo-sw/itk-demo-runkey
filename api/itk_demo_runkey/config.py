from config_checker import BaseConfig, EnvSource
from pyregistry.registry import ServiceRegistry
from pyregistry.exceptions import SRResponseError, SRConnectionError

import logging
from rich.logging import RichHandler

class RunkeyManager_Config(BaseConfig):
  sr_url: str = "http://localhost:5111/api"
  configdb_url_key: str = "demi/default/itk-demo-configdb/api/url"
  celery_url_key: str = "demi/default/topology-server/api/url"
  log_level: str = logging._levelToName[logging.INFO]
  worker_threads: int = 4
  # performance_output_path: str = "/performance/stats.txt"
  
  CONFIG_SOURCES = [EnvSource(allow_all=True, file=".env")]

config = RunkeyManager_Config()
valid_log_level = True
try:
  log_level = logging._nameToLevel[config.log_level]
except KeyError:
  original_log_level = config.log_level
  valid_log_level = False
  log_level = logging.INFO

handler = RichHandler(level=log_level, show_time=False, omit_repeated_times=False)
handler.setFormatter(logging.Formatter("%(asctime)s\n%(message)s"))
log = logging.Logger("rich", log_level)
log.addHandler(handler)

if not valid_log_level:
  log.error(f"{original_log_level} is not a valid log level. Use one of {logging._levelToName.keys()}")

sr = ServiceRegistry(config.sr_url)
try:
  celery_url = sr.get(config.celery_url_key)
except SRConnectionError as e:
  log.error(f"Service registry not found\nError: {e}")
  raise e
except SRResponseError as e:
  log.error(f"Celery url not found\nError: {e}")
  raise e