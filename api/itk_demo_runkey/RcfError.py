from rcf_response.exceptions import RcfException 
from rcf_response import Error
from fastapi.responses import JSONResponse

class RcfError(Exception):
    def __init__(
        self,
        status,
        title,
        msg,
        status_id = 0,
        type = "about:blank",
        instance = None,
        ext = None
    ):
        self.error = Error.from_exception(RcfException(
            status,
            title,
            msg,
            status_id,
            type,
            instance,
            ext
        ))

    def to_response(self):
        response, status = self.error.to_conn_response()
        return JSONResponse(content=response, status_code=status)
    
    def __str__(self):
        return f"Error: {self.error}"
    
    def __repr__(self):
        return f"RcfError({self.error})"