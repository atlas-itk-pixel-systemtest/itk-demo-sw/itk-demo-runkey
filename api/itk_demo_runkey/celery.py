from celery import Celery
import os

def is_docker():
    path = "/proc/self/cgroup"
    return (
        os.path.exists("/.dockerenv")
        or os.path.isfile(path)
        and any("docker" in line for line in open(path))
    )


if is_docker():
    # print ("Running in Docker")

    broker = "pyamqp://guest:guest@rabbitmq"
    backend = "redis://redis:6379/0"
    # backend="rpc://"

else:
    broker = "pyamqp://guest:guest@localhost"
    backend = "redis://localhost:6379/0"


app = Celery('server', backend=backend, broker=broker)