# Changelog

All notable changes[^1] to the `itk-demo-runkey`

This repo is "living at head" which means tags correspond to the overall repo.
All subrepos/workspaces, all packages in the package registry and all container images in the image registry follow the repo tag.

[^1]: The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.2.4] - 2024-03-06
- UI: Can pass type parameter, when cloning, commiting or creating a new runkey

## [3.2.3] - 2024-11-25
- Added histogram viewing capability
  - Added jsroot container
- Removed whitespaces in UI to show more info on one screen

## [3.2.0] - 2024-11-25
- Added scalable example
- Made awaiting configuration results asynchronous

## [3.1.1] - 2024-09-30
- bug fixes and example clean up

## [3.1.0] - 2024-09-30
- runkey api server working as runkey manager: 
  - configure the system with a single runkey
- runkey manager tab in ui:
  - whatch the configuration updates in real time
- an example to see everything in action

## [3.0.5] - 2024-09-25
  - ci for api server

## [3.0.4] - 2024-09-25
  - fixed a bug where the runkey ui would not find the right configdb url

## [3.0.3] - 2024-09-11
  - fixed error messages for tags without `objects`
  - empty modals focus the confirm button
  - handle failed requests from deleting a runkey correctly

## [3.0.2] - 2024-09-11
  - scroll tree view separate from the site
  - made help in topology editor expandable
  - mount ui configs from `${DOCKER_DIR}/mod_configs/` and populate it with default files if none are given

## [3.0.1] - 2024-08-21
  - adjusted size of fields in single tag tables and added author fields
  
## [3.0.0] - 2024-08-14
  - updated to new configdb endpoints

## [2.6.4] - 2024-07-08
  - fixed performance issues in bulk editor with large lists

## [2.6.3] - 2024-07-08
  - support payload/read changes of configdb 1.11.3

## [2.6.2] - 2024-07-03
  - added support for lists and nested object-list-constructs in bulk editor
  - added a checkbox to update payloads instead of copying them on change in tree editor

## [2.6.1] - 2024-06-26
  - bulk editor only shows parts of the tree, that include selected payload type
  - fixed a bug in bulk editor where payloads where still selected, when switching payload type
  - added option to add fields to payloads in bulk editor

## [2.6.0] - 2024-06-24
  - switched order of payloads and components in tree view
  - fixed a bug where runkey names with upper case characters would open an tab to an invalid tag
  - added bulk payload editor

## [2.5.3] - 2024-06-10
  - fixed bug where cloning and commiting runkeys would not close the pop up

## [2.5.2] - 2024-06-05
  - left align components in toplogy editor, if the graph gets split
  - success messages will automatically disappear

## [2.5.1] - 2024-06-04
  - added panel title to topology editor panel

## [2.5.0] - 2024-05-28
  - added topology editor to adjust relations between components more easily.

## [2.4.0] - 2024-04-15
  - First documented release of the runkey-ui
