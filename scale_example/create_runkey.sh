#!/usr/bin/env bash

source ./config
rm -f runkeymanager/.env
cat .env >> runkeymanager/.env
source .env
HOSTS=()
DUMMY_COUNTS=()
while getopts "h:d:" opt; do
    case $opt in
        h ) HOSTS+=("$OPTARG");;
        d ) DUMMY_COUNTS+=("$OPTARG");;
        * ) echo Wrong argument.;exit 1 ;;
    esac
done
shift $((OPTIND-1))
echo ${HOSTS[@]}
echo ${DUMMY_COUNTS[@]}

WORKING_DIR=${WORKING_DIR-"runkeymanager/runkey"}
RUNKEY_NAME=${RUNKEY_NAME-"decentralization"}

amount_hosts=${#HOSTS[@]}
mkdir -p $WORKING_DIR/$RUNKEY_NAME
cur_dir=$WORKING_DIR/$RUNKEY_NAME

felix_id=1
opto_id=0
fe_id=0
for i in $(seq 1 $amount_hosts); do
host=${HOSTS[$i-1]}
dummy_count=${DUMMY_COUNTS[$i-1]}
export HOST=$host DUMMY_COUNT=$dummy_count
source ./create_compose.sh
for d in $(seq 1 $dummy_count); do
if [ $opto_id -eq 0 ]; then
    cur_dir+=/felix$felix_id
    mkdir -p $cur_dir
    cat <<-EOF >$cur_dir/felix_meta.json
{
	"configEndpoint": {
		"key": "demi/$host/itk-demo-dummy-$d/api/url",
		"method": "post",
		"endpoint": "configure"
	}
}    
EOF
    opto_id=$(($opto_id + 1))
elif [ $fe_id -eq 0 ]; then
    cur_dir+=/opto$opto_id
    mkdir -p $cur_dir
    cat <<-EOF >$cur_dir/opto_meta.json
{
	"configEndpoint": {
		"key": "demi/$host/itk-demo-dummy-$d/api/url",
		"method": "post",
		"endpoint": "configure"
	}
}    
EOF
    fe_id=$(($fe_id + 1))
else
    cur_dir+=/fe$fe_id
    mkdir -p $cur_dir
    cat <<-EOF >$cur_dir/fe_meta.json
{
	"configEndpoint": {
		"key": "demi/$host/itk-demo-dummy-$d/api/url",
		"method": "post",
		"endpoint": "configure"
	}
}    
EOF
    fe_id=$(($fe_id + 1))
    cur_dir=$WORKING_DIR/$RUNKEY_NAME/felix$felix_id/opto$opto_id
fi
if [ $fe_id -gt 2 ]; then
    fe_id=0
    opto_id=$(($opto_id + 1))
    cur_dir=$WORKING_DIR/$RUNKEY_NAME/felix$felix_id
fi
if [ $opto_id -gt 2 ]; then
    opto_id=0
    felix_id=$(($felix_id + 1))
    cur_dir=$WORKING_DIR/$RUNKEY_NAME
fi
done
done