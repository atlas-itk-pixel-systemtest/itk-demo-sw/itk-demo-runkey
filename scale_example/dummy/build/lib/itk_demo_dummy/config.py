from config_checker import BaseConfig, EnvSource

import logging
from rich.logging import RichHandler

logging.basicConfig(level=logging.INFO, format="%(message)s", datefmt=" ", handlers=[RichHandler()])
log = logging.getLogger("rich")

class DummyConfig(BaseConfig):
  sr_url: str = "http://localhost:5111/api"
  configdb_url_key: str = "demi.default.itk-demo-configdb.api.url"
  
  CONFIG_SOURCES = [EnvSource(allow_all=True, file=".env")]

config = DummyConfig()