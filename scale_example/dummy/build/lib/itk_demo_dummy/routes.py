from fastapi import APIRouter, HTTPException
from fastapi.responses import HTMLResponse
from pydantic import BaseModel
from pyconfigdb import ConfigDB
from itk_demo_dummy.config import config
from random import randint
from time import sleep

import logging
from rich.logging import RichHandler

logging.basicConfig(
    level=logging.INFO, format="%(message)s", datefmt=" ", handlers=[RichHandler()]
)
log = logging.getLogger("rich")


class ConfigureBody(BaseModel):
    tree: str


configdb = ConfigDB(config.configdb_url_key, config.sr_url)

router = APIRouter(prefix="/api")
dump_router = APIRouter()


@router.post(
    "/configure/",
    summary="configure the dummy service",
    description="load a tree from the configdb and wait for a random amount of time before returning. May randomly fail.",
    response_class=HTMLResponse,
)
async def dump(configuration: ConfigureBody):
    depth = randint(1, 3)
    tree = configdb.object_tree(configuration.tree, False, depth, True)
    print(tree.id)
    sleep_timer = randint(300, 1500) / 1000
    sleep(sleep_timer)
    error = randint(1, 100)
    if error == 100:
        raise HTTPException(500, "Unexpected vacation. Server will be right back.")
    return


@router.get("/health/", summary="check if dummy is available")
async def health():
    return
