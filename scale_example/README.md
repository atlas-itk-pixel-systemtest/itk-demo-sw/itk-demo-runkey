# Scale Example
This directory provides some scripts to easily create a scalable runkey manager example.

## Prerequisite
The dummy container is not officially released for now, so it is necessary to firstly build it (assuming pwd==scale_example):
```
cd dummy && docker compose build
cd ..
```

## Usage
### Setup
The main script is the `create_runkey.sh` script. It can be called with multiple `-h [HOST]` and `-d [DUMMY_COUNT]` flags. The number of hosts and dummy counts must be the same, because these flags will be paired up according to their position, so the first `-h` will be paired with the first `-d`. The pairs then describe all the hosts and the amount of dummy services to start there. Example:
```
create_runkey.sh -h host1 -d 10 -h host2 -d 20
```
if an .env file should be preserved
```
source .env && create_runkey.sh -h...
```

The script uses this information to create a runkey of the form:
- felix0
    - opto0
        - fe0
        - fe1
    - opto1
- felix1   
    - opto0
        - fe0
        - fe1
    - opto1
- felix2 ...

Furthermore the script runs `HOST=HOSTS[i] DUMMY_COUNT=DUMMY_COUNT[i] create_compose.sh` for every pair to generate a compose file with for that host with the correct amount of dummies. The containers will connect to the runkey manager stack on start up, so it is necessary to start it first. To start the dummy containers, copy the compose file onto the correct host and make sure the docker network deminet exist on that host:
```
docker network create deminet
```
and run the file with docker compose on that host:
```
docker compose -f [HOST]_compose.yaml up -d
```

Lastly the script assumes, that the system it is called on will also be the system running the runkey manager. For this purpose the `config` script is called. This generates a `.env` file with some configurations needed to run the runkey manager stack. The `STACK` and `SR_URL` variables are also used in the creation of the compose files to reference the services of the runkey manager stack.

### Starting everything automagically
The provided `docker.sh` script will build the dummy container, manage the runkey manager stack if the `HOST` is matching the one in the `runkeymanager/.env` file and manage the dummy services and the registrator.
```
HOST=[Prefix to '_compose.yaml' for dummies] ./docker.sh ['up' | 'down']
```
`up` and `down` correspond to the `docker compose` comands.

To run it on another host via ssh
```
ssh itkpixdaq@[OtherHost] "cd [path to ]/itk-demo-runkey/scale_test && HOST=[OtherHost] ./docker.sh ['up' | 'down']"
```

### Starting the Runkey Manager manually
Ideally run the whole runkey manager stack on the same maschine on which you called the `create_runkey.sh` script, otherwise you will have to find out yourself how to configure the communication correctly.
Make sure the `.env` file is in the `runkeymanager` directory and start the `runkeymanager/compose.yaml`:
```
docker compose -f runkeymanager/compose.yaml up -d 
```
