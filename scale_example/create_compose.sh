#!/usr/bin/env bash

HOST=${HOST}
STACK=${STACK}
SR_URL=${SR_URL}

DUMMY_COUNT=${DUMMY_COUNT-1}
rm -f ${HOST}_compose.yaml
touch ${HOST}_compose.yaml
echo "services:" >> ${HOST}_compose.yaml
for i in $(seq 1 $DUMMY_COUNT); do
  cat <<-EOF >> ${HOST}_compose.yaml
  dummy-service$i:
    image: gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-dummy-service/api:latest
    command: uvicorn asgi:app --port 5000 --host 0.0.0.0
    environment:
      - SR_URL=\${SR_URL:-$SR_URL}
      - CONFIGDB_URL_KEY=\${CONFIGDB_URL_KEY:-demi/$STACK/itk-demo-configdb/api/url}
    ports:
      - :5000
    labels:
      - demi.$HOST.itk-demo-dummy-$i.api.host=$HOST
      - demi.$HOST.itk-demo-dummy-$i.api.category=Microservice
      - demi.$HOST.itk-demo-dummy-$i.api.url_prefix=/api
      - demi.$HOST.itk-demo-dummy-$i.api.url_postfix=/docs
      - demi.$HOST.itk-demo-dummy-$i.api.health=/health
      - demi.$HOST.itk-demo-dummy-$i.api.description=simple dummy service to test access the configdb
      - demi.$HOST.itk-demo-dummy-$i.api.protocol=http
      - demi.$HOST.itk-demo-dummy-$i.api.api_name=dummy-service $i
EOF
done

cat <<-EOF >> ${HOST}_compose.yaml
  registrator:
    image: gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-registrator:latest
    command: python3 itk_demo_registrator/registrator.py
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    extra_hosts:
      - "host.docker.internal:host-gateway"
      - "$HOST:host-gateway"
    environment:
      - SR_URL=\${SR_URL:-$SR_URL}
networks:
  default:
    name: deminet
    external: true
EOF

docker network create deminet >/dev/null 2>&1 || true
