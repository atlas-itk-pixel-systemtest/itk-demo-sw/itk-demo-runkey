const fs = require('node:fs')

console.log(process.env.npm_lifecycle_event)
if (process.env.npm_lifecycle_event === 'prebuild') {
fs.readFile('./src/index.jsx', 'utf8', (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  const cleaned = data.substring(0, data.indexOf('// #START_LOCAL')) + data.substring(data.indexOf('// #END_LOCAL') + '// #END_LOCAL\n'.length)
  fs.writeFile('./src/index.jsx', cleaned, 'utf8', (err) => {
    if (err) {
      console.error(err)
      return
    }
    console.log('Cleanup complete')
  })
})
}
