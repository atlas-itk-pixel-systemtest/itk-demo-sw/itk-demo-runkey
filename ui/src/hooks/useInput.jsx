import { useState } from 'react'

/**
 * @typedef InputTypes = 'string' | 'boolean'
 * @typedef InputValue = {name: string, type?: InputTypes, placeHolder?: string | boolean, isRequired?: bool}
 *
 * @param {InputValue[]} inputValues
 * @returns [getInputValue: (name) -> value, setInputValue: (name, value), resetInputValues: (), inputValueState: {name: value}]
 */
const useInput = (inputValues = {}) => {
  const values = () => inputValues.map((v, i) => {
    if (!('name' in v)) {
      throw new Error(`Unnamed inputValue. Expected a 'name' field in every inputValue, but found none in ${v}, at position ${i}`)
    }

    const value = { isRequired: true, ...v }

    if (!('type' in v)) {
      value.type = 'string'
    }

    const defaultPlaceHolders = {
      text: '',
      string: '',
      boolean: false,
      list: [],
      select: []
    }

    const place = value.placeHolder
    const type = value.type || 'string'
    if (place !== undefined) {
      switch (type) {
        case 'text':
        case 'string':
          if (typeof place !== 'string') {
            throw new Error(`Mismatched types. Expected a 'string' as 'placeHolder' but encountered '${place}' with type '${typeof place}' in ${v}, at position ${i}`)
          }
          break
        case 'boolean':
          if (typeof place !== 'boolean') {
            throw new Error(`Mismatched types. Expected a 'boolean' as 'placeHolder' but encountered '${place}' with type '${typeof place}' in ${v}, at position ${i}`)
          }
          break
        case 'select':
        case 'list':
          if (!Array.isArray(place)) {
            throw new Error(`Mismatched types. Expected a 'array' as 'placeHolder' but encountered '${place}' with type '${typeof place}' in ${v}, at position ${i}`)
          }
          break
      }
    } else {
      value.placeHolder = defaultPlaceHolders[type]
    }
    if (type === 'select' && value.onSelect === undefined) {
      value.onSelect = () => { }
    }
    return value
  }).reduce((obj, v) => {
    obj[v.name] = v
    return obj
  }, {})

  const [inputValueState, setInputValues] = useState(values())

  const resetInputValues = () => setInputValues(values())

  const setInputValue = (name, value) => {
    setInputValues((values) => {
      const v = { ...values }
      v[name].placeHolder = value
      return v
    })
  }

  const getInputValue = (name) => {
    if (name === 'dumpEverythingYouCan') {
      return inputValueState
    }
    return inputValueState[name].placeHolder
  }

  return [getInputValue, setInputValue, resetInputValues, inputValueState]
}

export default useInput
