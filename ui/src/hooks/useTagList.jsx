import { useState, useEffect } from 'react'
import { generalRequest } from '@itk-demo-sw/utility-functions'

const useTagList = (
  dbApiUrl,
  onError = (data) => { },
  onGenericError = (data) => { },
  updateState = (url) => { },
  stagingArea = true,
  author = false
) => {
  const [tagList, setTagList] = useState([])

  const reqBody = {
    table: 'tag',
    stage: stagingArea,
    order_by: 'time',
    asc: false
  }

  const updateTag = (author = false) => {
    if (dbApiUrl === '/api') { return }
    const query = new URLSearchParams(reqBody).toString()
    generalRequest(
      `${dbApiUrl}/read_all?${query}`, { httpMethod: 'GET' }
    ).then(
      data => {
        const newTagList = []
        for (const [, dataset] of data.list.entries()) {
          let timeString = dataset.time || '1971-12-08T00:00:00.892772Z'
          if (!timeString.endsWith('Z')) {
            timeString += 'Z'
          }
          const time = new Date(timeString)
          const tag = [
            dataset.name,
            dataset.type,
            `${time.toLocaleTimeString().substring(0, 5)}  ${time.toLocaleDateString()}`,
            dataset.comment || ''
          ]
          if (author) {
            tag.push(dataset.author || '')
          }
          newTagList.push(tag)
        }
        setTagList(newTagList)
      }
    ).catch(
      err => {
        if (err.detail === undefined) {
          onGenericError('JavaScript-Error: ' + err.name, err.message, err.message)
        } else {
          onError(err)
        }
      }
    )
  }

  useEffect(() => {
    updateTag(author)
  }, [dbApiUrl])

  return [tagList, updateTag]
}

export default useTagList
