import { generalRequest } from '@itk-demo-sw/utility-functions'

const useRunkeyWriter = (
  dbApiUrl,
  // type,
  onError = (data) => { },
  onGenericError = (data) => { },
  updateState = (url) => { },
  updateTagList = () => { }
) => {
  const createNewRunkey = async (name, comment, author, type) => {
    let success = false
    const state = await updateState(dbApiUrl)
    const reqBody = {
      name,
      payloads: [],
      comment,
      author,
      type,
      data: {
        type: 'root'
      }
    }
    if (state === 'Active') {
      try {
        await generalRequest(`${dbApiUrl}/stage`, { body: reqBody })
        updateTagList()
        success = true
      } catch (err) {
        if (err.detail === undefined) {
          onGenericError('JavaScript-Error: ' + err.name, err.message, err.message)
        } else {
          onError(err)
        }
        success = false
      }
    }
    return success
  }

  const deleteRunkey = async (name) => {
    const reqBody = {
      identifier: name
    }
    const query = new URLSearchParams(reqBody).toString()
    try {
      return await generalRequest(`${dbApiUrl}/stage?${query}`, { httpMethod: 'DELETE' })
    } catch (err) {
      if (err.detail === undefined) {
        onGenericError('JavaScript-Error: ' + err.name, err.message, err.message)
      } else {
        onError(err)
      }
    }
  }

  const cloneRunkey = (identifier, name, comment, author, type) => {
    const reqBody = {
      identifier,
      name,
      comment,
      author,
      type
    }
    return generalRequest(`${dbApiUrl}/stage/clone`, { body: reqBody })
  }

  const commitRunkey = (identifier, name, author, comment, type) => {
    const reqBody = {
      author,
      identifier,
      name,
      comment,
      type
    }
    return generalRequest(`${dbApiUrl}/stage/commit`, { body: reqBody })
  }

  const addObjectToRunkey = (type, parents, children = [], payloads = []) => {
    const reqBody = {
      children: children.map(child => { return { id: child } }),
      parents: parents.map(parent => { return { id: parent } }),
      payloads,
      type
    }
    return generalRequest(`${dbApiUrl}/node?stage=true`, { body: reqBody })
  }

  const addConnectionsToObject = (parentId, children) => {
    const reqBody = {
      children: children.map(child => { return { id: child } }),
      payloads: []
    }
    const query = new URLSearchParams({ stage: true, id: parentId }).toString()
    return generalRequest(`${dbApiUrl}/on_node?${query}`, { body: reqBody })
  }

  const addPayloadToRunkey = (parentId, data, name, type, isMeta) => {
    const addBody = {
      children: [],
      payloads: [
        {
          data,
          meta: isMeta,
          name: name || '',
          type: type || ''
        }
      ]
    }
    const query = new URLSearchParams({ stage: true, id: parentId }).toString()
    return generalRequest(`${dbApiUrl}/on_node?${query}`, { body: addBody })
  }

  const removeObjectsFromRunkey = (parentId, ids = [], payloads = []) => {
    const reqBody = {
      id: parentId
    }
    ids.length && (reqBody.children = ids)
    payloads.length && (reqBody.payloads = payloads)
    const query = new URLSearchParams(reqBody).toString()
    return generalRequest(`${dbApiUrl}/on_node?${query}`, { httpMethod: 'DELETE' })
  }

  const changePayload = async (parentId, payloadId, data, name, type, isMeta, createCopy) => {
    if (createCopy) {
      const removeBody = {
        id: parentId,
        payloads: [payloadId]
      }
      const addBody = {
        payloads: [
          {
            data,
            meta: isMeta,
            name: name || '',
            type: type || ''
          }
        ]
      }
      const query = new URLSearchParams({ stage: true, id: parentId }).toString()
      try {
        await generalRequest(`${dbApiUrl}/on_node?${query}`, { body: addBody })
      } catch (e) {
        return e
      }
      const query2 = new URLSearchParams(removeBody).toString()
      return generalRequest(`${dbApiUrl}/on_node?${query2}`, { httpMethod: 'DELETE' })
    } else {
      const updateBody = {
        id: payloadId,
        type: type || '',
        name: name || '',
        meta: isMeta,
        data
      }
      return generalRequest(`${dbApiUrl}/payload/update`, { body: updateBody })
    }
  }

  return { createNewRunkey, addObjectToRunkey, removeObjectsFromRunkey, commitRunkey, cloneRunkey, deleteRunkey, changePayload, addPayloadToRunkey, addConnectionsToObject }
}

export default useRunkeyWriter
