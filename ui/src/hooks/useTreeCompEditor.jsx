import { useState } from 'react'

const useTreeComponentEditor = (allMetaData) => {
  const [metaData, setMetaData] = useState({})
  const [tree, setT] = useState({ children: [], payloads: [] })

  const setTree = (tree) => {
    if (tree == null) return
    setT(Object.entries(tree).reduce((obj, [id, children]) => {
      if (children === null) {
        obj.payloads.push(id)
      } else {
        obj.children.push(allMetaData[id])
      }
      return obj
    }, { children: [], payloads: [] }))
  }

  const getTree = () => {
    const t = {}
    tree.children.forEach(c => {
      t[c] = {}
    })
    tree.payloads.forEach(c => {
      t[c] = null
    })
    return t
  }

  return [tree, setTree, metaData, setMetaData, getTree]
}

export default useTreeComponentEditor
