import { useState } from 'react'
import { generalRequest } from '@itk-demo-sw/utility-functions'
// import { callbackify } from 'util'

const useTagLoader = (
  dbApiUrl,
  onError = (data) => { },
  meta,
  stagingArea = true
) => {
  const [tagData, setTagData] = useState({})
  const [tagTag] = useState('')
  const [tagType, setTagType] = useState('')
  const [tagComment, setTagComment] = useState('')
  const [tagAuthor, setTagAuthor] = useState('')
  const [lastObject, setLastObjet] = useState({})

  // const _getTagId = async (tagOrId) => {
  //   if (!isNaN(parseInt(tagOrId))) {
  //     setTagTag('')
  //     return parseInt(tagOrId)
  //   }
  //   const reqBody = {
  //     name: tagOrId,
  //     table: 'taguration'
  //   }
  //   return generalRequest(
  //     `${dbApiUrl}/tag/get`, reqBody
  //   ).then(
  //     data => {
  //       setTagTag(tagOrId)
  //       return data.dataset
  //     }
  //   ).catch(
  //     err => {
  //       setTagData('Error loading tag tag!')
  //       onError(err)
  //     }
  //   )
  // }

  const prepareData = (data) => {
    const children = data.children.map((child) => prepareData(child))
    const d = { id: data.id, children, payloads: data.payloads, type: data.type, name: data.name || '' }
    meta.forEach((key) => {
      d[key] = key in data.metadata ? data.metadata[key] : ''
    })
    return d
  }

  const loadObject = (id, view = 1) => {
    const reqBody = {
      stage: stagingArea,
      id,
      depth: 1,
      payload_data: false,
      view
    }
    const query = new URLSearchParams(reqBody).toString()
    generalRequest(
      `${dbApiUrl}/object/tree?${query}`
    ).then(
      data => {
        console.log(data)
        setLastObjet(prepareData(data))
      }
    ).catch(
      err => {
        if (err.status === 403 && err.title === 'Dataset not found') {
          err.detail = 'Dataset not yet added to the database. Wait a moment or contact a developer.'
        }
        onError(err)
      }
    )
  }

  /**
   *
   * @param {string} name
   * @param {'runkey'} type
   * @param {num} view
   * @param {bool} killError
   */
  const loadTag = (name, type, killError = false, depth = 1, view = 1) => {
    const reqBody = {
      stage: stagingArea,
      depth,
      payload_data: false,
      name,
      view
    }
    const query = new URLSearchParams(reqBody).toString()
    generalRequest(
      `${dbApiUrl}/tag/tree?${query}`
    ).then(
      data => {
        console.log(data)
        const children = data.objects && data.objects.map((child) => prepareData(child))
        const d = { id: data.id, children, payloads: data.payloads, type: data.type, name: data.name || '' }
        meta.forEach((key) => {
          d[key] = key in data.metadata ? data.metadata[key] : ''
        })
        setTagData(d)
        setTagType(type + ': ' + name)
        setTagComment(data.comment || '')
        setTagAuthor(data.author || '')
      }
    ).catch(
      err => {
        setTagData('Error loading tag data!')
        if (killError) {
          console.log(err)
        } else {
          onError(err)
        }
      }
    )
  }

  return [tagData, tagTag, tagType, tagComment, tagAuthor, loadTag, loadObject, lastObject]
}

export default useTagLoader
