import { useState } from 'react'
import { generalRequest } from '@itk-demo-sw/utility-functions'

const usePayloadLoader = (
  dbApiUrl,
  onError = (data) => { },
  stagingArea = true
) => {
  const [configData, setPayloadData] = useState('')
  const [configType, setPayloadType] = useState('')
  const [configName, setPayloadName] = useState('')
  const [configMetaData, setPayloadMetaData] = useState('')
  const [configId, setPayloadId] = useState('')

  // const _getConfigId = async (tagOrId) => {
  //   if (!isNaN(parseInt(tagOrId))) {
  //     setConfigTag('')
  //     return parseInt(tagOrId)
  //   }
  //   const reqBody = {
  //     name: tagOrId,
  //     table: 'configuration'
  //   }
  //   return generalRequest(
  //     `${dbApiUrl}/tag/get`, reqBody
  //   ).then(
  //     data => {
  //       setConfigTag(tagOrId)
  //       return data.dataset
  //     }
  //   ).catch(
  //     err => {
  //       setPayloadData('Error loading config tag!')
  //       onError(err)
  //     }
  //   )
  // }

  const loadPayload = (id) => {
    const reqBody = {
      id,
      stage: stagingArea,
      format: true
    }
    const query = new URLSearchParams(reqBody).toString()
    generalRequest(
      `${dbApiUrl}/payload?${query}`
    ).then(
      data => {
        // console.log(data)
        setPayloadName(data.name || '')
        setPayloadData(data.data)
        setPayloadType(data.type)
        setPayloadMetaData(data.meta)
        setPayloadId(id)
      }
    ).catch(
      err => {
        setPayloadData('Error loading config data!')
        onError(err)
      }
    )
  }

  return [configData, configType, configName, configMetaData, loadPayload, configId]
}

export default usePayloadLoader
