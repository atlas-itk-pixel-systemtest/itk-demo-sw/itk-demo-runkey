import { useState } from 'react'
import useMutableTree from './useMutableTree'
import useDynamicTree from './useDynamicTree'
/**
 * Main hook for the `Tree` component. Additional extension hooks exist, that can be loaded by adding to the `feature` array.
 * Currently following features are available:
 *  - 'mutable' - provides utility functions to mutate tree entries:
 *      - `addChild(parentId: string, childId: string, childMetaData: object, child = {})`
 *      - `changeId(oldId: string, newId: string)`
 *      - `deleteComponent(id: string)`
 *  - 'dynamic' - provides utility functions to dynamically add sub trees:
 *      - `addSubTree(rawTreeData: data)`
 *      - `hasComponentChildren(id: string)`
 *  - 'parent'  - provides `parent` value in the `metaData` object with the parent id
 *
 * Standard return values of this hook are:
 *  - `loadTree(rawTreeData: data)`
 *  - `toggleComp(id: string)`
 *  - `tree: object`
 *  - `metadata: object`
 *  - `expandedMap: object`
 *  - `toggleAll()`
 *  - `clearTree()`
 *  - `findId(tree: object, id: string): objects[] `
 *
 * A new tree can be loaded with the `loadTree` function. The expected minimum format of the input for `loadTree` is shown in the example.
 * Any additional keys that `Child` and `Payload` contain, which are relevant for the tree, can be specified in `metaDataAttributes`.
 *
 * @example
 *  data: Child = {
 *    id: string,
 *    children: Child[],
 *    payloads: Payload[]
 *  }
 *  Payload: {
 *    id: string,
 * }.
 * @param {string[]} [metaDataAttributes=[]] additional keys with relevanz for the tree
 * @param {string} [sortBy] value to sort layers by. Possible Options are entries from `metaDataAttributes`
 * @param {boolean} [enablePayloads] `true`. Toggling it to `false` removes payloads from the tree
 * @returns `{ loadTree, toggleComp, tree, metaData, expandedMap, toggleAll, clearTree, findId }` + features
 */
const useTree = ({ metaDataAttributes = [], sortBy = null, enablePayloads = true, features = [] } = {}) => {
  const [tree, setTree] = useState({})
  const [expandedMap, setExpandedMap] = useState({})
  const [metaData, setMetaData] = useState({})

  const toggleComp = (id) => {
    const newExpanded = { ...expandedMap }
    newExpanded[id] = !expandedMap[id]
    setExpandedMap(newExpanded)
  }

  const makeExpandedMap = (tree) => {
    if (Object.keys(tree).length === 0) return
    const expandedMap = {}
    const createLayer = (obj) => {
      Object.entries(obj).forEach(([key, val]) => {
        const id = key
        if (val != null) {
          expandedMap[id] = false
          createLayer(val)
        }
      })
    }
    createLayer(tree)
    return expandedMap
  }

  const toggleAll = () => {
    const toggleTo = !Object.values(expandedMap).every(val => val)
    const newExpanded = {}
    Object.keys(expandedMap).forEach(id => {
      newExpanded[id] = toggleTo
    })
    setExpandedMap(newExpanded)
  }

  const clearTree = () => {
    setTree({})
    setExpandedMap({})
    setMetaData({})
  }

  const useParent = features.includes('parent')
  const buildTree = (data, parentId = null) => {
    const newTree = {}
    const newMeta = {}
    const fillTree = (data, newTree, parentId = null) => {
      const newMetaData = {}
      useParent && (newMetaData.parent = parentId)
      metaDataAttributes.forEach(attr => {
        newMetaData[attr] = data[attr]
      })
      newMeta[data.id] = newMetaData
      if (newTree[data.id] === undefined) newTree[data.id] = {}
      // simplify to tree
      if (sortBy) {
        data.children && data.children.sort((a, b) => a[sortBy].localeCompare(b[sortBy]))
        enablePayloads && data.payloads && data.payloads.sort((a, b) => a[sortBy].localeCompare(b[sortBy]))
      }
      enablePayloads && data.payloads && data.payloads.forEach(pl => {
        newTree[data.id][pl.id] = null
        const newMetaData = {}
        useParent && (newMetaData.parent = data.id)
        metaDataAttributes.forEach(attr => {
          newMetaData[attr] = pl[attr]
        })
        newMeta[pl.id] = newMetaData
      })
      data.children && data.children.forEach(child => {
        fillTree(child, newTree[data.id], data.id)
      })
    }
    fillTree(data, newTree, parentId)
    // Update expansion-  and toggle dicts
    const dest = makeExpandedMap(newTree)

    return { tree: newTree, metaData: newMeta, expandedMap: dest }
  }

  /**
   * The expected minimum format of the input for `loadTree` is shown in the example.
   * @example
   *  data: Child = {
   *    id: string,
   *    children: Child[],
   *    payloads: Payload[]
   *  }
   *  Payload: {
   *    id: string,
   * }.
   * @param {{id: string; children: Child[], payloads: Payload[]}} data
   */
  const loadTree = (data) => {
    const { tree, metaData, expandedMap } = buildTree(data)
    // Set states
    setExpandedMap(expandedMap)
    setMetaData(metaData)
    setTree(tree)
  }

  const findParentTrees = (tree, childId) => {
    const founds = []
    if (tree == null || childId == null || typeof tree !== 'object') return founds
    const ids = Object.keys(tree)
    if (ids.includes(childId)) return [tree]
    for (let i = 0; i < ids.length; i++) {
      const found = findParentTrees(tree[ids[i]], childId)
      founds.push(...found)
    }
    return founds
  }

  const findParentIds = (tree, childId) => {
    const founds = []
    if (tree == null || childId == null || typeof tree !== 'object') return false
    const ids = Object.keys(tree)
    if (ids.includes(childId)) return true
    for (let i = 0; i < ids.length; i++) {
      const found = findParentIds(tree[ids[i]], childId)
      console.log('found', found)
      if (found === true) {
        founds.push(ids[i])
      } else if (Array.isArray(found)) {
        founds.push(...found)
      }
    }
    return founds
  }

  const findId = (tree, id) => {
    const parentTrees = findParentTrees(tree, id)
    return parentTrees.map((tree) => tree[id])
  }

  const dynamics = useDynamicTree(setTree, setMetaData, setExpandedMap, findParentTrees, buildTree, tree, findId)
  const mutables = useMutableTree(setTree, setMetaData, setExpandedMap, findParentTrees, findId)
  const additionalFeatures = features.reduce((collection, feature) => {
    switch (feature) {
      case 'dynamic': {
        collection = { ...collection, ...dynamics }
        break
      }
      case 'mutable': {
        collection = { ...collection, ...mutables }
        break
      }
      default: {
        break
      }
    }
    return collection
  }, {})

  return { loadTree, toggleComp, tree, metaData, expandedMap, toggleAll, clearTree, findId, setMetaData, findParentIds, ...additionalFeatures }
}

export default useTree
