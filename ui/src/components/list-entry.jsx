import React from 'react'
import PropTypes from 'prop-types'
import { Divider, Text } from '@patternfly/react-core'

const ListEntry = (props) => {
  return (
    <div
      className='list-entry'
    >
      <Text onClick={props.onClick}>
        {props.label}
      </Text>
      <Divider style={{
        marginBottom: '8px',
        marginTop: '0px'
      }} />
    </div>
  )
}

ListEntry.propTypes = {
  onClick: PropTypes.func,
  label: PropTypes.string
}

export default ListEntry
