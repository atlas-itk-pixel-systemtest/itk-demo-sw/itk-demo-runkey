import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import useInput from '../hooks/useInput'
import Input from './input'
import { Text } from '@patternfly/react-core'

const TreeComponentEditor = (props) => {
  const possibleStates = ['idle', 'payload', 'component']

  const [getModifiedPayload, setModifiedPayload, resetModifiedPayload, modifiedPayload] = useInput([{ name: 'Name' }, { name: 'Create Copy', type: 'boolean', placeHolder: true, isRequired: false }, { name: 'Content', type: 'text', isRequired: false }])
  const payloadModifier = (
    <Input
      isVisible={true}
      title={`Modify ${props.isPayloadMeta ? 'Metadata' : 'Payload'} ${props.payloadName}`}
      type='flex'
      values={modifiedPayload}
      setValues={setModifiedPayload}
      valueDescriptions={{
        Name: 'name of the new file',
        'Create Copy': 'Create a copy of the payload, instead of modifying the original. This will not update links from other components to this payload.',
        Content: 'Content of the payload'
      }}
      handleClose={(variant) => {
        const payloadId = props.payloadId
        if (variant === 'cancel') {
          props.resetPayloadId()
          // setState(possibleStates[0])
        } else if (variant === 'confirm') {
          const parentId = props.findParentId(payloadId)
          props.changePayload(parentId, payloadId, getModifiedPayload('Content'), getModifiedPayload('Name'), props.payloadType, props.isPayloadMeta, getModifiedPayload('Create Copy')).then((d) => {
            if (d.status === 200) {
              resetModifiedPayload()
              props.onSucces('Successfully changed payload: ', props.payloadName)
              props.resetPayloadId()
            } else {
              props.onError(d)
            }
          }).catch(e => {
            props.onError(e)
          })
        }
      }}
    />
  )

  useEffect(() => {
    if (props.payloadData === 'Error loading config data!' || props.payloadId === '') {
      props.setState('idle')
      return
    }
    setModifiedPayload('Content', props.payloadData)
    setModifiedPayload('Name', props.payloadName)
    props.setState('payload')
  }, [props.payloadData, props.payloadId])

  const idleNotice = (
    <Text
      style={{
        color: 'RoyalBlue',
        fontSize: '20px',
        whiteSpace: 'pre-wrap'
      }}
    >
      Select a component in the tree to edit it
    </Text>
  )

  const [, setModifiedComponent, , modifiedComponent] = useInput([{ name: 'Children', type: 'list', isRequired: false }, { name: 'Payloads', type: 'list', isRequired: false }])
  const componentModifier = (
    <Input
      isVisible={true}
      title={`Modify Component ${props.componentSerial}`}
      type='flex'
      values={modifiedComponent}
      setValues={setModifiedComponent}
      valueDescriptions={{
      }}
      actions={[]}
      handleClose={() => { }}
    />
  )

  useEffect(() => {
    if (!props.componentId || props.componentId === '') {
      props.setState('idle')
      return
    }
    setModifiedComponent('Children', props.componentTree.children)
    setModifiedComponent('Payloads', props.componentTree.payloads)
    props.setState('component')
  }, [props.componentId, props.componentTree])

  let returnComponent = <></>
  switch (props.state) {
    case possibleStates[1]:
      returnComponent = payloadModifier
      break
    case possibleStates[2]:
      returnComponent = componentModifier
      break
    case possibleStates[0]:
    default:
      returnComponent = idleNotice
      break
  }
  return (
    returnComponent
  )
}

TreeComponentEditor.propTypes = {
  dbApiUrl: PropTypes.string.isRequired,
  onError: PropTypes.func.isRequired,
  onSucces: PropTypes.func.isRequired,

  payloadData: PropTypes.string.isRequired,
  payloadId: PropTypes.string.isRequired,
  payloadName: PropTypes.string.isRequired,
  payloadType: PropTypes.string.isRequired,
  isPayloadMeta: PropTypes.bool.isRequired,
  resetPayloadId: PropTypes.func.isRequired,
  findParentId: PropTypes.func.isRequired,
  changePayload: PropTypes.func.isRequired,

  componentId: PropTypes.string.isRequired,
  componentSerial: PropTypes.string.isRequired,
  componentTree: PropTypes.object.isRequired,

  state: PropTypes.string.isRequired,
  setState: PropTypes.func.isRequired
}

export default TreeComponentEditor
