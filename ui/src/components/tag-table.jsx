import React, { useState, useEffect } from 'react'
import {
  Bullseye,
  Button,
  Divider,
  EmptyState,
  EmptyStateBody,
  EmptyStateIcon,
  EmptyStateSecondaryActions,
  Pagination,
  Title,
  Toolbar,
  ToolbarContent,
  ToolbarFilter,
  ToolbarItem,
  Text,
  Tooltip
} from '@patternfly/react-core'
import {
  SearchIcon,
  SyncIcon,
  TrashIcon
} from '@patternfly/react-icons'
// Components
import {
  CheckboxSelect,
  SearchField,
  StandardTable
} from '@itk-demo-sw/components'
// import StandardTable from './standard-table'
// Hooks
import {
  useCheckboxSelect,
  usePagination,
  useSearchField,
  useStandardTable
} from '@itk-demo-sw/hooks'
// PropTypes
import PropTypes from 'prop-types'

const TagTable = (props) => {
  // States and custom Hooks
  const [nameFilter, setNameFilter] = useState('')

  const [
    searchValue,
    handleSearchChange,
    handleSearch,
    clearSearch
  ] = useSearchField(() => setNameFilter(searchValue))

  const [
    isOpen,
    selections,
    onToggle,
    onSelect,
    clearSelections
  ] = useCheckboxSelect()

  const [
    page,
    perPage,
    handleSetPage,
    handlePerPageSelect,
    resetPage
  ] = usePagination(15)

  const [filteredRows, setFilteredRows] = useStandardTable(props.tagList)

  // Helper functions

  const applyFilters = () => {
    const newRows = (
      nameFilter !== '' || selections.length > 0
        ? props.tagList.filter(row => {
          return (
            (nameFilter === '' ||
              row[0].toLowerCase().includes(nameFilter.toLowerCase())
            ) &&
            (selections.length === 0 || selections.includes(row[1]))
          )
        })
        : props.tagList
    )
    const Icon = props.icon
    const tagList = newRows && props.iconAction
      ? newRows.map(r => {
        const row = [...r]
        if (row.length === 3) {
          row.push('')
        }
        row.push(
          Icon
            ? <Icon
              onClick={(e) => {
                e.stopPropagation()
                e.preventDefault()
                props.iconAction(r)
              }}
            />
            : <TrashIcon
              onClick={(e) => {
                e.stopPropagation()
                e.preventDefault()
                props.iconAction(r)
                // resetPage()
              }}
            ></TrashIcon>
        )
        return row
      })
      : newRows
    setFilteredRows([...tagList])
    resetPage()
  }

  const clearAll = () => {
    clearSearch()
    setNameFilter('')
    clearSelections()
  }

  const handleDelete = (filterOption, id = '') => {
    if (filterOption === 'Name') {
      clearSearch()
      setNameFilter('')
    }
    if (filterOption === 'Type') {
      if (selections.includes(id)) {
        onSelect(null, id)
      }
    }
    resetPage()
  }

  // Additional Hooks
  useEffect(() => {
    applyFilters()
  }, [nameFilter, selections, props.tagList])

  // Constants
  const perPageOptions = [
    { title: '5', value: 5 },
    { title: '10', value: 10 },
    { title: '15', value: 15 },
    { title: '20', value: 20 }
  ]

  const columns = [
    { title: 'Name' },
    { title: 'Type' },
    { title: 'Date' },
    { title: 'Comment' }
  ]
  if (props.fullPage) {
    columns.push({ title: 'Author' })
  }

  // React components
  const searchField = (
    <SearchField
      handleChange={handleSearchChange}
      handleSearch={handleSearch}
      value={searchValue}
      name={'Tag Name'}
    />
  )

  const checkboxSelect = (
    <CheckboxSelect
      isOpen={isOpen}
      onToggle={onToggle}
      onSelect={onSelect}
      options={props.tagTypes}
      selections={selections}
      name={'Tag Type'}
    />
  )

  const refreshButton = (
    <Button
      variant="plain"
      onClick={() => props.updateTagList()}
    >
      <SyncIcon />
    </Button>
  )

  const pagination = (
    <Pagination
      itemCount={filteredRows.length}
      perPage={perPage}
      page={page}
      perPageOptions={perPageOptions}
      onPerPageSelect={handlePerPageSelect}
      onSetPage={handleSetPage}
      isCompact
    />
  )

  const toolbar = (
    <Toolbar
      id="tag-list-toolbar"
      clearAllFilters={clearAll}
      collapseListedFiltersBreakpoint="xl"
    >
      <ToolbarContent>
        <ToolbarFilter
          variant="search-filter"
          categoryName="Name"
          chips={nameFilter === '' ? [] : [nameFilter]}
          deleteChip={handleDelete}
        >
          {searchField}
        </ToolbarFilter>
        <ToolbarFilter
          categoryName="Type"
          chips={selections}
          deleteChip={handleDelete}
        >
          {checkboxSelect}
        </ToolbarFilter>
        <ToolbarItem>
          {refreshButton}
        </ToolbarItem>
        <ToolbarItem>
          {pagination}
        </ToolbarItem>
      </ToolbarContent>
    </Toolbar>
  )

  const emptyTable = (
    <Bullseye>
      <EmptyState>
        <EmptyStateIcon icon={SearchIcon} />
        <Title headingLevel="h5" size="lg">
          No results found
        </Title>
        <EmptyStateBody>
          {'No results match this filter criteria. ' +
            'Clear all filters to show results.'}
        </EmptyStateBody>
        <EmptyStateSecondaryActions>
          <Button variant="link" onClick={() => clearAll()}>
            Clear all filters
          </Button>
        </EmptyStateSecondaryActions>
      </EmptyState>
    </Bullseye>
  )
  const table = (
    <StandardTable
      rows={filteredRows.slice((page - 1) * perPage, page * perPage).map((row) => {
        if (typeof row[0] === 'string') {
          const content = <Text
            className='trunkatedIdNoHover'
            style={{ userSelect: 'all', '--maxWidth': props.fullPage ? '300px' : '120px' }}
          >
            {row[0]}
          </Text>
          row[0] = <Tooltip content={row[0]} key={row[0]} className='trunkatedIdNoHover'>{content}</Tooltip>
        }
        if (typeof row[3] === 'string') {
          const content = <Text
            className='trunkatedIdNoHover'
            style={{ '--maxWidth': props.fullPage ? '500px' : '120px' }}
          >
            {row[3]}
          </Text>
          row[3] = <Tooltip content={row[3]} key={row[3]}>{content}</Tooltip>
        }
        return row
      })}
      columns={columns}
      emptyTable={emptyTable}
      isHoverable={props.onRowClick !== null}
      onRowClick={props.onRowClick ? props.onRowClick : () => { }}
    />
  )

  return (
    <React.Fragment>
      {toolbar}
      <Divider />
      {table}
    </React.Fragment>
  )
}

TagTable.propTypes = {
  tagList: PropTypes.array.isRequired,
  tagTypes: PropTypes.array.isRequired,
  onRowClick: PropTypes.func.isRequired,
  updateTagList: PropTypes.func.isRequired,
  iconAction: PropTypes.func,
  icon: PropTypes.object,
  fullPage: PropTypes.bool.isRequired
}

export default TagTable
