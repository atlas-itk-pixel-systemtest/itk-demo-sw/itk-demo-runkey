import React from 'react'
// Components
// Hooks
import PropTypes from 'prop-types'
//
// ./node_modules/jsroot/modules/gui.mjs
// Can't import the named export 'HierarchyPainter' from non EcmaScript module (only default export is available)
// caused by (inacessible) webpack config from react-scripts (deprecated) that could be updated via craco (inactive)
// import { parse, draw } from 'jsroot'

const ResultHisto = (props) => {
  // States and custom Hooks
  const fileUrl = `${props.dbApiUrl}/payload?id=${props.payloadId}&stage=false&jsroot=true`
  const queryString = `/index.htm?nobrowser&optstat=00&json="${fileUrl}"`

  return <iframe
    style={props.style}
    src={props.jsrootUrl + queryString}
  />
}

ResultHisto.propTypes = {
  dbApiUrl: PropTypes.string.isRequired,
  jsrootUrl: PropTypes.string.isRequired,
  payloadId: PropTypes.string.isRequired,
  style: PropTypes.object
}

export default ResultHisto
