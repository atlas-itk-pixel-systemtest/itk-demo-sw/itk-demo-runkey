import React from 'react'
import {
  Text,
  DataListContent,
  DataList,
  DataListItem,
  DataListItemRow,
  DataListToggle,
  DataListItemCells,
  FlexItem,
  Flex,
  Tooltip
} from '@patternfly/react-core'
import { MinusIcon } from '@patternfly/react-icons'

const TagTreeLayer = (id, metaData, content, expandedMap, onCompSelect, canRemove = true, showId = false) => {
  const isExpanded = expandedMap[id]
  const isNotRunkey = metaData[id].type !== 'runkey'
  const isNotRoot = metaData[id].type !== 'root'
  const name = !isNotRunkey ? metaData[id].name : metaData[id].serial
  return (
    <DataListItem
      aria-label={`List ${id}`}
      isExpanded={isExpanded}
      key={`List ${id}`}
    >
      <DataListItemRow
        style={{
          alignItems: 'baseline'
        }}
        key={`Item Row ${id}`}
      >
        <DataListToggle
          onClick={() => onCompSelect(id, 'Toggle')}
          isExpanded={isExpanded}
          key={`List Toggle ${id}`}
        />
        <DataListItemCells
          // style={{
          //   height: '40px'
          // }}
          key={`Item Cell ${id}`}
          dataListCells={[
            <Flex
              key={'mighty One'}
              style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-between'
              }}
            >
              <FlexItem
                key={`Name Cell ${id}`}
                style={{
                  cursor: (isNotRunkey) ? 'pointer' : 'unset',
                  width: '85%',
                  position: 'relative'
                }}
                onClick={() => onCompSelect(id, 'Select', { canSelect: isNotRunkey })}
              >
                <Text
                  key={`Row Title ${id}`}
                  style={{
                    fontSize: '14px',
                    color: 'black'
                  }}
                  className='monospace trunkatedIdNoHover'
                // className='monospace trunkatedId'
                >
                  <span style={{ color: 'navy', fontWeight: 'bold', fontFamily: 'RedHatText' }}>{metaData[id].type}: </span>
                  <Tooltip content={name}><span style={{ userSelect: 'all' }}>{name}</span></Tooltip>
                  {showId && <>
                    <span>; #</span>
                    <Tooltip content={id}><span style={{ userSelect: 'all' }}>{id}</span></Tooltip>
                  </>}
                </Text>
              </FlexItem>
              {isNotRunkey && isNotRoot && canRemove &&
                <>
                  <FlexItem
                    key={`Remove Child ${id}`}
                    className='iconHolder'
                    onClick={() => onCompSelect(id, 'Remove')}
                  >
                    <MinusIcon></MinusIcon>
                  </FlexItem>
                </>
              }
            </Flex>
          ]}
        />
      </DataListItemRow>
      <DataListContent
        key={`List Content ${id}`}
        isHidden={!isExpanded}
      >
        <DataList
          isCompact
          key={`${id}`}
        >
          {content}
        </DataList>
      </DataListContent>
    </DataListItem >)
}

const TagTreePayload = (id, metaData, onCompSelect, canRemove = true, showId = false) => {
  return (
    <DataListItem
      aria-label={`List ${id}`}
      isExpanded={false}
      key={`List ${id}`}
    >
      <DataListItemRow
        style={{
          alignItems: 'baseline'
        }}
        key={`Item Row ${id}`}
      >
        <DataListItemCells
          // style={{
          //   height: '40px'
          // }}
          key={`Item Cell ${id}`}
          dataListCells={[
            <Flex
              key={'mighty One'}
              style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-between'
              }}
            >
              <FlexItem
                key={`Name Cell ${id}`}
                style={{
                  cursor: 'pointer',
                  width: '85%',
                  position: 'relative'
                }}
                onClick={() => onCompSelect(id, 'Payload')}
              >
                <Text
                  key={`type ${id}`}
                  style={{
                    fontSize: '14px',
                    color: '#7d7d7d'
                  }}
                  className='monospace trunkatedIdNoHover'
                >
                  {`${metaData[id].type}: `}<span style={{ userSelect: 'all' }}>{metaData[id].name}</span>
                  {showId && <>
                    <span>; #</span>
                    <Tooltip content={id}><span style={{ userSelect: 'all' }}>{id}</span></Tooltip>
                  </>}
                </Text>
              </FlexItem>

              {!canRemove ||
                <FlexItem
                  key={`Remove Child ${id}`}
                  className='iconHolder'
                  onClick={() => onCompSelect(id, 'RemovePayload')}
                >
                  <MinusIcon></MinusIcon>
                </FlexItem>
              }
            </Flex>
          ]}
        />
      </DataListItemRow>
    </DataListItem >
  )
}

const tagTreeElements = () => {
  return [TagTreeLayer, TagTreePayload]
}

export default tagTreeElements
