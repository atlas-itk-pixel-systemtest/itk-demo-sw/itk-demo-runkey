import React, { useState, useEffect } from 'react'
import {
  Bullseye,
  Button,
  Divider,
  EmptyState,
  EmptyStateBody,
  EmptyStateIcon,
  EmptyStateSecondaryActions,
  Pagination,
  Title,
  Toolbar,
  ToolbarContent,
  ToolbarFilter,
  ToolbarItem
} from '@patternfly/react-core'
import {
  SearchIcon,
  SyncIcon
} from '@patternfly/react-icons'
// Components
import {
  SearchField,
  StandardTable
} from '@itk-demo-sw/components'
// Hooks
import {
  usePagination,
  useSearchField,
  useStandardTable
} from '@itk-demo-sw/hooks'
// PropTypes
import PropTypes from 'prop-types'

const PayloadTable = (props) => {
  // States and custom Hooks
  const [typeFilter, setTypeFilter] = useState('')
  const [nameFilter, setNameFilter] = useState('')

  const [
    searchValueType,
    handleSearchChangeType,
    handleSearchType,
    clearSearchType
  ] = useSearchField(() => setTypeFilter(searchValueType))
  const [
    searchValueName,
    handleSearchChangeName,
    handleSearchName,
    clearSearchName
  ] = useSearchField(() => setNameFilter(searchValueName))

  const [
    page,
    perPage,
    handleSetPage,
    handlePerPageSelect,
    resetPage
  ] = usePagination(15)

  const [filteredRows, setFilteredRows] = useStandardTable(props.payloadList)

  // Helper functions

  const applyFilters = () => {
    const newRows = (
      typeFilter !== '' || nameFilter !== ''
        ? props.payloadList.filter(row => {
          return (
            (typeFilter === '' ||
              row[1].toLowerCase().includes(typeFilter.toLowerCase())
            ) &&
            (nameFilter === '' ||
              row[0].toLowerCase().includes(nameFilter.toLowerCase())
            )
          )
        })
        : props.payloadList
    )
    setFilteredRows([...newRows])
    resetPage()
  }

  const clearAll = () => {
    clearSearchName()
    clearSearchType()
    setTypeFilter('')
    setNameFilter('')
  }

  const handleDelete = (filterOption) => {
    if (filterOption === 'Type') {
      clearSearchType()
      setTypeFilter('')
    }
    if (filterOption === 'Name') {
      clearSearchName()
      setNameFilter('')
    }
    resetPage()
  }

  // Additional Hooks
  useEffect(() => {
    applyFilters()
  }, [typeFilter, nameFilter, props.payloadList])

  // Constants
  const perPageOptions = [
    { title: '5', value: 5 },
    { title: '10', value: 10 },
    { title: '15', value: 15 },
    { title: '20', value: 20 }
  ]

  const columns = [
    { title: 'Name' },
    { title: 'Type' },
    { title: 'UUID' }
  ]

  // React components
  const searchFieldName = (
    <SearchField
      handleChange={handleSearchChangeName}
      handleSearch={handleSearchName}
      value={searchValueName}
      name={'Payload Name'}
    />
  )
  const searchFieldType = (
    <SearchField
      handleChange={handleSearchChangeType}
      handleSearch={handleSearchType}
      value={searchValueType}
      name={'Payload Type'}
    />
  )

  // const checkboxSelect = (
  //   <CheckboxSelect
  //     isOpen={isOpen}
  //     onToggle={onToggle}
  //     onSelect={onSelect}
  //     options={props.tagTypes}
  //     selections={selections}
  //     name={'Config Type'}
  //   />
  // )

  const refreshButton = (
    <Button
      variant="plain"
      onClick={() => props.updateConfigList()}
    >
      <SyncIcon />
    </Button>
  )

  const pagination = (
    <Pagination
      itemCount={filteredRows.length}
      perPage={perPage}
      page={page}
      perPageOptions={perPageOptions}
      onPerPageSelect={handlePerPageSelect}
      onSetPage={handleSetPage}
      isCompact
    />
  )

  const toolbar = (
    <Toolbar
      id="config-list-toolbar"
      clearAllFilters={clearAll}
      collapseListedFiltersBreakpoint="xl"
    >
      <ToolbarContent>
        <ToolbarFilter
          variant="search-filter"
          categoryName="Name"
          chips={nameFilter === '' ? [] : [nameFilter]}
          deleteChip={handleDelete}
        >
          {searchFieldName}
        </ToolbarFilter>
        <ToolbarFilter
          variant="search-filter"
          categoryName="Type"
          chips={typeFilter === '' ? [] : [typeFilter]}
          deleteChip={handleDelete}
        >
          {searchFieldType}
        </ToolbarFilter>
        <ToolbarItem>
          {refreshButton}
        </ToolbarItem>
        <ToolbarItem>
          {pagination}
        </ToolbarItem>
      </ToolbarContent>
    </Toolbar>
  )

  const emptyTable = (
    <Bullseye>
      <EmptyState>
        <EmptyStateIcon icon={SearchIcon} />
        <Title headingLevel="h5" size="lg">
          No results found
        </Title>
        <EmptyStateBody>
          {'No results match this filter criteria. ' +
            'Clear all filters to show results.'}
        </EmptyStateBody>
        <EmptyStateSecondaryActions>
          <Button variant="link" onClick={() => clearAll()}>
            Clear all filters
          </Button>
        </EmptyStateSecondaryActions>
      </EmptyState>
    </Bullseye>
  )

  const table = (
    <StandardTable
      rows={filteredRows.slice((page - 1) * perPage, page * perPage)}
      columns={columns}
      emptyTable={emptyTable}
      isHoverable={props.onRowClick !== null}
      onRowClick={props.onRowClick ? props.onRowClick : () => { }}
    />
  )

  return (
    <React.Fragment>
      {toolbar}
      <Divider />
      {table}
    </React.Fragment>
  )
}

PayloadTable.propTypes = {
  payloadList: PropTypes.array.isRequired,
  tagTypes: PropTypes.array.isRequired,
  onRowClick: PropTypes.func.isRequired,
  updateConfigList: PropTypes.func.isRequired
}

export default PayloadTable
