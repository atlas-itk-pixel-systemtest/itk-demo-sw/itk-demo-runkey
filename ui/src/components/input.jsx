import React from 'react'
import { Modal, ModalVariant, Button, Form, FormGroup, Popover, TextInput, Text, Checkbox, TextArea, Flex, Select, SelectOption } from '@patternfly/react-core'
import HelpIcon from '@patternfly/react-icons/dist/esm/icons/help-icon'
// PropTypes
import PropTypes from 'prop-types'

const Input = (props) => {
  const nameInputRef = React.useRef()

  const createInput = (placeHolder, valueName, type, index, isRequired, isDisabled, inputValue) => {
    const id = `modal-with-form-form-${valueName}`
    switch (type) {
      case 'string':
        return <TextInput
          isRequired={isRequired}
          isDisabled={isDisabled}
          type="email"
          id={id}
          name={id}
          value={placeHolder}
          onChange={(newPlaceHolder) => {
            props.setValues(valueName, newPlaceHolder)
          }}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              e.preventDefault()
            }
          }}
          ref={index === 0 ? nameInputRef : undefined}
        />
      case 'boolean':
        return <Checkbox
          isRequired={isRequired}
          isDisabled={isDisabled}
          id={id}
          name={id}
          isChecked={placeHolder}
          onChange={(newPlaceHolder) => {
            props.setValues(valueName, newPlaceHolder)
          }}
          ref={index === 0 ? nameInputRef : undefined}
        ></Checkbox>
      case 'text':
        return <TextArea
          resizeOrientation='vertical'
          rows="15"
          aria-label="configContent"
          onChange={(value) => props.setValues(valueName, value)}
          value={placeHolder}
          ref={index === 0 ? nameInputRef : undefined}
        />
      case 'list':
        return <>
          {placeHolder.map((pH, i) => {
            return (
              typeof pH === 'string'
                ? <Text
                  key={i}
                >
                  {pH}
                </Text>
                : pH)
          })}
        </>
      case 'select': {
        const [isOpen, setIsOpen] = React.useState(false)
        const [selected, setSelected] = React.useState(null)
        return <Select
          isOpen={isOpen}
          onToggle={() => setIsOpen((o) => !o)}
          onSelect={(event, selection, isPlaceholder) => {
            if (isPlaceholder) {
              setSelected(null)
              setIsOpen(false)
            } else {
              setSelected(selection)
              setIsOpen(false)
            }
            inputValue.onSelect(selection)
          }}
          selections={selected}
          ref={index === 0 ? nameInputRef : undefined}
          >
          {
            placeHolder.map((pH, i) => {
              return (
                typeof pH === 'string'
                  ? <SelectOption
                    key={i}
                    value={pH}
                    isPlaceholder={i === 0}
                  />
                  : pH)
            })
          }
        </Select >
      }
      default:
        return <Text>
          UNIMPLEMENTED TYPE {type}: {valueName} = {String(placeHolder)}
        </Text>
    }
  }

  React.useEffect(() => {
    if (props.isVisible && nameInputRef && nameInputRef.current) {
      nameInputRef.current.focus()
    }
  }, [props.isVisible, nameInputRef])

  const inputs =
    Object.values(props.values || {}).map((inputValue, i) => {
      const value = inputValue.name
      const description = props.valueDescriptions && props.valueDescriptions[value]
      const type = inputValue.type
      const isRequired = inputValue.isRequired
      const placeHolder = inputValue.placeHolder
      const isDisabled = props.disabledValues && props.disabledValues[value]
      return (
        <FormGroup
          key={value}
          label={value}
          labelIcon={description
            ? <Popover
              headerContent={<div>{description}</div>}
            >
              <button
                type='button'
                aria-label={`More info for ${value} field`}
                onClick={e => e.preventDefault()}
                aria-describedby="modal-with-form-form-name"
                className="pf-c-form__group-label-help"
              >
                <HelpIcon noVerticalAlign />
              </button>
            </Popover>
            : <></>}
          isRequired={isRequired}
          fieldId="modal-with-form-form-name"
        >
          {createInput(placeHolder, value, type, i, isRequired, isDisabled, inputValue)}
        </FormGroup>
      )
    })

  const actions = props.actions || [
    <Button
      key="create"
      variant="primary"
      form="modal-with-form-form"
      onClick={() => props.handleClose('confirm')}
      ref={Object.keys(props.values).length === 0 ? nameInputRef : undefined}
    >
      Confirm
    </Button>,
    <Button key="cancel" variant="link" onClick={() => props.handleClose('cancel')}>
      Cancel
    </Button>
  ]

  switch (props.type) {
    case 'flex':
      return (
        <>
          <Flex
            style={{
              visibility: props.isVisible ? 'visible' : 'hidden',
              flexDirection: 'column',
              gap: '10px'
            }}
          >
            <Text
              style={{
                color: 'RoyalBlue',
                fontSize: '20px',
                whiteSpace: 'pre-wrap'
              }}
            >
              {props.title ? props.title : ''}
            </Text>
            <Text>
              {props.description ? props.description : ''}
            </Text>
            <Form
              id="modal-with-form-form"
              style={{
                width: '100%'
              }}
              onSubmit={(e) => {
                e.preventDefault()
              }}
            >
              {inputs}
            </Form>
            <Flex>
              {actions}
            </Flex>
          </Flex>
        </>
      )
    case 'modal':
    default:
      return (
        <>
          <Modal
            variant={ModalVariant.small}
            title={props.title ? props.title : ''}
            description={props.description ? props.description : ''}
            isOpen={props.isVisible}
            onClose={() => props.handleClose('cancel')}
            actions={actions}
          >
            <Form
              id="modal-with-form-form"
              onSubmit={(e) => {
                e.preventDefault()
              }}
            >
              {inputs}
            </Form>
          </Modal>
        </>
      )
  }
}

Input.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  description: PropTypes.string,
  values: PropTypes.object.isRequired,
  setValues: PropTypes.func.isRequired,
  valueDescriptions: PropTypes.object,
  disabledValues: PropTypes.object,
  type: PropTypes.string.isRequired,
  actions: PropTypes.array
}

export default Input
