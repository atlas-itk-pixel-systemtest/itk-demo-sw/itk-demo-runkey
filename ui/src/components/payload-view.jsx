import React, { useState } from 'react'
import {
  Checkbox,
  Divider,
  FlexItem,
  Text,
  TextArea,
  TextContent
} from '@patternfly/react-core'
import PropTypes from 'prop-types'
import ResultHisto from './result-histo'

const histoIdentifiers = [
  'meantotmap',
  // 'occupancymap',
  'chi2map',
  'noisemap',
  'thresholdmap',
  // 'noiseoccupancy',
  'occupancy',
]
const PayloadView = (props) => {
  const [showHisto, setShowHisto] = useState(true)
  const testType = props.curPayloadType.toLowerCase()
  const isHisto = histoIdentifiers.some(id => testType.includes(id.toLowerCase()))
  return (
    <TextContent style={{
      display: 'flex',
      flexDirection: 'column',
      ...props.style
      }}>
      <Text
        style={{
          color: 'rgb(100,149,237,1)',
          fontSize: '28px',
          whiteSpace: 'pre-wrap',
          marginBottom: '0px'
        }}
      >
        {`${props.isPayloadMetaData ? 'MetaData' : 'Payload'} View`}
      </Text>
      <Divider
        style={{
          marginBottom: '0px'
        }}
      />
      <FlexItem
        style={{
          display: 'flex',
          flexDirection: 'row',
        }}
      >
        <Text
          component="p"
          style={{
            color: props.curPayloadTag ? 'DarkBlue' : 'rgb(100,149,237,0.7)',
            fontSize: '22px',
            whiteSpace: 'pre-wrap'
          }}
        >
          {props.curPayloadData ? props.curPayloadTag : 'No payload selected.'}
        </Text>
        <Text
          component="p"
          style={{
            color: 'rgb(100,149,237,1)',
            fontStyle: 'italic',
            fontSize: '20px',
            whiteSpace: 'pre-wrap'
          }}
        >
          {'   ' + props.curPayloadType}
        </Text>
      </FlexItem>
      {isHisto && <Checkbox
      isChecked={showHisto}
      onChange={() => setShowHisto(!showHisto)}
      label='Show Histogram'
      ></Checkbox> }
 {props.curPayloadId === '' || !isHisto || !showHisto
      ? <TextArea
        aria-label="config-text-area"
        value={props.curPayloadData}
        resizeOrientation='vertical'
        rows={20}
        autoResize
        style={{
          width: '100%',
          borderStyle: 'solid',
          borderWidth: '1px',
          backgroundColor: 'rgb(240,248,255,0.7)',
          color: 'rgb(0,0,0,0.85)',
          whiteSpace: 'pre-wrap'
        }}
      />
  : <ResultHisto
      dbApiUrl={props.dbApiUrl}
      jsrootUrl={props.jsrootUrl}
      payloadId={props.curPayloadId}
      style={{
        width: '100%',
        height: '100%',
        borderStyle: 'solid',
        borderWidth: '1px',
        backgroundColor: 'rgb(240,248,255,0.7)',
        color: 'rgb(0,0,0,0.85)',
      }}
    />}
    </TextContent>
  )
}

PayloadView.propTypes = {
  dbApiUrl: PropTypes.string.isRequired,
  jsrootUrl: PropTypes.string.isRequired,
  curPayloadData: PropTypes.string.isRequired,
  curPayloadTag: PropTypes.string.isRequired,
  curPayloadType: PropTypes.string.isRequired,
  isPayloadMetaData: PropTypes.string.isRequired,
  curPayloadId: PropTypes.string.isRequired,
  style: PropTypes.object
}

export default PayloadView
