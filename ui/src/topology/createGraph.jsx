import { EdgeStyle, NodeStatus } from '@patternfly/react-topology'
import { RegionsIcon as Icon1, FolderOpenIcon as Icon2 } from '@patternfly/react-icons'
const findLeavesDepths = (nodes, edges) => {
  const leaves = [...nodes]
  const parents = {}
  for (const edge of edges) {
    if (edge.target in parents) {
      parents[edge.target].push(edge.source)
    } else {
      parents[edge.target] = [edge.source]
    }
    const index = leaves.findIndex((node) => node.id === edge.source)
    if (index !== -1) {
      leaves.splice(index, 1)
    }
  }
  const depths = {}
  const findDepth = (id, depth) => {
    const currentParents = parents[id] || []
    if (currentParents.length === 0) {
      return depth
    }
    if (id in depths) {
      return depths[id] + 1
    }
    const parrentDepths = currentParents.map((parent) => {
      const d = findDepth(parent, depth + 1)
      depths[parent] = d - depth
      return d
    })
    return Math.max(...parrentDepths)
  }
  const leafDepths = leaves.map(leaf => [leaf, findDepth(leaf.id, 1)])
  return leafDepths
}

const createModelFromTree = (tree, config) => {
  if (!tree || !config) {
    return {
      model: {
        nodes: [],
        edges: [],
        graph: {
          id: 'g1',
          type: 'graph',
          layout: 'Dagre'
        }
      },
      nodeIdMap: {}
    }
  }

  let id = 0
  const nodeIdMap = {}
  const createdNodes = {}

  const createNodes = (tree, parentId, parentUuid, siblings, depth) => {
    // Setup
    const nodes = []
    const edges = []
    let nodeConfig = tree.type
    while (typeof nodeConfig === 'string') {
      nodeConfig = config[nodeConfig]
    }
    const style = { ...config.default, ...nodeConfig }
    const labelSuffix = (style.labelSuffix && (tree.metadata[style.labelSuffix] === undefined ? null : tree.metadata[style.labelSuffix])) || id
    const label = style.label
      ? {
        label: `${tree.type} ${labelSuffix}`
      }
      : {}
    if (label.label && label.label.length > 20) {
      label.label = label.label.slice(0, 15) + '...'
    }
    const nodeId = `${tree.type} ${id++}`

    if (tree.id in createdNodes) {
      const nodeId = createdNodes[tree.id].id
      createdNodes[tree.id].data.parentUuids.push(parentUuid)
      // console.log('nodeId', nodeId, 'parent', parentId, tree.view)
      return [[], (tree.view !== 2 && parentId)
        ? [{
          id: `edge-${parentId}-${nodeId}`,
          type: 'edge',
          source: parentId,
          target: nodeId,
          edgeStyle: EdgeStyle.default
        }]
        : [], depth]
    }
    nodeIdMap[tree.metadata.serial || tree.id] = nodeId
    // Current Node
    const node = {
      id: nodeId,
      type: 'node',
      width: style.width,
      height: style.height,
      shape: style.shape,
      status: style.defaultStatus || NodeStatus.danger,
      data: {
        serial: tree.metadata.serial,
        uuid: tree.id,
        parentUuids: parentUuid ? [parentUuid] : [],
        width: style.width / 3,
        height: style.height / 3,
        type: tree.type,
        icon: style.icon && (style.icon === 'Icon1' ? Icon1 : Icon2)
      },
      ...label
    }
    nodes.push(node)
    createdNodes[tree.id] = node
    // console.log('nodeId', nodeId, 'parentID', parentId, tree.view)
    if (parentId && tree.view !== 2) {
      edges.push({
        id: `edge-${parentId}-${node.id}`,
        type: 'edge',
        source: parentId,
        target: node.id,
        edgeStyle: EdgeStyle.default
      })
    }

    // Following Nodes
    if (siblings) {
      // The first of the Chained Children
      let newParentId = node.id
      let maxDepth = depth
      siblings.forEach((sibling, i) => {
        const [childNodes, childEdges, d] = createNodes(sibling, newParentId, parentUuid, null, depth + 1 + i)
        if (maxDepth < d) {
          maxDepth = d
        }
        newParentId = childNodes[0].id
        nodes.push(...childNodes)
        edges.push(...childEdges)
      })
      depth = maxDepth
    } else if (!style.chainChildren) {
      // Standard (chainedChildren = false)
      let maxDepth = depth
      tree.children && tree.children.forEach((child) => {
        const [childNodes, childEdges, d] = createNodes(child, node.id, node.data.uuid, null, depth + 1)
        if (maxDepth < d) {
          maxDepth = d
        }
        nodes.push(...childNodes)
        edges.push(...childEdges)
      })
      depth = maxDepth
    } else if (tree.children.length > 0) {
      // chainedChildren = true
      const child = tree.children[0]
      const siblings = tree.children.slice(1)
      const [childNodes, childEdges, d] = createNodes(child, node.id, node.data.uuid, siblings, depth + 1)
      depth = d
      nodes.push(...childNodes)
      edges.push(...childEdges)
    }
    return [nodes, edges, depth]
  }
  const [nodes, edges] = createNodes(tree, null, null, null, 1)

  const leaves = findLeavesDepths(nodes, edges)
  const maxDepth = Math.max(...leaves.map(([, depth]) => depth))
  for (const [leaf, depth] of leaves) {
    const count = maxDepth - depth
    if (count === 0) {
      continue
    }
    let nodeid = leaf.id
    const nodeType = nodeid.split(' ')[0]
    const style = { ...config.default, ...config[nodeType] }
    const children = Array.from({ length: count }, (_, i) => {
      return {
        id: `${nodeid}-needy-${i}`,
        type: 'invisible',
        width: style.width,
        height: style.height
      }
    })
    nodes.push(...children)
    children.forEach((child, i) => {
      edges.push({
        id: `edge-${nodeid}-${child.id}`,
        type: 'invisible',
        source: nodeid,
        target: child.id,
        edgeStyle: EdgeStyle.default
      })
      nodeid = child.id
    })
  }

  const model = {
    nodes: nodes,
    edges: edges,
    graph: {
      id: 'g1',
      type: 'graph',
      layout: 'Dagre'
    }
  }
  return { model, nodeIdMap }
}

export default createModelFromTree
