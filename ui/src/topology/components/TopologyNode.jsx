import React from 'react'
import PropTypes from 'prop-types'
import { Rectangle, DefaultNode, getShapeComponent } from '@patternfly/react-topology'

const Rect = ({ backgroundClassName, element, width, height, dndDropRef, filter }) => {
  const rect = <Rectangle
    className={backgroundClassName}
    element={element}
    width={width}
    height={height}
    dndDropRef={dndDropRef}
    cornerRadius={5}
    filter={filter}></Rectangle>
  return rect
}
Rect.propTypes = {
  backgroundClassName: PropTypes.string,
  element: PropTypes.object.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  dndDropRef: PropTypes.func,
  cornerRadius: PropTypes.number,
  filter: PropTypes.string
}

const TopologyNode = ({ element, onSelect, selected, onContextMenu, contextMenuOpen, ...rest }) => {
  const data = element.getData()
  const Icon = data.icon
    ? <g transform={`translate(${data.width}, ${data.height})`}>
      <data.icon style={{
        color: '#393F44'
      }} width={data.width} height={data.height} />
    </g>
    : <></>

  return <DefaultNode element={element} onContextMenu={onContextMenu} contextMenuOpen={contextMenuOpen} showStatusDecorator onSelect={onSelect} selected={selected} getCustomShape={(node) => {
    switch (node.getNodeShape()) {
      case 'rect':
        return Rect
      default:
        getShapeComponent(node)
        break
    }
  }} { ...rest }>
    {Icon}
  </DefaultNode >
}
TopologyNode.propTypes = {
  element: PropTypes.object.isRequired,
  onSelect: PropTypes.func.isRequired,
  selected: PropTypes.bool.isRequired,
  onContextMenu: PropTypes.func.isRequired,
  contextMenuOpen: PropTypes.bool.isRequired
}

export default TopologyNode
