import React, { useState, useMemo, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
// Components
import { PageSection, PageSectionVariants } from '@patternfly/react-core'
import { SELECTION_EVENT, TopologyView, Visualization, VisualizationProvider, VisualizationSurface, GRAPH_LAYOUT_END_EVENT } from '@patternfly/react-topology'
import { generalRequest } from '@itk-demo-sw/utility-functions'
import { layoutFactory, componentFactory } from './topologyFactories'
import createModelFromTree from './createGraph'
import ListEntry from '../components/list-entry'
import useRunkeyWriter from '../hooks/useRunkeyWriter'
import useInput from '../hooks/useInput'
import { useConfig } from '../admin-panel/config'
import Input from '../components/input'

const TopologyScreen = (props) => {
  const { getConfig } = useConfig()

  const { removeObjectsFromRunkey, addObjectToRunkey, addConnectionsToObject } = useRunkeyWriter(props.configDBApiUrl, props.onError)

  const [treeConfig, setTreeConfig] = useState(null)
  useEffect(() => {
    fetch('mod_configs/topologyConfig.json', { cache: 'no-cache' }).then((response) => response.json()).then((data) => {
      setTreeConfig(data)
    })
  }, [])

  const [modifiedTree, setModifiedTree] = useState(false)
  const toggleModifiedTree = () => {
    setModifiedTree(!modifiedTree)
  }
  const [tree, setTree] = useState(null)
  useEffect(() => {
    props.configDBApiUrl &&
      props.runkey &&
      generalRequest(`${props.configDBApiUrl}/tag/tree?${new URLSearchParams({
        stage: true,
        depth: -1,
        payload_data: false,
        name: props.runkey,
        view: 3
      })}`).then((data) => {
        const root = { type: 'runkey', children: data.objects || [], metadata: {}, id: data.id }
        if (!root) {
          props.onError({ status: 404, title: 'Not Found', detail: `The selected runkey '${props.runkey}' is empty.` })
          return
        }
        setTree(root)
      }).catch((e) => {
        props.setValidTag(false)
      })
  }, [props.configDBApiUrl, props.runkey, modifiedTree])

  const [selectedIds, setSelectedIds] = useState([])

  const [newParentIds, setNewParentIds] = useState([])
  const [getNewComponent, setNewComponent, , newComponent] = useInput([{ name: 'Type' }])
  const [isNewCompModalVisible, setIsNewCompModalVisible] = useState(false)

  const deletableInfo = useRef(props.deletableInfo)
  deletableInfo.current = props.deletableInfo
  const [controller, nodeIdMap] = useMemo(() => {
    const { model, nodeIdMap } = createModelFromTree(tree, treeConfig)
    const newController = new Visualization()
    newController.registerLayoutFactory(layoutFactory)
    let selectedIds = []

    const metadataMenu = (element) => {
      const serial = element.getData().serial
      const id = element.getData().uuid
      return [
        element.getData().serial
          ? <ListEntry
            key={'serial'}
            onClick={(e) => {
              e.stopPropagation()
            }}
            label={
              <div>
                serial:
                <span
                  style={{
                    userSelect: 'all'
                  }}
                >
                  {serial}
                </span>
              </div>
            }
          />
          : <></>,
        getConfig('showId')
          ? <ListEntry
            key={'id'}
            onClick={(e) => {
              e.stopPropagation()
            }}
            label={
              <div>
                id:
                <span
                  style={{
                    userSelect: 'all'
                  }}
                >
                  {id}
                </span>
              </div>
            }
          />
          : <></>
      ]
    }
    const connectionMenu = (element) => [
      <ListEntry
        key={'addChild'}
        onClick={() => {
          console.log('add child to', selectedIds, element)
          const elements = selectedIds.length > 0
            ? selectedIds.map(id => {
              const elements = newController.elements
              return elements[id]
            })
            : [element]
          setNewParentIds(() => {
            return elements.map(el => el.getData().uuid)
          })
          setIsNewCompModalVisible(true)
        }}
        label='Add Child'
      />,
      <ListEntry
        key={'connect'}
        onClick={() => {
          const elements = newController.elements
          const targets = selectedIds.map(id => elements[id].getData().uuid)
          const deleteInfo = deletableInfo.current(`Connecting ${targets.length} elements to ${element.getLabel() || element.getId()}...`)
          addConnectionsToObject(element.getData().uuid, targets).then(resp => {
            if (resp.status !== 200) {
              deleteInfo({ status: resp.status, title: 'Error', detail: `Failed to connect ${targets} to ${element.getData().uuid}` })
            } else {
              deleteInfo('Successfully connected elements.', 2000)
              toggleModifiedTree()
            }
          }).catch(err => {
            deleteInfo(err)
          })
        }}
        label='Connect selected to'
      />,
      <ListEntry
        key={'replace'}
        onClick={() => {
          const elements = newController.elements
          const children = selectedIds.map(id => elements[id])
          const deletions = children.map(element => {
            return [element.getData().parentUuids, element.getData().uuid]
          }).reduce((deletions, [parents, target]) => {
            parents.forEach(parent => {
              if (parent in deletions) {
                deletions[parent].push(target)
              } else {
                deletions[parent] = [target]
              }
            })
            return deletions
          }, {})

          const deleteInfo = deletableInfo.current(`Replacing ${selectedIds.length} elements parents with ${element.getLabel() || element.getId()}...`)
          Promise.all(Object.entries(deletions).map(async ([parent, targets]) => {
            const response = await removeObjectsFromRunkey(parent, targets)
            if (response.status !== 200) {
              deleteInfo({ status: response.status, title: 'Error', detail: `Failed to delete ${targets} from ${parent}` })
            }
            console.log('DELETED', parent, targets)
          })).then(() => {
            addConnectionsToObject(element.getData().uuid, children.map(child => child.getData().uuid)).then(resp => {
              if (resp.status !== 200) {
                deleteInfo({ status: resp.status, title: 'Error', detail: `Failed to connect ${children} to ${element.getData().uuid}` })
              } else {
                deleteInfo('Successfully connected elements.', 2000)
                toggleModifiedTree()
              }
            }).catch(err => {
              console.log(err)
              deleteInfo(err)
            })
          }).catch(err => {
            console.log(err)
            deleteInfo(err)
          })
        }}
        label='Replace Parents with'
      />]
    const deletionMenu = (element) => [
      <ListEntry
        key={'delete'}
        onClick={() => {
          const elements = selectedIds.length > 0
            ? selectedIds.map(id => {
              const elements = newController.elements
              return elements[id]
            })
            : [element]

          let deletions
          if (elements[0].getType() === 'edge') {
            const edge = elements[0]
            deletions = { [edge.getSource().getData().uuid]: [edge.getTarget().getData().uuid] }
          } else {
            deletions = elements.map(element => {
              return [element.getData().parentUuids, element.getData().uuid]
            }).reduce((deletions, [parents, target]) => {
              parents.forEach(parent => {
                if (parent in deletions) {
                  deletions[parent].push(target)
                } else {
                  deletions[parent] = [target]
                }
              })
              return deletions
            }, {})
          }

          const deleteInfo = deletableInfo.current(`Deleting ${selectedIds.length > 0 ? selectedIds.length : 1} elements...`)
          Promise.all(Object.entries(deletions).map(async ([parent, targets]) => {
            try {
              const response = await removeObjectsFromRunkey(parent, targets)
              if (response.status !== 200) {
                deleteInfo({ status: response.status, title: 'Error', detail: `Failed to delete ${targets} from ${parent}` })
              }
            } catch (e) {
              deleteInfo(e)
            }
          })).then(() => {
            deleteInfo('Successfully deleted elements.', 2000)
            toggleModifiedTree()
          })
        }
        }
        label='Delete'
      />]
    const graphMenu = (graph) => [
      <ListEntry
        key={'format'}
        onClick={() => {
          graph.layout()
        }}
        label='Format Graph'
      />
    ]
    const contextMenus = {
      graph: graphMenu,
      node: (el) => [...metadataMenu(el), ...connectionMenu(el), ...deletionMenu(el)],
      edge: deletionMenu
    }
    newController.registerComponentFactory((kind, type) => componentFactory(kind, type, contextMenus))

    const fitGraph = () => {
      const graph = newController.getGraph()
      graph.fit(80)
    }
    newController.addEventListener(GRAPH_LAYOUT_END_EVENT, fitGraph)
    newController.addEventListener(SELECTION_EVENT, (selected) => {
      selectedIds = selected
      setSelectedIds(selected)
    })
    newController.fromModel(model, false)
    return [newController, nodeIdMap]
  }, [tree, treeConfig])
  console.log(nodeIdMap)

  const newCompModal = (
    <Input
      isVisible={isNewCompModalVisible}
      title={'Create new Component'}
      values={newComponent}
      type='modal'
      setValues={setNewComponent}
      valueDescriptions={{
        Type: 'type of the component'
      }}
      handleClose={(variant) => {
        controller.fireEvent(SELECTION_EVENT, [])
        if (variant === 'cancel') {
          setIsNewCompModalVisible(false)
        } else if (variant === 'confirm') {
          setIsNewCompModalVisible(false)
          console.log('create new component', getNewComponent('Type'), newParentIds)
          addObjectToRunkey(getNewComponent('Type'), newParentIds).then(({
            status, id
          }) => {
            if (status === 200) {
              props.onSuccess('Successfully created new ' + getNewComponent('Type'))
              toggleModifiedTree()
            }
          }).catch(
            err => {
              if (err.detail === undefined) {
                props.onGenericError('JavaScript-Error: ' + err.name, err.message, err.message)
              } else {
                props.onError(err)
              }
            }
          )
        }
      }}
    ></Input >
  )

  const TopologyVisualizer = () => {
    return (
      <VisualizationProvider controller={controller}>
        <VisualizationSurface
          state={{ selectedIds }}
        />
      </VisualizationProvider>
    )
  }

  return (
      <PageSection
        isFilled
        padding={{ default: 'noPadding' }}
        style={{ overflow: 'hidden', height: '100%' }}
        variant={PageSectionVariants.light}
      >
        <TopologyView onClick={() => {
          controller.fireEvent(SELECTION_EVENT, [])
        }}>
          <TopologyVisualizer />
        </TopologyView>
        {newCompModal}
      </PageSection>
  )
}

export default TopologyScreen

TopologyScreen.propTypes = {
  onInfo: PropTypes.func.isRequired,
  onWarn: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  onGenericError: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  configDBApiUrl: PropTypes.string.isRequired,
  deletableInfo: PropTypes.func.isRequired,
  runkey: PropTypes.string.isRequired,
  setValidTag: PropTypes.func.isRequired
}
