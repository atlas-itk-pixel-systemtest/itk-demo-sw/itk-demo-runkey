import React from 'react'
import {
  DefaultEdge,
  ColaLayout,
  withContextMenu, DefaultGroup,
  withPanZoom, withSelection,
  DagreLayout, ColaGroupsLayout,
  GraphComponent, ModelKind,
  withDragNode
} from '@patternfly/react-topology'
import TopologyNode from './components/TopologyNode'

const InvisibleComponent = () => {
  return (
    <></>
  )
}

const componentFactory = (kind, type, contextMenus = {}) => {
  switch (type) {
    case 'group':
      return DefaultGroup
    case 'invisible':
      return InvisibleComponent
    default:
      switch (kind) {
        case ModelKind.graph:
          return withContextMenu((element) => contextMenus.graph(element) || [])(withPanZoom()(GraphComponent))
        case ModelKind.node:
          return withDragNode()(withContextMenu((element) => contextMenus.node(element) || [])(withSelection({ multiSelect: true })(TopologyNode)))
        case ModelKind.edge:
          return withContextMenu((element) => contextMenus.edge(element) || [])(DefaultEdge)
        default:
          return undefined
      }
  }
}

const layoutFactory = (type, graph) => {
  switch (type) {
    case 'Dagre':
      return new DagreLayout(graph, { rankdir: 'LR', nodesep: 5, ranksep: 60, edgesep: 5, nodeDistance: 20 })
    case 'ColaGroups':
      return new ColaGroupsLayout(graph, {
        layoutOnDrag: false
      })
    default:
      return new ColaLayout(graph, {
        layoutOnDrag: false
      })
  }
}

export { componentFactory, layoutFactory }
