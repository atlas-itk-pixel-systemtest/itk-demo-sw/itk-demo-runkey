import React, { useState, useEffect, useRef } from 'react'
import { useConfig } from '../admin-panel/config'
import {
  Alert,
  Page,
  Button,
  PageNavigation,
  PageSection,
  PageSectionVariants
} from '@patternfly/react-core'
import { generalRequest, getCookie, setCookie } from '@itk-demo-sw/utility-functions'
// Screens
import TagPanel from './tag-panel'
// Components
import {
  LoggingViewer,
  Navbar,
  Notifications
} from '@itk-demo-sw/components'
// Hooks
import {
  useLoggingViewer,
  useNotifications,
  useConfigureValue
} from '@itk-demo-sw/hooks'
// Proptypes
import PropTypes from 'prop-types'
import RunkeyManagerPanel from './runkey-manager-panel'

const ConfigdbDashboard = (props) => {
  const [tagTypes, setTagTypes] = useState([])
  useEffect(() => {
    fetch('mod_configs/tagTypes.json', { cache: 'no-cache' }).then((response) => response.json()).then(tt => setTagTypes(tt)).catch(e => {
      console.error('Failed to load "mod_configs/tag_types.json":', e)
    })
  }, [])
  const [connectionEstablished, setConnectionEstablished] = useState(false)
  const [showLog, setShowLog] = useState(false)
  const [logContent, addLog] = useLoggingViewer()
  const [dbState, setState] = useState('')
  const [dbStateStatus, setStatus] = useState('')
  const [
    alerts,
    onErrorFromNotifications,
    onWarnFromNotifications,
    onInfoFromNotifications,
    onSuccessFromNotifications,
    deleteAlert,
    onGenericError
  ] = useNotifications(addLog)

  const eternalDelete = useRef(deleteAlert)
  eternalDelete.current = deleteAlert
  const eternalSuccess = useRef(onSuccessFromNotifications)
  eternalSuccess.current = onSuccessFromNotifications
  const eternalError = useRef(onErrorFromNotifications)
  eternalError.current = onErrorFromNotifications
  const deletableInfo = (msg) => {
    const id = alerts.length
    onInfoFromNotifications(msg)
    return (resolveMessage = null, timeout = false) => {
      console.log(resolveMessage)
      eternalDelete.current(id)
      if (resolveMessage === null) return
      if (typeof resolveMessage === 'string') eternalSuccess.current(resolveMessage, timeout)
      else if (typeof resolveMessage === 'object') eternalError.current(resolveMessage, timeout)
    }
  }

  const { getConfig, setConfig } = useConfig()
  const onWarn = (err, timeout = 3000) => onWarnFromNotifications(err, timeout)
  const onInfo = (err, timeout = 3000) => onInfoFromNotifications(err, timeout)
  const onSuccess = (err, timeout = 2000) => onSuccessFromNotifications(err, timeout)
  const onError = (err, timeout = false) => onErrorFromNotifications(err, timeout)
  const healthFrequency = getConfig('healthFrequency')
  const callHealth = getConfig('callHealth')
  const [intervalId, setIntervalId] = useState(0)
  const dbApiUrl = props.dbUrl || useConfigureValue('/config', 'configdbUrlKey', getConfig('configdb'), 'defaultConfigDbUrl')
  const backendApiUrl = props.apiUrl || useConfigureValue('/config', 'apiUrlKey', getConfig('api'), 'defaultApiUrl')
  const jsrootUrl = props.jsrootUrl || useConfigureValue('/config', 'jsrootUrlKey', getConfig('jsroot'), 'defaultJsrootUrl')
  useEffect(() => {
    setConfig('jsroot', jsrootUrl)
  }, [jsrootUrl])
  useEffect(() => {
    setConfig('api', backendApiUrl)
  }, [backendApiUrl])
  useEffect(() => {
    setConfig('configdb', dbApiUrl)
  }, [dbApiUrl])

  const handleLog = response => {
    if ((Object.keys(response).includes('status')) &&
      (Object.keys(response).includes('messages'))) {
      if (response.messages.length > 0) {
        for (const m of response.messages) {
          if (response.status === 200) onInfo(m)
          if (response.status === 299) onWarn(m)
        }
      }
    }
  }

  const getHealth = async (apiUrl) => {
    return generalRequest(`${apiUrl}/health`).then(
      data => {
        handleLog(data)
        setState(data.state)
        setStatus(data.state_status)
        setConnectionEstablished(true)
        return data.state
      }
    ).catch(
      () => {
        console.log('Failed flask backend connection. URL: ' + apiUrl)
        setConnectionEstablished(false)
        setState('')
        setStatus('')
        return ''
      }
    )
  }

  useEffect(() => {
    clearInterval(intervalId)
    getHealth(dbApiUrl)
    setIntervalId(setInterval(() => { if (callHealth) getHealth(dbApiUrl) }, healthFrequency))
  }, [callHealth, healthFrequency, dbApiUrl])

  const noConnectionAlert = (
    connectionEstablished
      ? null
      : (
        <PageSection
          key={'no connection alert sec'}
          variant={PageSectionVariants.light}
          style={{ padding: '0px' }}
        >
          <Alert
            key={'no connection alert alert'}
            variant='danger'
            isInline
            title='configDB API is not responding!'
          >
            {'Please check if the API backend is running. ' +
              `Currently trying to connect to '${dbApiUrl}'.`}
          </Alert>
        </PageSection>
      )
  )

  const panels = (props.panels || [
    {
      title: 'Combined',
      Content: TagPanel,
      props: {}
    },
    {
      title: 'Staging Area',
      Content: TagPanel,
      props: { stagingArea: true }
    },
    {
      title: 'Backend',
      Content: TagPanel,
      props: { stagingArea: false }
    },
    {
      title: 'Runkey Manager',
      Content: RunkeyManagerPanel
    }
  ]).map(({ title, Content, props }) => {
      return {
        title,
        content: <Content
          tagTypes={tagTypes}
          dbApiUrl={dbApiUrl}
          backendApiUrl={backendApiUrl}
          jsrootUrl={jsrootUrl}
          onError={onError}
          onGenericError={onGenericError}
          onWarn={onWarn}
          onInfo={onInfo}
          onSuccess={onSuccess}
          deletableInfo={deletableInfo}
          dbState={dbState}
          dbStateStatus={dbStateStatus}
          updateState={getHealth}
          {...props}
        />
      }
    })

  const [activeItem, setActiveItem] = useState(Math.min(Number(getCookie(`activePanel-${props.path}`)), panels.length - 1))
  const changePanel = (panel) => {
    setCookie(`activePanel-${props.path}`, panel.itemId)
    setActiveItem(panel.itemId)
  }
  const itemNames = panels.map((p) => p.title)

  return (
    <Page>
      {noConnectionAlert}
      <PageSection padding={{ default: 'noPadding' }} id='main page navbar'>
        <PageNavigation key={'page nav'}>
          <Navbar
            key={'navbar'}
            activeItem={activeItem}
            changePanel={changePanel}
            itemNames={itemNames}
            onError={onError}
            onInfo={onInfo}
            onSuccess={onSuccess}
            onWarn={onWarn}
          />
        </PageNavigation>

      </PageSection>
      <Notifications alerts={alerts} deleteAlert={deleteAlert} />
      <PageSection
        isFilled
        hasOverflowScroll
        padding={{ default: 'noPadding' }}
      >
        {panels[activeItem].content}
      </PageSection>
      <PageSection
        key={'logging section'}
        sticky='bottom'
        padding={{ default: 'noPadding' }}
        isFilled={false}
      >
        <Button
          key={'toggle logging button'}
          isBlock
          variant='plain'
          onClick={(event) => setShowLog((prevShowLog) => !prevShowLog)}
          style={{
            backgroundColor: 'aliceblue'
          }}
        >
          {showLog ? 'Hide Logs' : 'Show Logs'}
        </Button>
        {showLog
          ? (
            <LoggingViewer content={logContent} key={'logging view'} id={'logging view'} />
          )
          : null
        }
      </PageSection>
    </Page>
  )
}

ConfigdbDashboard.propTypes = {
  dbUrl: PropTypes.string,
  apiUrl: PropTypes.string,
  jsrootUrl: PropTypes.string,
  panels: PropTypes.array,
  path: PropTypes.string.isRequired
}

export default ConfigdbDashboard
