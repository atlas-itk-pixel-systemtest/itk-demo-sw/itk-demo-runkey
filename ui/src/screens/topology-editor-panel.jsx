import React, { useState } from 'react'
import PropTypes from 'prop-types'
import TopologyScreen from '../topology/TopologyScreen'
import { ExpandableSection, Page, PageSection, PageSectionVariants, Text, Bullseye } from '@patternfly/react-core'
import PanelTitle from '../components/panel-title'

const TopologyEditorPanel = (props) => {
  const [validTag, setValidTag] = useState(true)

  const topology = <TopologyScreen
    onInfo={props.onInfo}
    onWarn={props.onWarn}
    onError={props.onError}
    onGenericError={props.onGenericError}
    onSuccess={props.onSuccess}
    configDBApiUrl={props.dbApiUrl}
    runkey={props.searchParams.name}
    setValidTag={setValidTag}
    deletableInfo={props.deletableInfo}
  ></TopologyScreen>
  const title = (
    <PanelTitle
      title={'Tag Tree Editor - Topology'}
      dbState={props.dbState}
      dbStateStatus={props.dbStateStatus}
      dbApiUrl={props.dbApiUrl}
      onError={props.onError}
      onInfo={props.onInfo}
      updateState={props.updateState}
    >
    </PanelTitle>
  )

  const [isExpanded, setIsExpanded] = useState(false)
  const helpText = 'Select multiple nodes with control + click. Bring up the context menu with right click on a target node to perform operations with the selected nodes. "Delete" will remove only edges. When nothing is selected it removes all edges pointing to the the target, otherwise only the edges between the target and the selected nodes. Nodes without a connection will be removed from the tree on commit.'
  const help = <ExpandableSection
    toggleText={isExpanded ? 'Hide help' : 'Show help'}
    onToggle={() => setIsExpanded(!isExpanded)}
    isExpanded={isExpanded}
    isIndented
  >
    {helpText}<br/>
  </ExpandableSection>

  const invalidTagPage = (
    <Page>
      <PanelTitle
        title={'Something went wrong'}
        subtext={`The ${props.searchParams.type}: ${props.searchParams.name} could not be found.`}
        dbState={props.dbState}
        dbStateStatus={props.dbStateStatus}
        dbApiUrl={props.dbApiUrl}
        onError={props.onError}
        onInfo={props.onInfo}
        updateState={props.updateState}
      ></PanelTitle>
      <PageSection
        isFilled
        variant={PageSectionVariants.light}
      >
        <Bullseye>
          <Text>
            Please try <a onClick={
              (e) => {
                window.location.reload()
                return false
              }}>again</a> or go <a onClick={
                (e) => {
                  window.close()
                  return false
                }}>back to the Tag List</a>
          </Text>
        </Bullseye>
      </PageSection>
    </Page>
  )

  return (
    validTag
      ? <Page>
        {title}
        <PageSection
          style={{
            paddingTop: 0,
            paddingBottom: 0
          }}
          variant={PageSectionVariants.light}
        >
          {help}
          </PageSection>
        <PageSection
          isFilled
          variant={PageSectionVariants.light}
        >
          {topology}
        </PageSection>
      </Page>
      : invalidTagPage
  )
}

TopologyEditorPanel.propTypes = {
  onInfo: PropTypes.func.isRequired,
  onWarn: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onGenericError: PropTypes.func.isRequired,
  dbApiUrl: PropTypes.string.isRequired,
  searchParams: PropTypes.object.isRequired,
  dbState: PropTypes.string.isRequired,
  dbStateStatus: PropTypes.string,
  updateState: PropTypes.func.isRequired,
  deletableInfo: PropTypes.func.isRequired
}

export default TopologyEditorPanel
