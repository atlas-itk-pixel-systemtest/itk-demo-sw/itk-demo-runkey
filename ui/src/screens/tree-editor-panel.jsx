import React, { useEffect, useMemo, useRef, useState } from 'react' //, { useEffect, useState } from 'react'able
import {
  Flex,
  FlexItem,
  Page,
  PageSection,
  PageSectionVariants,
  Text,
  Bullseye,
  Button
} from '@patternfly/react-core'
// Components
import { MinusIcon, PlusIcon } from '@patternfly/react-icons'
import {
  Tree
} from '@itk-demo-sw/components'
import PanelTitle from '../components/panel-title'
import tagTreeElements from '../components/tag-tree-elements'
import TreeComponentEditor from '../components/tree-component-editor'
// Hooks
// import { useTree } from '@itk-demo-sw/hooks'
import useTree from '../hooks/useTree'
import useTagLoader from '../hooks/useTagLoader'
import { useConfig } from '../admin-panel/config'
// Proptypes
import PropTypes from 'prop-types'

import useRunkeyWriter from '../hooks/useRunkeyWriter'
import useInput from '../hooks/useInput'
import Input from '../components/input'
import usePayloadLoader from '../hooks/usePayloadLoader'

const TreeEditorPanel = (props) => {
  const { getConfig } = useConfig()
  const deletableInfo = useRef(props.deletableInfo)
  deletableInfo.current = props.deletableInfo

  // States and custom Hooks
  const meta = ['serial']
  const [
    curTagData,
    ,
    curTagType,
    curTagComment,
    curTagAuthor,
    loadTag,
    loadObject,
    lastObject
  ] = useTagLoader(props.dbApiUrl, props.onError, meta)

  const metaDataAttributes = ['type', 'name', ...meta]
  const {
    loadTree,
    toggleComp,
    tree,
    metaData,
    expandedMap,
    deleteComponent,
    addSubTree,
    findId,
    hasComponentChildren,
    findParentIds
  } = useTree({ metaDataAttributes, sortBy: metaDataAttributes[0], features: ['dynamic', 'mutable', 'parent'] })
  const { addObjectToRunkey, removeObjectsFromRunkey, commitRunkey, changePayload, addPayloadToRunkey } = useRunkeyWriter(props.dbApiUrl, props.onError, props.onGenericError, props.updateState)

  const [getRunkeyCommit, setRunkeyCommit, , runkeyCommit] = useInput([{ name: 'Name', placeHolder: props.searchParams && props.searchParams.name }, { name: 'Type' }, { name: 'Author' }, { name: 'Comment', isRequired: false }])
  const [isCommitModalVisible, setIsCommitModalVisible] = useState(false)
  const commitButton = (
    <>
      <Button
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          gap: '5px',
          marginTop: '0.8rem'
        }}
        onClick={() => {
          setRunkeyCommit('Comment', curTagComment)
          setRunkeyCommit('Author', curTagAuthor)
          setRunkeyCommit('Type', props.searchParams.type)
          setIsCommitModalVisible(true)
        }}
      >
        <Text>
          Commit Runkey
        </Text>
      </Button>
      <Input
        isVisible={isCommitModalVisible}
        title={'Create new Runkey'}
        values={runkeyCommit}
        type='modal'
        setValues={setRunkeyCommit}
        valueDescriptions={{
          Name: 'name of the new runkey'
        }}
        handleClose={(variant) => {
          if (variant === 'cancel') {
            setIsCommitModalVisible(false)
          } else if (variant === 'confirm') {
            const deleteInfo = deletableInfo.current('Committing the runkey as ' + getRunkeyCommit('Name'))
            commitRunkey(props.searchParams.name, getRunkeyCommit('Name'), getRunkeyCommit('Author'), getRunkeyCommit('Comment'), getRunkeyCommit('Type')).then(({ status, id }) => {
              deleteInfo('Successfully committed runkey')
              if (status === 200) {
                setIsCommitModalVisible(false)
                window.close()
              }
            }).catch(
              err => {
                deleteInfo(err)
              }
            )
          }
        }}
      ></Input>
    </>
  )

  // const [getNewComponent, setNewComponent, , newComponent] = useInput([{ name: 'Type', type: 'option', variants: ['felix', 'optoboard', 'frontend'] }])
  const [getNewComponent, setNewComponent, , newComponent] = useInput([{ name: 'Type' }])
  const [isNewCompModalVisible, setIsNewCompModalVisible] = useState(false)
  const newCompModal = (
    <Input
      isVisible={isNewCompModalVisible}
      title={'Create new Component'}
      values={newComponent}
      type='modal'
      setValues={setNewComponent}
      valueDescriptions={{
        Type: 'type of the component'
      }}
      handleClose={(variant) => {
        if (variant === 'cancel') {
          setIsNewCompModalVisible(false)
        } else if (variant === 'confirm') {
          setIsNewCompModalVisible(false)
          addObjectToRunkey(getNewComponent('Type'), [selectedTreeComp]).then(({
            status, id
          }) => {
            if (status === 200) {
              props.onSuccess('Successfully created new ' + getNewComponent('Type'))
              loadObject(selectedTreeComp)
            }
          }).catch(
            err => {
              if (err.detail === undefined) {
                props.onGenericError('JavaScript-Error: ' + err.name, err.message, err.message)
              } else {
                props.onError(err)
              }
            }
          )
        }
      }}
    ></Input >
  )
  const [getNewPayload, setNewPayload, resetNewPayload, newPayload] = useInput([{ name: 'Name' }, { name: 'Type' }, { name: 'Metadata', type: 'boolean', placeHolder: false }, { name: 'Content', type: 'text', isRequired: false }])
  const [isNewPayloadModalVisible, setIsNewPayloadModalVisible] = useState(false)
  const newPayloadModal = (
    <Input
      isVisible={isNewPayloadModalVisible}
      title={'Create new Payload'}
      values={newPayload}
      type='modal'
      setValues={setNewPayload}
      valueDescriptions={{
        Metadata: 'Is this payload a metadata payload'
      }}
      handleClose={(variant) => {
        if (variant === 'cancel') {
          setIsNewPayloadModalVisible(false)
        } else if (variant === 'confirm') {
          addPayloadToRunkey(selectedTreeComp, getNewPayload('Content'), getNewPayload('Name'), getNewPayload('Type'), getNewPayload('Metadata')).then(({
            status, id
          }) => {
            if (status === 200) {
              setIsNewPayloadModalVisible(false)
              props.onSuccess('Successfully created new ' + getNewComponent('Type'))
              loadObject(selectedTreeComp)
              resetNewPayload()
            }
          }).catch(
            err => {
              if (err.detail === undefined) {
                props.onGenericError('JavaScript-Error: ' + err.name, err.message, err.message)
              } else {
                props.onError(err)
              }
            }
          )
        }
      }}
    ></Input >
  )

  const [selectedTreeComp, setSelectedTreeComp] = useState('')
  // const [componentTree, setComponentTree] = useState({ children: [], payloads: [] })
  const componentTree = useMemo(() => {
    const comp = findId(tree, selectedTreeComp)[0]
    console.log('SELECTED CHANGED TO: ', selectedTreeComp, ' with tree', comp)
    return comp
      ? (
        {
          children: Object.entries(comp)
            .reduce(
              (children, [id, child]) =>
                child !== null
                  ? [...children,
                  <Flex
                    key={id}
                    style={{
                      justifyContent: 'space-between',
                      width: '50%',
                      border: '1px solid black'
                    }}
                  >
                    <Text>
                      {metaData[id].type} {metaData[id].serial}
                    </Text>
                    <FlexItem
                      key={`Remove Child ${id}`}
                      className='iconHolder'
                      onClick={() => onCompSelect(id, 'Remove')}
                    >
                      <MinusIcon></MinusIcon>
                    </FlexItem>
                  </Flex>
                  ]
                  : children,
              [
                <Button
                  key='createChild'
                  variant='secondary'
                  onClick={() => {
                    setIsNewCompModalVisible(true)
                  }}
                >
                  <PlusIcon></PlusIcon>
                  new Child
                </Button>
              ]),
          payloads: Object.entries(comp)
            .reduce(
              (payloads, [id, child]) =>
                child === null
                  ? [...payloads,
                  <Flex
                    key={id}
                    style={{
                      justifyContent: 'space-between',
                      width: '50%'
                    }}
                  >
                    <Text>
                      {metaData[id].type} {metaData[id].name}
                    </Text>
                    <FlexItem
                      key={`Remove Child ${id}`}
                      className='iconHolder'
                      onClick={() => onCompSelect(id, 'RemovePayload')}
                    >
                      <MinusIcon></MinusIcon>
                    </FlexItem>
                  </Flex>]
                  : payloads,
              [
                <Button
                  key='createPayload'
                  variant='secondary'
                  onClick={() => {
                    setIsNewPayloadModalVisible(true)
                  }}
                >
                  <PlusIcon></PlusIcon>
                  new Payload
                </Button>
              ])
        })
      : { children: [], payloads: [] }
  }, [selectedTreeComp, tree])
  const [payloadId, setPayloadId] = useState('')
  const [payloadData, payloadType, payloadName, isPayloadMeta, loadPayload] = usePayloadLoader(props.dbApiUrl, props.onError)
  const [state, setState] = useState('idle')
  const treeCompEditor = (
    <TreeComponentEditor
      dbApiUrl={props.dbApiUrl}
      onError={props.onError}
      onSucces={props.onSuccess}

      payloadData={payloadData}
      payloadId={payloadId}
      payloadName={payloadName}
      payloadType={payloadType}
      isPayloadMeta={isPayloadMeta}
      resetPayloadId={() => {
        const parentId =
          findParentIds(tree, payloadId)[0]
        deleteComponent(payloadId)
        loadObject(parentId)
        setPayloadId('')
        setSelectedTreeComp('')
      }}
      findParentId={(id) => {
        return findParentIds(tree, id)[0]
      }}
      changePayload={changePayload}

      componentId={selectedTreeComp}
      componentSerial={metaData[selectedTreeComp] ? metaData[selectedTreeComp].serial : ''}
      componentTree={componentTree}

      state={state}
      setState={setState}
    ></TreeComponentEditor>
  )

  const onCompSelect = (id, origin, event) => {
    console.log(origin, id, event)
    let newState = 'idle'
    switch (origin) {
      case 'Toggle':
        newState = state
        if (!hasComponentChildren(id)) {
          loadObject(id)
        }
        toggleComp(id)
        break
      case 'Select':
        if (!hasComponentChildren(id)) {
          loadObject(id)
        }
        if (event.canSelect) {
          setSelectedTreeComp(id)
          newState = 'component'
        }
        break
      case 'Remove':
        removeObjectsFromRunkey(metaData[id].parent, [id]).then((d) => {
          if (d.status !== 200) {
            throw Error(d.status)
          }
          if (id === selectedTreeComp) {
            setSelectedTreeComp('')
          }
          deleteComponent(id)
        }).catch(
          err => {
            if (err.detail === undefined) {
              props.onGenericError('JavaScript-Error: ' + err.name, err.message, err.message)
            } else {
              props.onError(err)
            }
          }
        )
        break
      case 'RemovePayload':
        removeObjectsFromRunkey(metaData[id].parent, [], [id]).then((d) => {
          if (d.status !== 200) {
            throw Error(d.status)
          }
          if (id === payloadId) {
            setPayloadId('')
          }
          deleteComponent(id)
        }).catch(
          err => {
            if (err.detail === undefined) {
              props.onGenericError('JavaScript-Error: ' + err.name, err.message, err.message)
            } else {
              props.onError(err)
            }
          }
        )
        break
      case 'Payload':
        newState = 'payload'
        setPayloadId(id)
        loadPayload(id)
        break
      default:
        console.log(id, origin, event)
    }
    setState(newState)
  }
  const [TagTreeLayer, TagTreePayload] = tagTreeElements()
  const validTag = curTagData !== 'Error loading tag data!'
  const treeComp = (
    <Tree
      tree={tree}
      expandedMap={expandedMap}
      toggleComp={toggleComp}
      metaData={metaData}
      createLevel={(id, metaData, content, expandedMap, toggleComp, onCompSelect, isTitleButton) =>
        TagTreeLayer(id, metaData, content, expandedMap, onCompSelect, true, getConfig('showId'))}
      createLowestLevel={(id, metaData, onCompSelect) => TagTreePayload(id, metaData, onCompSelect, true, getConfig('showId'))}
      onCompSelect={onCompSelect}
      isTitleButton={false}
    />
  )

  useEffect(() => {
    loadTag(props.searchParams.name, props.searchParams.type, true)
  }, [props.searchParams, props.dbApiUrl])

  useEffect(() => {
    if (validTag) {
      loadTree(curTagData)
    }
  }, [
    curTagData
  ])

  useEffect(() => {
    addSubTree(lastObject, metaData && lastObject && metaData[lastObject.id] && metaData[lastObject.id].parent)
  }, [
    lastObject
  ])

  // React components
  const title = (
    <PanelTitle
      title={'Tag Tree Editor'}
      subtext={`Modify the ${curTagType} created by ${curTagAuthor}\n\n${curTagComment}`}
      dbState={props.dbState}
      dbStateStatus={props.dbStateStatus}
      dbApiUrl={props.dbApiUrl}
      onError={props.onError}
      onInfo={props.onInfo}
      updateState={props.updateState}
    >
    </PanelTitle>
  )

  const invalidTagPage = (
    <Page>
      <PanelTitle
        title={'Something went wrong'}
        subtext={`The ${props.searchParams.type}: ${props.searchParams.name} could not be found.`}
        dbState={props.dbState}
        dbStateStatus={props.dbStateStatus}
        dbApiUrl={props.dbApiUrl}
        onError={props.onError}
        onInfo={props.onInfo}
        updateState={props.updateState}
      ></PanelTitle>
      <PageSection
        isFilled
        variant={PageSectionVariants.light}
      >
        <Bullseye>
          <Text>
            Please try <a onClick={
              (e) => {
                window.location.reload()
                return false
              }}>again</a> or go <a onClick={
                (e) => {
                  window.close()
                  return false
                }}>back to the Tag List</a>
          </Text>
        </Bullseye>
      </PageSection>
    </Page>
  )

  const titleRef = document.getElementById(title.props.title)
  const navbarRef = document.getElementById('main page navbar')
  const commitButtonRef = document.getElementById('commitButton')
  const height = (titleRef ? titleRef.clientHeight : 0) + (navbarRef ? navbarRef.clientHeight : 0) + 36 + (commitButtonRef ? commitButtonRef.clientHeight : 0)
  const referenceHeight = document.getElementById('editor side') ? document.getElementById('editor side').clientHeight : 0
  return (
    validTag
      ? <Page>
        {title}
        <PageSection
          isFilled
          variant={PageSectionVariants.light}
        >
          <Flex
            style={{
              flexDirection: 'column',
              gap: '10px',
              alignContent: 'flex-start'
            }}
          >
            <FlexItem id='commitButton'>
              {commitButton}
            </FlexItem>
            <FlexItem
              style={{
                width: '100%',
                justifyContent: 'space-between'
              }}
            >
              <Flex
                style={{
                  width: '100%'
                }}
              >
                <Flex
                  direction={{ default: 'column' }}
                  style={{
                    height: `calc(100vh - ${height}px - var(--pf-c-page__main-section--PaddingBottom) - var(--pf-c-page__main-section--PaddingTop))`,
                    minHeight: `${referenceHeight}px`,
                    overflowY: 'auto',
                    flexWrap: 'nowrap',
                  }}
                >
                  <FlexItem>
                    <Text
                      style={{
                        color: 'RoyalBlue',
                        fontSize: '20px',
                        whiteSpace: 'pre-wrap'
                      }}
                    >
                      Tree Overview
                    </Text>
                  </FlexItem>
                  <FlexItem>
                    {treeComp}
                  </FlexItem>
                </Flex>
                <FlexItem
                  id='editor side'
                  style={{
                    flexGrow: 1
                  }}
                >
                  {treeCompEditor}
                  {newCompModal}
                  {newPayloadModal}
                </FlexItem>
              </Flex>
            </FlexItem>
          </Flex>
        </PageSection>
      </Page>
      : invalidTagPage
  )
}

TreeEditorPanel.propTypes = {
  dbApiUrl: PropTypes.string.isRequired,
  onError: PropTypes.func.isRequired,
  onGenericError: PropTypes.func.isRequired,
  onWarn: PropTypes.func.isRequired,
  onInfo: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  deletableInfo: PropTypes.func.isRequired,
  dbState: PropTypes.string.isRequired,
  dbStateStatus: PropTypes.string,
  updateState: PropTypes.func.isRequired,
  searchParams: PropTypes.object.isRequired
}

export default TreeEditorPanel
