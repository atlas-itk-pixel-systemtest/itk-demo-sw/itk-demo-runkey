import React, { useEffect, useRef, useState } from 'react' //, { useEffect, useState } from 'react'able
import {
  Button,
  Flex,
  FlexItem,
  Page,
  PageSection,
  PageSectionVariants,
  Text
} from '@patternfly/react-core'
import { PlusIcon, CloneIcon } from '@patternfly/react-icons'
// Components
import TagTable from '../components/tag-table'
import PanelTitle from '../components/panel-title'
import Input from '../components/input'
// Hooks
import useTagList from '../hooks/useTagList'
import useRunkeyWriter from '../hooks/useRunkeyWriter'
import useInput from '../hooks/useInput'
// Proptypes
import PropTypes from 'prop-types'

const TagPanel = (props) => {
  const navigate = (args = {}) => {
    const target = Object.entries(args).reduce((acc, [key, value]) => {
      return acc + `${key}=${value}&`
    }, '/?')
    window.open(target + `dbUrl=${props.dbApiUrl}&apiUrl=${props.backendApiUrl}`)
  }

  const deletableInfo = useRef(props.deletableInfo)
  deletableInfo.current = props.deletableInfo
  // States and custom Hooks
  const [tagList, updateTagList] = useTagList(props.dbApiUrl, props.onError, props.onGenericError, props.updateState, true, props.stagingArea !== undefined)
  const [dbTagList, dbUpdateTagList] = useTagList(props.dbApiUrl, props.onError, props.onGenericError, props.updateState, false, props.stagingArea !== undefined)

  const { createNewRunkey, cloneRunkey, deleteRunkey } = useRunkeyWriter(props.dbApiUrl, props.onError, props.onGenericError, props.updateState, updateTagList)

  const [isCreateRunkeyModalVisible, setIsCreateRunkeyModalVisible] = useState(false)
  const [getNewRunkeySettings, setNewRunkeySettings, resetNewRunkey, newRunkey] = useInput([{ name: 'Name' }, { name: 'Type', placeHolder: 'runkey' }, { name: 'Author' }, { name: 'Comment', isRequired: false }, { name: 'Open', placeHolder: true, type: 'boolean', isRequired: false }])

  const [isCloneRunkeyModalVisible, setIsCloneRunkeyModalVisible] = useState(false)
  const [getClonedRunkeySettings, setClonedRunkeySettings, resetClonedRunkey, clonedRunkey] = useInput([{ name: 'Name' }, { name: 'Type' }, { name: 'Author' }, { name: 'Comment', isRequired: false }, { name: 'Open', placeHolder: true, type: 'boolean', isRequired: false }])
  const [runkeyToBeCloned, setRunkeyToBeCloned] = useState({})

  const [isConfirmDeleteModalVisible, setIsConfirmDeleteModalVisible] = useState(false)
  const [runkeyToBeDeleted, setRunkeyToBeDeleted] = useState('')

  useEffect(() => {
    const updateTagLists = () => {
      updateTagList(props.stagingArea !== undefined)
      dbUpdateTagList(props.stagingArea !== undefined)
    }

    window.addEventListener('focus', updateTagLists)
    return () => window.removeEventListener('focus', updateTagLists)
  }, [props.dbApiUrl])

  const handleCloseCloneRunkeyModal = (variant) => {
    if (variant === 'cancel') {
      setIsCloneRunkeyModalVisible(false)
      setRunkeyToBeCloned({})
      resetClonedRunkey()
    } else if (variant === 'confirm') {
      const deleteInfo = deletableInfo.current(`Cloning the runkey ${runkeyToBeCloned.name} as ${getClonedRunkeySettings('Name')}`)
      cloneRunkey(runkeyToBeCloned.name, getClonedRunkeySettings('Name'), getClonedRunkeySettings('Comment'), getClonedRunkeySettings('Author'), getClonedRunkeySettings('Type')).then(({ status, id }) => {
        deleteInfo(`Successfully cloned the runkey ${runkeyToBeCloned.name} as ${getClonedRunkeySettings('Name')}`)
        if (status === 200) {
          setIsCloneRunkeyModalVisible(false)
          setRunkeyToBeCloned({})
          resetClonedRunkey()
          if (getClonedRunkeySettings('Open')) {
            navigate({ path: 'editor', name: getClonedRunkeySettings('Name'), type: getClonedRunkeySettings('Type') })
          } else {
            updateTagList()
          }
        }
      }).catch(
        err => {
          deleteInfo(err)
        }
      )
    }
  }

  const handleCloseNewRunkeyModal = (variant) => {
    if (variant === 'cancel') {
      setIsCreateRunkeyModalVisible(false)
      resetNewRunkey()
    } else if (variant === 'confirm') {
      createNewRunkey(getNewRunkeySettings('Name'), getNewRunkeySettings('Comment'), getNewRunkeySettings('Author'), getNewRunkeySettings('Type')).then(success => {
        if (success) {
          setIsCreateRunkeyModalVisible(false)
          resetNewRunkey()
          getNewRunkeySettings('Open') && navigate({ path: 'editor', name: getNewRunkeySettings('Name'), type: getNewRunkeySettings('Type') })
        }
      })
    }
  }

  const handleCloseConfirmDeleteModal = (variant) => {
    if (variant === 'cancel') {
      setIsConfirmDeleteModalVisible(false)
    } else if (variant === 'confirm') {
      setIsConfirmDeleteModalVisible(false)
      const deleteInfo = deletableInfo.current(`Deleting the runkey ${runkeyToBeDeleted}`)
      deleteRunkey(runkeyToBeDeleted).then((response) => {
        if (response.status === 200) {
          deleteInfo(`Successfully deleted the runkey ${runkeyToBeDeleted}`, 2000)
          updateTagList()
        } else {
          console.error(response)
          deleteInfo(response)
        }
      }).catch(
        err => {
          deleteInfo(err)
        }
      )
    }
    setRunkeyToBeDeleted('')
  }

  // React components
  const subtext = props.stagingArea === true ? 'Edit runkeys in the staging area' : props.stagingArea === false ? 'View runkeys in the database or clone them to the staging area' : 'Edit runkeys in the staging area or clone them from the database in the staging area.'
  const title = (
    <PanelTitle
      title={'Runkey Access'}
      subtext={subtext}
      dbState={props.dbState}
      dbStateStatus={props.dbStateStatus}
      dbApiUrl={props.dbApiUrl}
      onError={props.onError}
      onInfo={props.onInfo}
      updateState={props.updateState}
    />
  )

  const tagTable = (
    <TagTable
      tagList={tagList}
      tagTypes={props.tagTypes}
      onRowClick={(row) => {
        console.log(row)
        navigate({ path: 'editor', name: row[0].key, type: row[1] })
      }}
      updateTagList={updateTagList}
      iconAction={(row) => {
        setIsConfirmDeleteModalVisible(true)
        setRunkeyToBeDeleted(row[0])
      }}
      fullPage={props.stagingArea !== undefined}
    />
  )
  const dbTagTable = (
    <TagTable
      tagList={dbTagList}
      tagTypes={props.tagTypes}
      onRowClick={(row) => {
        console.log(row)
        navigate({ path: 'view', name: row[0].key, type: row[1] })
      }}
      iconAction={(row) => {
        setClonedRunkeySettings('Name', row[0])
        setClonedRunkeySettings('Comment', row[3])
        setClonedRunkeySettings('Type', row[1])
        setIsCloneRunkeyModalVisible(true)
        setRunkeyToBeCloned({ name: row[0], type: row[1] })
      }}
      icon={CloneIcon}
      updateTagList={dbUpdateTagList}
      fullPage={props.stagingArea !== undefined}
    />
  )

  const cloneRunkeyModal = (
    <Input
      isVisible={isCloneRunkeyModalVisible}
      title={`Clone the Runkey: ${runkeyToBeCloned.name}`}
      values={clonedRunkey}
      type='modal'
      setValues={setClonedRunkeySettings}
      valueDescriptions={{
        Name: 'name of the new runkey',
        Open: 'Open the Runkey automatically?'
      }}
      handleClose={handleCloseCloneRunkeyModal}
    ></Input>
  )

  const confirmDeleteModal = (
    <Input
      isVisible={isConfirmDeleteModalVisible}
      title={`Delete the runkey: ${runkeyToBeDeleted}?`}
      values={{}}
      type='modal'
      setValues={() => { }}
      valueDescriptions={{
      }}
      handleClose={handleCloseConfirmDeleteModal}
    ></Input>
  )

  const newRunkeyButton = (
    <>
      <Button
        style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          gap: '5px',
          marginTop: '0.8rem'
        }}
        onClick={() => {
          setIsCreateRunkeyModalVisible(true)
        }}
      >
        <PlusIcon></PlusIcon>
        <Text>
          Create new Runkey
        </Text>
      </Button>
      <Input
        isVisible={isCreateRunkeyModalVisible}
        title={'Create new Runkey'}
        values={newRunkey}
        type='modal'
        setValues={setNewRunkeySettings}
        valueDescriptions={{
          Name: 'name of the new runkey',
          Open: 'Open the Runkey automatically?'
        }}
        handleClose={handleCloseNewRunkeyModal}
      ></Input>
    </>
  )

  return (
    <Page>
      {title}
      <PageSection
        isFilled
        variant={PageSectionVariants.light}
      >
        <Flex
          style={{
            flexDirection: 'row',
            flexWrap: 'nowrap',
            justifyContent: 'space-around'
          }}
        >
          {props.stagingArea !== false && <Flex
            style={{
              flexDirection: 'column',
              width: props.stagingArea === true ? '100%' : '45%'
            }}
          >
            <Flex
              style={{
                alignItems: 'flex-end',
                gap: '50px'
              }}
            >
              <FlexItem>
                <Text
                  style={{
                    color: 'RoyalBlue',
                    fontSize: '20px',
                    whiteSpace: 'pre-wrap'
                  }}
                >
                  Staging Area
                </Text>
              </FlexItem>
              <FlexItem
                style={{
                  alignSelf: 'center'
                }}
              >
                {newRunkeyButton}
              </FlexItem>
            </Flex>
            <FlexItem
              style={{
                width: '100%'
              }}
            >
              {tagTable}
            </FlexItem>
          </Flex>}
          {props.stagingArea !== true && <Flex
            style={{
              width: props.stagingArea === false ? '100%' : '45%'
            }}
          >
            <FlexItem>
              <Text
                style={{
                  color: 'RoyalBlue',
                  fontSize: '20px',
                  whiteSpace: 'pre-wrap'
                }}
              >
                Database
              </Text>
            </FlexItem>
            <FlexItem
              style={{
                width: '100%'
              }}
            >
              {dbTagTable}
            </FlexItem>
          </Flex>}
          <FlexItem
            style={{
              alignSelf: 'center'
            }}
          >
            {cloneRunkeyModal}
            {confirmDeleteModal}
          </FlexItem>
        </Flex>
      </PageSection>
    </Page >
  )
}

TagPanel.propTypes = {
  tagTypes: PropTypes.array.isRequired,
  dbApiUrl: PropTypes.string.isRequired,
  backendApiUrl: PropTypes.string.isRequired,
  onError: PropTypes.func.isRequired,
  onGenericError: PropTypes.func.isRequired,
  onWarn: PropTypes.func.isRequired,
  onInfo: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  deletableInfo: PropTypes.func.isRequired,
  dbState: PropTypes.string.isRequired,
  dbStateStatus: PropTypes.string,
  updateState: PropTypes.func.isRequired,
  stagingArea: PropTypes.bool
}

export default TagPanel
