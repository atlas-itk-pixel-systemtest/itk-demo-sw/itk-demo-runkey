import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import useTagList from '../hooks/useTagList'
import { Bullseye, Flex, FlexItem, Page, PageSection, PageSectionVariants, Select, SelectOption, SelectVariant, Spinner, Text } from '@patternfly/react-core'
import PanelTitle from '../components/panel-title'
import useWebSocket, { ReadyState } from 'react-use-websocket'
import { generalRequest } from '@itk-demo-sw/utility-functions'
import createModelFromTree from '../topology/createGraph'
import { GRAPH_LAYOUT_END_EVENT, SELECTION_EVENT, TopologyView, Visualization, VisualizationProvider, VisualizationSurface } from '@patternfly/react-topology'
import { componentFactory, layoutFactory } from '../topology/topologyFactories'

const RunkeyManagerPanel = (props) => {
  const [runkey, setRunkey] = React.useState('')
  const [serviceStates, setServiceStates] = React.useState({})
  const [isErrorState, setIsErrorState] = React.useState(false)
  const [managerState, setManagerState] = React.useState('')
  console.log('runkey', runkey, 'serviceStates', serviceStates, 'managerState', managerState)

  const [tagList, updateTagList] = useTagList(props.dbApiUrl, props.onError, props.onGenericError, props.updateState, false, true)
  useEffect(() => {
    updateTagList(true)
  }, [props.dbApiUrl])
  const [treeConfig, setTreeConfig] = React.useState(null)
  useEffect(() => {
    fetch('mod_configs/topologyConfig.json', { cache: 'no-cache' }).then((response) => response.json()).then((data) => {
      setTreeConfig(data)
    })
  }, [])

  const [tree, setTree] = React.useState(null)
  useEffect(() => {
    props.dbApiUrl &&
      runkey &&
      generalRequest(`${props.dbApiUrl}/tag/tree?${new URLSearchParams({
        stage: false,
        depth: -1,
        payload_data: false,
        name: runkey,
      })}`).then((data) => {
        const root = { type: 'runkey', children: data.objects || [], metadata: {}, id: data.id }
        setTree(root)
      }).catch((e) => {
        console.error(e)
        props.onError({ status: 404, title: 'Runkey Not Found', detail: `The active runkey '${runkey}' of the runkey manager could not be found. Possibly its state is corrupted.` })
      })
  }, [props.dbApiUrl, runkey])
  console.log('tree', tree)

  const [controller, nodeIdMap] = React.useMemo(() => {
    const { model, nodeIdMap } = createModelFromTree(tree, treeConfig)
    const newController = new Visualization()
    newController.registerLayoutFactory(layoutFactory)
    newController.registerComponentFactory(componentFactory)

    const fitGraph = () => {
      const graph = newController.getGraph()
      graph.fit(80)
    }
    newController.addEventListener(GRAPH_LAYOUT_END_EVENT, fitGraph)
    newController.fromModel(model, false)
    return [newController, nodeIdMap]
  }, [tree, treeConfig])

  const [prevMessageId, setPrevMessageId] = React.useState(undefined)
  const { sendJsonMessage, lastJsonMessage, readyState } = useWebSocket(props.backendApiUrl + '/ws')
  const prevReadyState = React.useRef(readyState)

  console.log('lastJsonMessage', lastJsonMessage, 'readyState', ReadyState[readyState])
  if (lastJsonMessage?.id !== prevMessageId) {
    setPrevMessageId(lastJsonMessage?.id)
    switch (lastJsonMessage?.event) {
      case 'init': {
        setRunkey(lastJsonMessage.data.runkey)
        setServiceStates(lastJsonMessage.data.services)
        const [state, error] = lastJsonMessage.data.state.split('_')
        const isError = error === 'error'
        setManagerState(state)
        setIsErrorState(isError)
        break
      }
      case 'state_changed': {
        const [state, error] = lastJsonMessage.data.split('_')
        const isError = error === 'error'
        setManagerState(state)
        setIsErrorState(isError)
        break
      }
      case 'node_finished':
        // controller.elements[nodeIdMap[lastJsonMessage.data.node]].status = lastJsonMessage.data.success ? 'success' : 'danger'
        setServiceStates((prev) => {
          const next = { ...prev }
          next[lastJsonMessage.data.node] = lastJsonMessage.data.success || lastJsonMessage.data.error
          console.warn('NODE_FINISHED', nodeIdMap[lastJsonMessage.data.node], lastJsonMessage.data.success, lastJsonMessage.data.error)
          return next
        })
        if (!lastJsonMessage.data.success) {
          props.onError({ status: 500, title: 'Configuration Failed', detail: `Node ${nodeIdMap[lastJsonMessage.data.node]} failed with error: ${lastJsonMessage.data.error}` })
        }
        break
      default:
        console.warn('Unhandled websocket message', lastJsonMessage)
      break
    }
  }

  const [runkeySelectionOpen, setRunkeySelectionOpen] = React.useState(false)
  let component
  switch (readyState) {
    case ReadyState.CONNECTING:
      component = (
        <Bullseye>
          <Flex
            style={{
              alignItems: 'center',
              flexDirection: 'column',
              gap: '15px'
            }}
          >
            <Spinner size='xl' />
            <Text>Connecting to the runkey manager...</Text>
          </Flex>
        </Bullseye>
      )
      break
    case ReadyState.CLOSED:
      component = (
        <Bullseye>
          <Flex
            style={{
              alignItems: 'center',
              flexDirection: 'column',
              gap: '15px'
            }}
          >
            <Text
              key={'title text'}
              component="h1"
              style={{
                color: 'RoyalBlue',
                fontSize: '20px',
                display: 'inline-block',
                whiteSpace: 'pre-wrap'
              }}
            >
              Something went wrong
            </Text>
            <Text
              key={'subtitle text'}
              component="p"
            >
              The connection to the runkey manager at <a href={`${props.backendApiUrl}`} target='_blank' rel='noreferrer'>{`${props.backendApiUrl}`}</a>
              {prevReadyState.current === ReadyState.CONNECTING
                ? ' could not be established.'
                : prevReadyState.current === ReadyState.CLOSING
                ? ' was closed.'
                : ' was lost.'
              }
            </Text>
          </Flex>
        </Bullseye>
      )
      break
  }

  useEffect(() => {
    Object.entries(serviceStates).forEach(([node, state]) => {
      const n = controller.elements[nodeIdMap[node]]
      if (n) {
        n.status = state === true ? 'success' : state === false ? 'warning' : 'danger'
      }
    })
  }, [controller, nodeIdMap, serviceStates, props.onError])

  const TopologyVisualizer = () => {
    return (
      <VisualizationProvider controller={controller}>
        <VisualizationSurface/>
      </VisualizationProvider>
    )
  }

  let subtext = ''
  if (component === undefined) {
    component = (
      <Flex
        style={{
          gap: '15px',
          height: '100%',
          alignItems: 'stretch',
          flexDirection: 'column',
          width: '100%'
        }}
      >
        <Select
          variant={SelectVariant.typeahead}
          aria-label='Runkey select'
          onToggle={() => setRunkeySelectionOpen(!runkeySelectionOpen)}
          onSelect={(_, selection) => {
            sendJsonMessage({ event: 'set_runkey', data: { runkey: selection } })
            setRunkeySelectionOpen(false)
          }}
          selections={runkey}
          isOpen={runkeySelectionOpen}
          isDisabled={managerState === 'configuring'}
          placeholderText='Select a runkey to use it as configuration'
        >
          {tagList.map((tag, i) => (
            <SelectOption
              key={i}
              value={tag[0]}
              description={`"${tag[3]}"${tag[4].length > 0 ? ` by ${tag[4]}` : ''}`}
            />
           ))}
        </Select>
        <FlexItem
          style={{
            flexGrow: 1,
          }}
        >
          <TopologyView onClick={() => {
            controller.fireEvent(SELECTION_EVENT, [])
          }}>
            <TopologyVisualizer/>
          </TopologyView>
        </FlexItem>
      </Flex>
    )
    // if (lastJsonMessage?.event === 'init') {
    // }
    switch (managerState) {
      case 'unconfigured':
        subtext = 'The system is not yet configured'
        break
      case 'configured':
        subtext = `The system is configured with runkey ${runkey}`
        break
      case 'configuring':
        subtext = `Configuring the system with runkey ${runkey}...`
        break
      default:
        component = (
          <Bullseye>
            <Text>Runkey manager is in state {managerState}</Text>
          </Bullseye>
        )
        break
    }
  }
  if (isErrorState) {
    subtext += '.\nSome nodes have failed'
  }

  const title = (
    <PanelTitle
      title={'Runkey Manager'}
      subtext={subtext}
      dbState={props.dbState}
      dbStateStatus={props.dbStateStatus}
      dbApiUrl={props.dbApiUrl}
      onError={props.onError}
      onInfo={props.onInfo}
      updateState={props.updateState}
    />
  )

  if (readyState !== prevReadyState.current) {
    prevReadyState.current = readyState
  }
  return (
    <Page>
      {title}
      <PageSection
        isFilled
        variant={PageSectionVariants.light}
        style={{
          background: isErrorState ? 'linear-gradient(white,rgb(255, 220, 220))' : '',
        }}
      >
        {component}
      </PageSection>
    </Page>
  )
}

RunkeyManagerPanel.propTypes = {
  backendApiUrl: PropTypes.string.isRequired,
  dbApiUrl: PropTypes.string.isRequired,
  onError: PropTypes.func.isRequired,
  onGenericError: PropTypes.func.isRequired,
  onWarn: PropTypes.func.isRequired,
  onInfo: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  deletableInfo: PropTypes.func.isRequired,
  dbState: PropTypes.string.isRequired,
  dbStateStatus: PropTypes.string,
  updateState: PropTypes.func.isRequired,
}

export default RunkeyManagerPanel
