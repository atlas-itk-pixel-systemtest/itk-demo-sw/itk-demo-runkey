import React, { useEffect } from 'react' //, { useEffect, useState } from 'react'able
import {
  Flex,
  FlexItem,
  Page,
  PageSection,
  PageSectionVariants
} from '@patternfly/react-core'
// Components
import { Tree } from '@itk-demo-sw/components'
import PayloadView from '../components/payload-view'
import PanelTitle from '../components/panel-title'
import tagTreeElements from '../components/tag-tree-elements'
// Hooks
// import { useTree } from '@itk-demo-sw/hooks'
import useTree from '../hooks/useTree'
import useTagLoader from '../hooks/useTagLoader'
import usePayloadLoader from '../hooks/usePayloadLoader'
import { useConfig } from '../admin-panel/config'
// Proptypes
import PropTypes from 'prop-types'

const TreeViewPanel = (props) => {
  const { getConfig } = useConfig()
  // const navigate = useNavigate()
  // States and custom Hooks
  const meta = ['serial']
  const [
    curTagData,
    ,
    curTagType,
    curTagComment,
    curTagAuthor,
    loadTag,
    loadObject,
    lastObject
  ] = useTagLoader(props.dbApiUrl, props.onError, meta, false)
  const [
    curPayloadData,
    curPayload,
    curPayloadType,
    isPayloadMetaData,
    loadPayload,
    curPayloadId,
  ] = usePayloadLoader(props.dbApiUrl, props.onError, false)

  const metaDataAttributes = ['type', 'name', ...meta]
  const {
    loadTree,
    toggleComp,
    tree,
    metaData,
    expandedMap,
    addSubTree,
    hasComponentChildren
  } = useTree({ metaDataAttributes, sortBy: metaDataAttributes[0], features: ['dynamic'] })
  const [TagTreeLayer, TagTreePayload] = tagTreeElements()
  const validTag = curTagData !== 'Error loading tag data!'
  const treeComp = (
    <Tree
      tree={tree}
      expandedMap={expandedMap}
      toggleComp={toggleComp}
      metaData={metaData}
      createLevel={(id, metaData, content, expandedMap, toggleComp, onCompSelect, isTitleButton) =>
        TagTreeLayer(id, metaData, content, expandedMap, onCompSelect, false, getConfig('showId'))}
      createLowestLevel={(id, metaData, onCompSelect) => TagTreePayload(id, metaData, onCompSelect, false, getConfig('showId'))}
      onCompSelect={(id, origin, event) => {
        switch (origin) {
          case 'Toggle':
            if (!hasComponentChildren(id)) {
              loadObject(id)
            }
            toggleComp(id)
            break
          case 'Payload':
            loadPayload(id)
            break
        }
      }}
      isTitleButton={false}
    />
  )

  useEffect(() => {
    loadTag(props.searchParams.name, props.searchParams.type, true)
  }, [props.searchParams, props.dbApiUrl])

  useEffect(() => {
    if (validTag) {
      loadTree(curTagData)
    }
  }, [
    curTagData
  ])

  useEffect(() => {
    addSubTree(lastObject, metaData && lastObject && metaData[lastObject.id] && metaData[lastObject.id].parent)
  }, [
    lastObject
  ])

  // React components
  const title = (
    <PanelTitle
      title={'Tag Tree'}
      subtext={`Inspect the ${curTagType} created by ${curTagAuthor}\n\n${curTagComment}`}
      dbState={props.dbState}
      dbStateStatus={props.dbStateStatus}
      dbApiUrl={props.dbApiUrl}
      onError={props.onError}
      onInfo={props.onInfo}
      updateState={props.updateState}
    />
  )

  const navbarRef = document.getElementById('main page navbar')
  const height = (navbarRef ? navbarRef.clientHeight : 0) + 36
  const fullPageHeightStyle = {
    height: `calc(100vh - ${height}px - var(--pf-c-page__main-section--PaddingBottom) - var(--pf-c-page__main-section--PaddingTop))`,
  }
  return (
    <Page>
      {title}
      <PageSection
        isFilled
        variant={PageSectionVariants.light}
      >
        <Flex>
          <Flex style = {{
            overflowY: 'auto',
            ...fullPageHeightStyle
            }}>
            <FlexItem>
              {treeComp}
            </FlexItem>
          </Flex>
          <Flex
            direction={{ default: 'column' }}
            grow={{ default: 'grow' }}
          >
            <PayloadView
              dbApiUrl={props.dbApiUrl}
              jsrootUrl={props.jsrootUrl}
              curPayloadData={curPayloadData}
              curPayloadTag={curPayload}
              curPayloadType={curPayloadType}
              isPayloadMetaData={isPayloadMetaData}
              curPayloadId={curPayloadId}
              style={fullPageHeightStyle}
            />
          </Flex>
        </Flex>
      </PageSection>
    </Page>
  )
}

TreeViewPanel.propTypes = {
  dbApiUrl: PropTypes.string.isRequired,
  jsrootUrl: PropTypes.string.isRequired,
  onError: PropTypes.func.isRequired,
  onGenericError: PropTypes.func.isRequired,
  onWarn: PropTypes.func.isRequired,
  onInfo: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  dbState: PropTypes.string.isRequired,
  dbStateStatus: PropTypes.string,
  updateState: PropTypes.func.isRequired,
  searchParams: PropTypes.object.isRequired
}

export default TreeViewPanel
