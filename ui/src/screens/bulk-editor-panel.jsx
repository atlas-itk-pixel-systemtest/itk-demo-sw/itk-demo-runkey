import React, { useEffect, useMemo, useRef, useState } from 'react'
import PropTypes from 'prop-types'
import { Page, PageSection, PageSectionVariants, Text, Button, Bullseye, Tooltip, DataList, DataListItem, DataListItemRow, DataListCheck, DataListItemCells, DataListContent, DataListToggle, FlexItem, Flex, TextInput } from '@patternfly/react-core'
import PanelTitle from '../components/panel-title'

import useTagLoader from '../hooks/useTagLoader'
import useInput from '../hooks/useInput'
import Input from '../components/input'
import { useConfig } from '../admin-panel/config'
import { generalRequest } from '@itk-demo-sw/utility-functions'

const pathDivider = '~.~'

const BulkEditorPanel = (props) => {
  const { getConfig } = useConfig()
  const [
    curTagData,
    ,
    curTagType,
    ,
    ,
    loadTag
  ] = useTagLoader(props.dbApiUrl, props.onError, ['serial'])
  useEffect(() => {
    loadTag(props.searchParams.name, props.searchParams.type, true, -1)
  }, [props.searchParams, props.dbApiUrl])

  const [payloadData, setPayloadData] = React.useState(undefined)
  const [transition, setTransition] = React.useState(undefined)

  const [expandedMap, setExpandedMap] = React.useState({})
  const [selectedComps, setSelectedComps] = React.useState({})
  const validTag = curTagData !== 'Error loading tag data!'
  const payloadTypesForObjectTypes = useMemo(() => {
    if (transition) {
      return transition.payloadTypesForObjectTypes
    }
    const objectTypes = {}
    const expanded = {}
    const selected = {}
    const getPayloads = (data) => {
      expanded[data.id] = false
      selected[data.id] = false
      if (data.payloads && data.payloads.length > 0) {
        if (!(data.type in objectTypes)) {
          objectTypes[data.type] = []
        }
        for (const payload of data.payloads) {
          const payloadId = `${data.id}${pathDivider}${payload.id}`
          selected[payloadId] = false
          if (!objectTypes[data.type].includes(payload.type)) {
            objectTypes[data.type].push(payload.type)
          }
        }
      }
      data.children && data.children.forEach((child) => getPayloads(child))
    }
    getPayloads(curTagData)
    setSelectedComps(selected)
    setExpandedMap(expanded)
    return objectTypes
  }, [curTagData])

  const onObjectTypeSelect = useRef(() => { })
  const onPayloadTypeSelect = useRef(() => { })
  const [, setPayloadList, , payloadList] = useInput([
    {
      name: 'objectType',
      type: 'select',
      onSelect: (selection) => {
        onObjectTypeSelect.current(selection)
      }
    },
    {
      name: 'payloadType',
      type: 'select',
      onSelect: (selection) => {
        onPayloadTypeSelect.current(selection)
      }
    }])
  const payloadListComp = <Input
    isVisible={true}
    title='Payload List'
    values={payloadList}
    type='flex'
    setValues={setPayloadList}
    actions={[<Button
      key="create"
      variant="primary"
      form="modal-with-form-form"
      onClick={async () => {
        const selectedPayloads = Object.entries(selectedComps).filter(([id, value]) => value && id.includes(pathDivider)).map(([key, _]) => key.split(pathDivider)[1])
        if (selectedPayloads.length === 0) {
          setPayloadData(undefined)
          return
        }
        try {
          const query = new URLSearchParams({ ids: selectedPayloads })
          const response = await generalRequest(`${props.dbApiUrl}/payloads?${query}`)
          if (response.status !== 200) {
            props.onError({ status: response.status, title: 'Error loading payloads', detail: 'An unspecified error occurred while loading the payloads.' })
          } else {
            setPayloadData(response.bundle)
          }
        } catch (error) {
          props.onError(error)
        }
      }}
    >
      Edit Selected Payloads
    </Button>]}
    handleClose={() => { }}
  ></Input>

  const [selectedObjectType, setSelectedObjectType] = React.useState(undefined)
  const [selectedPayloadType, setSelectedPayloadType] = React.useState(undefined)
  useEffect(() => {
    const objectTypes = Object.keys(payloadTypesForObjectTypes)
    const selectedObjectType = transition ? transition.selectedObjectType : objectTypes[0]
    setPayloadList('objectType', objectTypes)
    setPayloadList('payloadType', payloadTypesForObjectTypes[selectedObjectType] || [])
    setSelectedObjectType(selectedObjectType)
    selectedObjectType && setSelectedPayloadType(transition ? transition.selectedPayloadType : payloadTypesForObjectTypes[selectedObjectType][0])
    onObjectTypeSelect.current = (selection) => {
      setTransition(undefined)
      setSelectedComps((sc) => {
        const newSelectedComps = { ...sc }
        Object.keys(newSelectedComps).forEach((id) => {
          newSelectedComps[id] = false
        })
        return newSelectedComps
      })
      setSelectedObjectType(selection)
      setPayloadList('payloadType', payloadTypesForObjectTypes[selection])
      setSelectedPayloadType(payloadTypesForObjectTypes[selection][0])
    }
    onPayloadTypeSelect.current = (selection) => {
      setTransition(undefined)
      setSelectedComps((sc) => {
        const newSelectedComps = { ...sc }
        Object.keys(newSelectedComps).forEach((id) => {
          newSelectedComps[id] = false
        })
        return newSelectedComps
      })
      setSelectedPayloadType(selection)
    }
  }, [payloadTypesForObjectTypes])

  const createTreeList = (tree, selectedObjectType, selectedPayloadType, expandedMap, selectedComps) => {
    if (!tree || !selectedObjectType || !selectedPayloadType) {
      return [<></>, false]
    }
    const name = tree.name.length > 0 ? tree.name : tree.serial
    const showId = getConfig('showId')
    const isExpanded = expandedMap[tree.id]
    let isChecked = true
    let content
    if (tree.type === selectedObjectType) {
      const payloads = tree.payloads.filter((payload) => payload.type === selectedPayloadType)
      if (payloads.length === 0) {
        isChecked = false
        content = undefined
      } else {
        content = payloads.map((payload, i) => {
          const payloadId = `${tree.id}${pathDivider}${payload.id}`
          const isPayloadChecked = selectedComps[payloadId]
          if (!isPayloadChecked) {
            isChecked = false
          }
          return [
            <DataListItem
              aria-label={`List ${payload.id}`}
              isExpanded={isExpanded}
              key={`List ${i}`}
            >
              <DataListItemRow
                style={{
                  alignItems: 'baseline'
                }}
                key={`Item Row ${i}`}
              >
                <DataListCheck checked={isPayloadChecked} onChange={() => setSelectedComps((sc) => {
                  return { ...sc, [payloadId]: !isPayloadChecked }
                })} />
                <DataListItemCells
                  style={{
                    height: '40px'
                  }}
                  key={`Item Cell ${i}`}
                  dataListCells={[
                    <Text
                      key={`type ${payload.id}`}
                      style={{
                        fontSize: '14px',
                        display: 'inline-block',
                        color: '#7d7d7d'
                      }}
                      className='monospace trunkatedIdNoHover'
                    >
                      {`${payload.type}: `}<span style={{ userSelect: 'all' }}>{payload.name}</span>
                      {showId && <>
                        <span>; #</span>
                        <Tooltip content={payload.id}><span style={{ userSelect: 'all' }}>{payload.id}</span></Tooltip>
                      </>}
                    </Text>
                  ]}
                />
              </DataListItemRow>
            </DataListItem>,
            isPayloadChecked]
        })
      }
    } else {
      content = tree.children.map((child) => {
        const [comp, isChildChecked] = createTreeList(child, selectedObjectType, selectedPayloadType, expandedMap, selectedComps)
        if (!isChildChecked) {
          isChecked = false
        }
        return comp
      }).filter((comp) => comp)
      if (content.length === 0) {
        content = undefined
      }
    }
    if (!content) {
      return [undefined, true]
    }
    return [
      <DataListItem
        aria-label={`List ${tree.id}`}
        isExpanded={isExpanded}
        key={`List ${tree.id}`}
      >
        <DataListItemRow
          style={{
            alignItems: 'baseline'
          }}
          key={`Item Row ${tree.id}`}
        >
          <DataListToggle
            onClick={() => setExpandedMap((ex) => {
              return { ...ex, [tree.id]: !isExpanded }
            })}
            isExpanded={isExpanded}
            key={`List Toggle ${tree.id}`}
          />
          <DataListCheck checked={isChecked} onChange={() => {
            setSelectedComps((sc) => {
              const newSelectedComps = { ...sc }
              const select = !isChecked
              const selectAll = (data) => {
                newSelectedComps[data.id] = select
                if (data.type === selectedObjectType) {
                  data.payloads.forEach((payload) => {
                    if (payload.type === selectedPayloadType) {
                      const payloadId = `${data.id}${pathDivider}${payload.id}`
                      newSelectedComps[payloadId] = select
                    }
                  })
                } else {
                  data.children && data.children.forEach(selectAll)
                }
              }
              selectAll(tree)
              return newSelectedComps
            })
          }} />
          <DataListItemCells
            style={{
              height: '40px'
            }}
            key={`Item Cell ${tree.id}`}
            dataListCells={[<Text
              key={`Row Title ${tree.id}`}
              style={{
                fontSize: '14px',
                color: 'black'
              }}
              className='monospace trunkatedIdNoHover'
            >
              <span style={{ color: 'navy', fontWeight: 'bold', fontFamily: 'RedHatText' }}>{tree.type}: </span>
              <Tooltip content={name}><span style={{ userSelect: 'all' }}>{name}</span></Tooltip>
              {showId && <>
                <span>; #</span>
                <Tooltip content={tree.id}><span style={{ userSelect: 'all' }}>{tree.id}</span></Tooltip>
              </>}
            </Text>
            ]}
          />
        </DataListItemRow>
        <DataListContent
          key={`List Content ${tree.id}`}
          isHidden={!isExpanded}
        >
          <DataList
            style={{
              '--pf-c-data-list__expandable-content-body--PaddingTop': '0px',
              '--pf-c-data-list__expandable-content-body--PaddingRight': '0px',
              '--pf-c-data-list__expandable-content-body--PaddingBottom': '0px'
            }}
            isCompact
            key={`${tree.id}`}
          >
            {content}
          </DataList>
        </DataListContent>
      </DataListItem >,
      isChecked]
  }
  const payloadTreeSelector = <DataList
    style={{
      marginTop: '10px',
      '--pf-c-data-list__expandable-content-body--PaddingTop': '0px',
      '--pf-c-data-list__expandable-content-body--PaddingRight': '0px',
      '--pf-c-data-list__expandable-content-body--PaddingBottom': '0px'
    }}
  >
    {createTreeList(curTagData, selectedObjectType, selectedPayloadType, expandedMap, selectedComps)[0]}
  </DataList >

  const title = (
    <PanelTitle
      title={'Tag Tree Editor - Bulk Payload Editing'}
      subtext={`Modify the payloads of multiple components from the ${curTagType} at once.`}
      dbState={props.dbState}
      dbStateStatus={props.dbStateStatus}
      dbApiUrl={props.dbApiUrl}
      onError={props.onError}
      onInfo={props.onInfo}
      updateState={props.updateState}
    >
    </PanelTitle>
  )

  const invalidTagPage = (
    <Page>
      <PanelTitle
        title={'Something went wrong'}
        subtext={`The ${props.searchParams.type}: ${props.searchParams.name} could not be found.`}
        dbState={props.dbState}
        dbStateStatus={props.dbStateStatus}
        dbApiUrl={props.dbApiUrl}
        onError={props.onError}
        onInfo={props.onInfo}
        updateState={props.updateState}
      ></PanelTitle>
      <PageSection
        isFilled
        variant={PageSectionVariants.light}
      >
        <Bullseye>
          <Text>
            Please try <a onClick={
              (e) => {
                window.location.reload()
                return false
              }}>again</a> or go <a onClick={
                (e) => {
                  window.close()
                  return false
                }}>back to the Tag List</a>
          </Text>
        </Bullseye>
      </PageSection>
    </Page>
  )

  const [newPayload, setNewPayload] = useState({})
  const [newPayloadEntryKeyPrefix, setNewPayloadEntryKeyPrefix] = useState(undefined)
  const [newPayloadEntryListLength, setNewPayloadEntryListLength] = useState(undefined)
  const payloadEntryTypes = ['value', 'object', 'list']
  const [newPayloadEntryType, setNewPayloadEntryType] = useState(payloadEntryTypes[0])
  const [getNewPayloadEntry, setNewPayloadEntry, , payloadEntry] = useInput([
    { name: 'key' },
    { name: 'value', isRequired: false },
    {
      name: 'objectType',
      type: 'select',
      onSelect: (selection) => {
        setNewPayloadEntryType(selection)
      },
      placeHolder: payloadEntryTypes
    }
  ])
  const [getModifyList, setModifyList, resetModifyList, modifyList] = useInput([
    { name: 'index' },
    { name: 'value' }
  ])
  const [isModifyListModalVisible, setModifyListModalVisible] = useState(false)
  const modifyListModal = <Input
    isVisible={isModifyListModalVisible}
    title='Modify list entry'
    values={modifyList}
    type='modal'
    setValues={setModifyList}
    handleClose={(variant) => {
      if (variant === 'confirm') {
        const newKey = getModifyList('index')
        if (newKey && newKey.length === 0) {
          props.onInfo('The key cannot be empty.')
          return
        }
        const asNumber = Number(newKey)
        if (Number.isNaN(asNumber)) {
          props.onInfo('The key must be a number.')
          return
        }
        if (asNumber >= newPayloadEntryListLength) {
          props.onInfo('The index is out of bounds. The list has a length of ' + newPayloadEntryListLength + '.')
          return
        }
        resetModifyList()
        const key = (newPayloadEntryKeyPrefix ? newPayloadEntryKeyPrefix + pathDivider : '') + newKey
        const value = getModifyList('value')
        setPayloadData((pd) => {
          const newPayloadData = { ...pd }
          let localPayloadData = newPayloadData
          const keyFragments = key.split(pathDivider)
          for (let i = 0; i < keyFragments.length - 1; i++) {
            const frag = keyFragments[i]
            if (!(frag in localPayloadData)) {
              localPayloadData[frag] = {}
            }
            localPayloadData = localPayloadData[frag]
          }
          const frag = keyFragments[keyFragments.length - 1]
          if (newPayloadEntryType === 'value') {
            localPayloadData[frag] = [value]
          } else if (newPayloadEntryType === 'object') {
            localPayloadData[frag] = {}
          } else if (newPayloadEntryType === 'list') {
            localPayloadData[frag] = []
          }
          return newPayloadData
        })
        setNewPayload((np) => {
          const newNewPayload = { ...np }
          if (newPayloadEntryType === 'value') {
            newNewPayload[key] = value
          } else if (newPayloadEntryType === 'object') {
            newNewPayload[key] = {}
          } else if (newPayloadEntryType === 'list') {
            newNewPayload[key] = []
          }
          return newNewPayload
        })
      }
      setModifyListModalVisible(false)
    }}
  />

  const [isNewPayloadEntryModalVisible, setNewPayloadEntryModalVisible] = useState(false)
  const newPayloadEntryModal = <Input
    isVisible={isNewPayloadEntryModalVisible}
    title='Create New Payload Entry'
    values={payloadEntry}
    type='modal'
    setValues={setNewPayloadEntry}
    disabledValues={{
      key: newPayloadEntryListLength !== undefined,
      value: newPayloadEntryType !== 'value'
    }}
    handleClose={(variant) => {
      if (variant === 'confirm') {
        const newKey = newPayloadEntryListLength === undefined ? getNewPayloadEntry('key') : newPayloadEntryListLength.toString()
        if (newKey && newKey.length === 0) {
          props.onInfo('The key cannot be empty.')
          return
        }
        const key = (newPayloadEntryKeyPrefix ? newPayloadEntryKeyPrefix + pathDivider : '') + newKey
        const value = getNewPayloadEntry('value')
        setPayloadData((pd) => {
          const newPayloadData = { ...pd }
          let localPayloadData = newPayloadData
          const keyFragments = key.split(pathDivider)
          for (let i = 0; i < keyFragments.length - 1; i++) {
            const frag = keyFragments[i]
            if (!(frag in localPayloadData)) {
              localPayloadData[frag] = {}
            }
            localPayloadData = localPayloadData[frag]
          }
          const frag = keyFragments[keyFragments.length - 1]
          if (newPayloadEntryType === 'value') {
            localPayloadData[frag] = [value]
          } else if (newPayloadEntryType === 'object') {
            localPayloadData[frag] = {}
          } else if (newPayloadEntryType === 'list') {
            localPayloadData[frag] = []
          }
          return newPayloadData
        })
        setNewPayload((np) => {
          const newNewPayload = { ...np }
          if (newPayloadEntryType === 'value') {
            newNewPayload[key] = value
          } else if (newPayloadEntryType === 'object') {
            newNewPayload[key] = {}
          } else if (newPayloadEntryType === 'list') {
            newNewPayload[key] = []
          }
          return newNewPayload
        })
      }
      setNewPayloadEntryModalVisible(false)
    }}
  />

  const createInputField = (path, key, value) => {
    return <Flex style={{
      flexDirection: 'row',
      gap: '5px',
      alignItems: 'center'
    }}>
      {key}: <TextInput
        style={{
          flex: '1'
        }}
        value={newPayload[path] !== undefined ? newPayload[path] : value}
        type='text'
        aria-label={path + ' input'}
        onChange={(newValue) => setNewPayload((np) => {
          return { ...np, [path]: newValue }
        })}
      />,
    </Flex>
  }
  const createLiteralArrayField = (path, key, value) => {
    const changes = Object.entries(newPayload).filter(([key, _]) => key.startsWith(path)).map(([key, value]) => {
      return <Text key={key}>
        {key.substring(path.length + pathDivider.length)}: {value}
      </Text>
    })
    return <Flex>
      <Text>{key}: </Text>
      <Button
        key='modify'
        variant='secondary'
        onClick={() => {
          setNewPayloadEntryKeyPrefix(path)
          setModifyListModalVisible(true)
          setNewPayloadEntryListLength(value.length)
        }}>Modify List</Button>
      <FlexItem style={{ paddingLeft: '20px' }}>
        <Text>Changes: </Text>
        <Flex style={{ paddingLeft: '20px', flexDirection: 'column' }}>
          {changes}
        </Flex>
      </FlexItem>
    </Flex>
  }
  const createPayload = (data, path = undefined, createObjectAddButton = true) => {
    return <Flex style={{
      flexDirection: 'column'
    }}>
      {createObjectAddButton && <Button
        key='add'
        variant='secondary'
        onClick={() => {
          setNewPayloadEntryKeyPrefix(path)
          setNewPayloadEntryModalVisible(true)
          setNewPayloadEntryListLength(undefined)
        }}>add entry</Button>}
      {Object.entries(data).map(([key, value]) => {
        const newPath = path ? path + pathDivider + key : key
        const isValue = Array.isArray(value)
        const isArrayValue = isValue && ((value.length > 0 && typeof value[0] !== 'string' && (Array.isArray(value[0]) || typeof value[0] === 'object')) || value.length === 0)
        const isLiteralArrayValue = isArrayValue && value.every((v) => typeof v[0] === 'string' || typeof v[0] === 'number' || typeof v[0] === 'boolean')
        if (isLiteralArrayValue) {
          return createLiteralArrayField(newPath, key, value)
        }
        if (!isArrayValue && isValue) {
          return createInputField(newPath, key, value.every((v) => v === value[0]) ? value[0] : '')
        }
        const content = isArrayValue
          ? value
            .map((value, key) => {
              const referenceValue = value[0]
              const isLiteral = typeof referenceValue === 'string' || typeof referenceValue === 'number' || typeof referenceValue === 'boolean'
              if (!isLiteral) {
                const isValue = Array.isArray(value)
                const isArrayValue = isValue && ((value.length > 0 && typeof value[0] !== 'string' && (Array.isArray(value[0]) || typeof value[0] === 'object')) || value.length === 0)
                const newerPath = newPath + pathDivider + key
                return <Flex key={key} style={{
                  flexDirection: 'column'
                }}>
                  <Text>{key}: {isArrayValue ? '[' : '{'}</Text>
                  <FlexItem style={{
                    paddingLeft: '10px'
                  }}>
                    {createPayload(value, newerPath, !isArrayValue)}
                    {isArrayValue
                      ? <Button
                        key='add'
                        variant='secondary'
                        onClick={() => {
                          setNewPayloadEntryKeyPrefix(newerPath)
                          setNewPayloadEntryModalVisible(true)
                          setNewPayloadEntryListLength(value.length)
                        }}>add entry</Button>
                      : <></>}
                  </FlexItem>
                  <Text>{isArrayValue ? '],' : '},'}</Text>
                </Flex>
              } else {
                return createInputField(newPath + pathDivider + key, key, value.every((v) => v === referenceValue) ? referenceValue : '')
              }
            })
          : createPayload(value, newPath)
        return <Flex key={key} style={{
          flexDirection: 'column'
        }}>
          <Text>{key}: {isArrayValue ? '[' : '{'}</Text>
          <FlexItem style={{
            paddingLeft: '10px'
          }}>
            {content}
            {isArrayValue
              ? <Button
                key='add'
                variant='secondary'
                onClick={() => {
                  setNewPayloadEntryKeyPrefix(newPath)
                  setNewPayloadEntryModalVisible(true)
                  setNewPayloadEntryListLength(value.length)
                }}>add entry</Button>
              : <></>}
          </FlexItem>
          <Text>{isArrayValue ? '],' : '},'}</Text>
        </Flex>
      })}
    </Flex>
  }
  const editor = payloadData
    ? <Flex style={{
      flexDirection: 'column',
      gap: '10px'
    }}>
      <Text
        style={{
          color: 'RoyalBlue',
          fontSize: '20px',
          whiteSpace: 'pre-wrap'
        }}
      >
        Modify Payload
      </Text>
      <Button
        onClick={() => {
          const changes = {}
          for (const [key, value] of Object.entries(newPayload)) {
            const keyFragments = key.split(pathDivider)
            let localChanges = changes
            for (let i = 0; i < keyFragments.length - 1; i++) {
              const frag = keyFragments[i]
              if (!(frag in localChanges)) {
                localChanges[frag] = {}
              }
              localChanges = localChanges[frag]
            }
            const frag = keyFragments[keyFragments.length - 1]
            const asNumber = Array.isArray(value) ? Number.NaN : Number(value)
            localChanges[frag] = Number.isNaN(asNumber) ? value : asNumber
          }

          const selectedPayloads = Object.entries(selectedComps).filter(([id, value]) => value && id.includes(pathDivider)).map(([key, _]) => {
            const [object, payload] = key.split(pathDivider)
            return { object_id: object, payload_id: payload }
          })
          // console.log('Applying changes', changes, selectedPayloads)
          generalRequest(`${props.dbApiUrl}/payloads`, { body: { changes, connections: selectedPayloads } }).then((response) => {
            if (response.status !== 200) {
              props.onError({ status: response.status, title: 'Error changing payloads', detail: 'An unspecified error occurred while changing the payloads.' })
            } else {
              props.onSuccess('The payloads have been changed successfully.')
              const newIds = response.ids
              const newPayloadIds = newIds.map((id, i) => {
                const object = selectedPayloads[i].object_id
                return `${object}${pathDivider}${id}`
              }).reduce((acc, id) => {
                acc[id] = true
                return acc
              }, {})
              setExpandedMap((ex) => {
                const newExpanded = { ...ex, ...newPayloadIds }
                for (const { object_id, payload_id } of selectedPayloads) { // eslint-disable-line camelcase
                  const id = `${object_id}${pathDivider}${payload_id}` // eslint-disable-line camelcase
                  delete newExpanded[id]
                }
                return newExpanded
              })
              setSelectedComps((sc) => {
                const newSelectedComps = { ...sc, ...newPayloadIds }
                for (const { object_id, payload_id } of selectedPayloads) { // eslint-disable-line camelcase
                  const id = `${object_id}${pathDivider}${payload_id}` // eslint-disable-line camelcase
                  delete newSelectedComps[id]
                }
                return newSelectedComps
              })
              const transitionToNewPayloads = {
                selectedObjectType,
                selectedPayloadType,
                payloadTypesForObjectTypes: { ...payloadTypesForObjectTypes }
              }
              setTransition(transitionToNewPayloads)
              setNewPayload({})
              loadTag(props.searchParams.name, props.searchParams.type, true, -1)
            }
          }).catch((error) => {
            props.onError(error)
          })
        }}
      >Apply changes</Button>
      <FlexItem style={{
        height: '100%',
        overflowY: 'auto',
        fontFamily: 'monospace'
      }}>
        {createPayload(payloadData)}
      </FlexItem>
    </Flex>
    : <Text
      style={{
        color: 'RoyalBlue',
        fontSize: '20px',
        whiteSpace: 'pre-wrap'
      }}
    >
      Select multiple Payloads in the tree to edit them at once
    </Text>

  return (
    validTag
      ? <Page>
        {title}
        <PageSection
          isFilled
          variant={PageSectionVariants.light}
        >
          {newPayloadEntryModal}
          {modifyListModal}
          <Flex style={{
            flexDirection: 'row',
            gap: '20px'
          }}>
            <FlexItem style={{
              flex: '1',
              maxWidth: '50%'
            }}>
              {payloadListComp}
              {payloadTreeSelector}
            </FlexItem>
            <FlexItem style={{
              flex: '1',
              maxWidth: '50%'
            }}>
              {editor}
            </FlexItem>
          </Flex>
        </PageSection>
      </Page>
      : invalidTagPage
  )
}

BulkEditorPanel.propTypes = {
  onInfo: PropTypes.func.isRequired,
  onWarn: PropTypes.func.isRequired,
  onError: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onGenericError: PropTypes.func.isRequired,
  dbApiUrl: PropTypes.string.isRequired,
  searchParams: PropTypes.object.isRequired,
  dbState: PropTypes.string.isRequired,
  dbStateStatus: PropTypes.string,
  updateState: PropTypes.func.isRequired,
  deletableInfo: PropTypes.func.isRequired
}

export default BulkEditorPanel
