import React from 'react'
import ReactDOM from 'react-dom/client'
import '@patternfly/react-core/dist/styles/base.css'
import './style.css'
import ConfigdbDashboard from './screens/dashboard'
import PanelBadge from './admin-panel/panel-badge'
import TreeEditorPanel from './screens/tree-editor-panel'
import TopologyEditorPanel from './screens/topology-editor-panel'
import BulkEditorPanel from './screens/bulk-editor-panel'
import TreeViewPanel from './screens/tree-view-panel'

const searchString = window.location.search
let search = { path: '/', dbUrl: null, apiUrl: null, jsrootUrl: null }
if (searchString.length > 0) {
  search = Array.from(new URLSearchParams(searchString).entries()).reduce((obj, [param, value]) => {
    obj[param] = value
    return obj
  }, {})
}
// #START_LOCAL
// search.dbUrl = 'http://localhost:33659/api'
// search.apiUrl = 'http://0.0.0.0:5000/api'
// search.apiUrl = 'http://localhost:5000/api'
search.dbUrl = 'http://daqdev11.detlab.lan:5000/api'
search.apiUrl = 'http://daqdev11.detlab.lan:5000/api'
search.jsrootUrl = 'http://daqdev11.detlab.lan:36058'
// #END_LOCAL

const sites = {
  '/':
    <div className='fill-window'>
      <ConfigdbDashboard
        path=''
        dbUrl={search.dbUrl}
        apiUrl={search.apiUrl}
        jsrootUrl={search.jsrootUrl}
      />
      <PanelBadge />
    </div>,
  editor:
    <div className='fill-window'>
      <ConfigdbDashboard
        path='editor'
        panels={[{
          title: 'Tag Editor',
          Content: TreeEditorPanel,
          props: { searchParams: search }
        },
        {
          title: 'Topology Editor',
          Content: TopologyEditorPanel,
          props: { searchParams: search }
        },
        {
          title: 'Bulk Payload Editor',
          Content: BulkEditorPanel,
          props: { searchParams: search }
        }]}
        dbUrl={search.dbUrl}
        apiUrl={search.apiUrl}
      />
      <PanelBadge />
    </div>,
  view:
    <div className='fill-window'>
      <ConfigdbDashboard
        path='view'
        panels={[{
          title: 'Tag View',
          Content: TreeViewPanel,
          props: { searchParams: search }
        }
        ]}
        dbUrl={search.dbUrl}
        apiUrl={search.apiUrl}
        jsrootUrl={search.jsrootUrl}
      />
      <PanelBadge />
    </div>
}

if (!('path' in search && search.path in sites)) window.location.search = ''

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  sites[search.path]
)
