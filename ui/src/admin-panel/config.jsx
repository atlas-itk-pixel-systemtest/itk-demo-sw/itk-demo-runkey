import createConfig from 'react-runtime-config'

export const { useConfig, useAdminConfig } = createConfig({
  namespace: 'MY_APP_CONFIG',
  schema: {
    jsroot: {
      type: 'custom',
      title: 'jsroot URL',
      description: 'jsroot URL',
      default: 'No URL provided',
      parser: (value) => {
        if (typeof value === 'string') {
          return value
        } else {
          throw new Error('Invalid backend URL')
        }
      }
    },
    api: {
      type: 'custom',
      title: 'Backend Api URL',
      description: 'Backend Api URL',
      default: 'No URL provided',
      parser: (value) => {
        if (typeof value === 'string') {
          return value
        } else {
          throw new Error('Invalid backend URL')
        }
      }
    },
    configdb: {
      type: 'custom',
      title: 'ConfigDb URL',
      description: 'ConfigDb URL',
      default: 'No URL provided',
      parser: (value) => {
        if (typeof value === 'string') {
          return value
        } else {
          throw new Error('Invalid configDb URL')
        }
      }
    },
    healthFrequency: {
      type: 'number',
      title: '/health call frequency [ms]',
      description: '/health call frequency [ms]',
      default: 1000
    },
    callHealth: {
      type: 'boolean',
      title: 'Call /health endpoint regularly',
      description: 'Call /health endpoint regularly',
      default: true
    },
    showId: {
      type: 'boolean',
      title: 'Show ID',
      description: 'Show IDs for each component in the tree',
      default: false
    }
  }
})
