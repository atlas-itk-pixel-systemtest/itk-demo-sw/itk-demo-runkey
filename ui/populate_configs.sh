#!/bin/sh

# Variables
TARGET_DIR="/usr/share/nginx/html/mod_configs"
DEFAULT_DIR="/default_setup/default_configs"

# Iterate over all files in DEFAULT_DIR
for DEFAULT_FILE in "$DEFAULT_DIR"/*; do
  # Extract the filename from the path
  FILENAME=$(basename "$DEFAULT_FILE")
  TARGET_FILE="$TARGET_DIR/$FILENAME"

  # Check if the target file exists
  if [ ! -f "$TARGET_FILE" ]; then
    echo "File $TARGET_FILE does not exist. Copying default file..."
    cp "$DEFAULT_FILE" "$TARGET_FILE"
    echo "Default file copied to $TARGET_FILE."
  fi
done