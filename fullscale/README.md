# Scale Example
This directory provides some scripts to easily create a scalable runkey manager example.

## Prerequisite
The dummy container is not officially released for now, so it is necessary to firstly build it (assuming pwd==scale_example):
```
cd dummy && docker compose build
cd -
```
The same is true for the runkey manager with performance messages. To build it 
```
cd ../api && docker compose build
cd -
```

## Usage
### Setup
First the environment needs to be set up:
```
./config
```
To configure the runkey that will be created the `fullscale/config.py` can be edited. It describes which hosts are available to run how many services and how the runkey is structured.

To create the runkey run the `create_runkey.sh` script. The `RUNKEY` environment variable can be used to name the runkey.

Furthermore the script creates a compose file for every host and the runkey. The containers will connect to the runkey manager stack on start up, so it is necessary to start it first. To start the dummy containers, copy the compose file onto the correct host and make sure the docker network deminet exist on that host:
```
docker network create deminet
```
and run the file with docker compose on that host:
```
docker compose -f [HOST]_[RUNKEY]_compose.yaml up -d
```

Lastly the script assumes, that the system it is called on will also be the system running the runkey manager. This generates a `.env` file with some configurations needed to run the runkey manager stack. The `STACK` and `SR_URL` variables are also used in the creation of the compose files to reference the services of the runkey manager stack.

### Starting everything automagically
The provided `docker.sh` script will build the dummy container and runkey manager, manage the runkey manager stack if the `HOST` is matching the one in the `runkeymanager/.env` file and manage the dummy services and the registrator.
```
HOST=[Prefix to '_compose.yaml' for dummies] RUNKEY=[runkey name] ./docker.sh ['up' | 'down' | 'restart']
```
`up` and `down` correspond to the `docker compose` comands. `restart` will first run `down` and then `up`

To run it on another host via ssh
```
ssh itkpixdaq@[OtherHost] "cd [path to ]/itk-demo-runkey/scale_test && HOST=[OtherHost] RUNKEY=[runkey] ./docker.sh ['up' | 'down']"
```

### Starting the Runkey Manager manually
Ideally run the whole runkey manager stack on the same maschine on which you called the `create_runkey.sh` script, otherwise you will have to find out yourself how to configure the communication correctly.
Make sure the `.env` file is in the `runkeymanager` directory and start the `runkeymanager/compose.yaml`:
```
docker compose -f runkeymanager/compose.yaml up -d 
```
