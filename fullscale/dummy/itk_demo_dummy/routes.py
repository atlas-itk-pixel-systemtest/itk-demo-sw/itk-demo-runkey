from fastapi import APIRouter, HTTPException
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from pyconfigdb import ConfigDB
from itk_demo_dummy.config import config
from random import randint
from runstats import Statistics
from time import sleep

import timeit

import logging
from rich.logging import RichHandler

logging.basicConfig(
    level=logging.INFO, format="%(message)s", datefmt=" ", handlers=[RichHandler()]
)
log = logging.getLogger("rich")


class ConfigureBody(BaseModel):
    tree: str


configdb = ConfigDB(config.configdb_url_key, config.sr_url)

router = APIRouter(prefix="/api")
dump_router = APIRouter()


class Stats:
    def __init__(self, name):
        self.name = name
        self.requests = 0
        self.errors = 0
        self.last_start = 0
        self.last_end = 0
        self.response_times = Statistics()
        self.request_times = Statistics()

    def __str__(self):
        amount = 1 if self.requests == 0 else self.requests
        return f"""{self.name}
    requests: {self.requests},
    errors: {self.errors},
    error rate: {self.errors/amount*100:.2f}%
    mean response time: ({1000*self.response_times.mean():.2f} pm {1000*self.response_times.stddev():.2f})ms
    mean time between requests: ({1000*self.request_times.mean():.2f} pm {1000*self.request_times.stddev():.2f})ms"""

    def start_request(self):
        self.last_start = timeit.default_timer()
        if self.last_end != 0:
            self.request_times.push(self.last_start - self.last_end)

    def end_requests(self, error=False):
        self.last_end = timeit.default_timer()
        self.response_times.push(self.last_end - self.last_start)
        self.requests += 1
        if error:
            self.errors += 1


felix_stats = Stats("felix")
frontend_stats = Stats("frontend")
optoboard_stats = Stats("optoboard")


@router.post(
    "/noop/",
    summary="does nothing",
)
async def noop(configuration: ConfigureBody):
    return {}


@router.post(
    "/optoboard/",
    summary="configure the dummy optoboard",
    description="load a tree from the configdb and wait for a random amount of time before returning. May randomly fail.",
    response_class=JSONResponse,
)
async def optoboard(configuration: ConfigureBody):
    optoboard_stats.start_request()
    try:
        time = await load(configuration)
    except HTTPException as e:
        optoboard_stats.end_requests(True)
        raise e
    else:
        optoboard_stats.end_requests()
    return time


@router.post(
    "/frontend/",
    summary="configure the dummy frontend",
    description="load a tree from the configdb and wait for a random amount of time before returning. May randomly fail.",
    response_class=JSONResponse,
)
async def frontend(configuration: ConfigureBody):
    frontend_stats.start_request()
    try:
        time = await load(configuration)
    except HTTPException as e:
        frontend_stats.end_requests(True)
        raise e
    else:
        frontend_stats.end_requests()
    return time


@router.post(
    "/felix/",
    summary="configure the dummy felix",
    description="load a tree from the configdb and wait for a random amount of time before returning. May randomly fail.",
    response_class=JSONResponse,
)
async def felix(configuration: ConfigureBody):
    felix_stats.start_request()
    try:
        time = await load(configuration)
    except HTTPException as e:
        felix_stats.end_requests(True)
        raise e
    else:
        felix_stats.end_requests()
    return time


async def load(configuration: ConfigureBody):
    depth = 1
    start_time = timeit.default_timer()
    tree = configdb.object_tree(configuration.tree, False, depth, True)
    end_time = timeit.default_timer()
    config_time = end_time - start_time
    print(tree["id"])
    sleep_timer = randint(100, 400) / 1000
    # sleep(sleep_timer)
    error = randint(1, 100)
    if error == 101:
        raise HTTPException(500, "Unexpected vacation. Server will be right back.")
    return {
        "sleep_time": sleep_timer,
        "configdb_time": config_time,
        "total_time": config_time,
    }  # + sleep_timer}


@router.get("/stats/", summary="get statistics on the dummy services")
async def stats():
    return {
        "felix": str(felix_stats),
        "frontend": str(frontend_stats),
        "optoboard": str(optoboard_stats),
    }


@router.get("/reset/", summary="reset statistics on the dummy services")
async def reset():
    global felix_stats, frontend_stats, optoboard_stats
    felix_stats = Stats("felix")
    frontend_stats = Stats("frontend")
    optoboard_stats = Stats("optoboard")


@router.get("/health/", summary="check if dummy is available")
async def health():
    return
