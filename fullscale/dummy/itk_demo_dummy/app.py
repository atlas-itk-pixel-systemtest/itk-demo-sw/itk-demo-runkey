from itk_demo_dummy.routes import router
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

def create_app():
    app = FastAPI(
        title="Dummy Servive API",
        description="Test Configdb access",
        version="2.2.0",
        docs_url="/api/docs",  # custom location for swagger UI
    )
    app.include_router(router)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],  # Allows all origins
        allow_credentials=True,
        allow_methods=["*"],  # Allows all methods
        allow_headers=["*"],  # Allows all headers
    )

    return app


# if __name__ == "__main__":
#     if len(sys.argv) > 1:
#         # arg, if any, is the desired port number
#         port = int(sys.argv[1])
#         assert port > 1024
#     else:
#         port = os.getuid()
#     app.debug = True
#     app.run("0.0.0.0", port)
