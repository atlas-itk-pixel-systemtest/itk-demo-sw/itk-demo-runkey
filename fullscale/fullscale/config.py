from node import Setup
from config_checker import BaseConfig, EnvSource
from typing import Optional

import logging
from rich.logging import RichHandler


class Config(BaseConfig):
    run_runkey_stack: bool = True
    run_dummy_stack: bool = True
    host: Optional[str] = None
    path_to_setups_dir: str = "../setups"
    setup: str
    docker_dir: Optional[str] = None
    celery_dir: Optional[str] = None
    stack: str = "Fullscale"
    log_level: str = logging._levelToName[logging.INFO]

    CONFIG_SOURCES = [EnvSource(allow_all=True, file="../.env")]

    def setup_dir(self):
      return f"{self.path_to_setups_dir}/{self.setup}"
    
    def as_env(self):
      return f"""RUN_RUNKEY_STACK={self.run_runkey_stack} RUN_DUMMY_STACK={self.run_dummy_stack} HOST={self.host} \
PATH_TO_SETUPS_DIR={self.path_to_setups_dir} SETUP={self.setup} DOCKER_DIR={self.docker_dir} CELERY_DIR={self.celery_dir} \
STACK={self.stack} LOG_LEVEL={self.log_level}"""


config = Config()
if config.host == "" or config.host == "None":
  config = config.model_copy(update={"host": None})

valid_log_level = True
try:
  log_level = logging._nameToLevel[config.log_level]
except KeyError:
  original_log_level = config.log_level
  valid_log_level = False
  log_level = logging.INFO

logging.basicConfig(level=log_level, format="%(message)s", handlers=[RichHandler(level=log_level, show_time=False)])
log = logging.getLogger("rich")
log.level = log_level
if not valid_log_level:
  log.error(f"{original_log_level} is not a valid log level. Use one of {logging._levelToName.keys()}")

if __name__ == "__main__":
    test_json = """\
    {
        "runkey": {"name": "test", "structure": {"name": "Top", "amount": [2], "children": [{"name": "Bottom", "amount": [4], "children": [], "endpoint": "bottom", "service": "b"}], "endpoint": "top", "service": null}},
        "hosts": [{"name": "h1", "service_amount": 2, "user": "test", "path_to_itk_demo_runkey": "a"}]
    }"""
    s = Setup.model_validate_json(test_json)
    print(s)

# tree_structure = Node(
#     "FELIX",
#     [400],
#     [
#         Node(
#             "Optoboard",
#             [8],
#             [Node("Frontend", [10], [], "frontend")],
#             "optoboard",
#         )
#     ],
#     "felix",
#     "itk-demo-dummy",
# )
# # hosts = [Host("daqdev9", 100), Host("daqdev10", 100), Host("daqdev11", 200)]
# hosts = [Host("daqdev11", 400)]
