from pydantic import BaseModel
from typing import List, Optional
    
def makedirs(path: str):
    import os
    if not os.path.exists(path):
        os.makedirs(path)
    elif os.path.isfile(path):
        raise Exception(f"Cannot create directory: {path} is a file")

class Node(BaseModel):
    name: str
    amount: int
    children: List["Node"]
    endpoint: str
    service: Optional[str] = None

    def __str__(self):
        children = ", ".join([""] + [str(child) for child in self.children])
        s = f"{self.name}({self.amount}{children})"
        return s
    
    def build_compose(self, hosts: list["Host"], services: dict[str, int], compose_path: str, runkey:str):
        for _ in range(self.amount):
            if self.service:
                count = services[self.service] + 1 if self.service in services else 1
                services[self.service] = count
                if hosts[0].service_amount == 0:
                    hosts.pop(0)
                host = hosts[0].name
                compose = f"""
  dummy{count}:
    image: gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-dummy/api:latest
    command: uvicorn asgi:app --port 5000 --host 0.0.0.0
    environment:
      - SR_URL=${{SR_URL}}
      - CONFIGDB_URL_KEY=${{CONFIGDB_URL_KEY:-demi/Fullscale/itk-demo-configdb/api/url}}
    ports:
      - :5000
    labels:
      - demi.Fullscale.{host}.itk-demo-dummy-{count}.api.host={host}
      - demi.Fullscale.{host}.itk-demo-dummy-{count}.api.category=Microservice
      - demi.Fullscale.{host}.itk-demo-dummy-{count}.api.url_prefix=/api
      - demi.Fullscale.{host}.itk-demo-dummy-{count}.api.url_postfix=/docs
      - demi.Fullscale.{host}.itk-demo-dummy-{count}.api.health=/health
      - demi.Fullscale.{host}.itk-demo-dummy-{count}.api.description=simple dummy service to test access the configdb
      - demi.Fullscale.{host}.itk-demo-dummy-{count}.api.protocol=http
      - demi.Fullscale.{host}.itk-demo-dummy-{count}.api.api_name=dummy {count}
"""
                with open(compose_path + f"{host}_{runkey}_compose.yaml", "+a") as f:
                    f.write(compose)
                hosts[0].service_amount -= 1
            for child in self.children:
                child.build_compose(hosts, services, compose_path, runkey)


    def build_runkey(self, hosts: list["Host"], current_service: str, services: dict[str, int], path: str):
        for i in range(self.amount):
            if self.service:
                count = services[self.service] + 1 if self.service in services else 1
                current_service = f"{self.service}-{count}"
                services[self.service] = count
                if hosts[0].service_amount == 0:
                    hosts.pop(0)
                hosts[0].service_amount -= 1
            host = hosts[0].name
            new_path = f"{path}{self.name}{i}/" 
            makedirs(new_path)
            meta = f"""
{{
	"configEndpoint": {{
		"key": "demi/Fullscale/{host}/{current_service}/api/url",
		"method": "post",
		"endpoint": "{self.endpoint}"
	}}
}}"""
            with open(new_path + "meta.json", "w") as f:
                f.write(meta)
            for child in self.children:
                child.build_runkey(hosts, current_service, services, new_path)
            
    def total_amount(self, current_count: int, only_services: bool):
        count = current_count
        if self.service or not only_services:
            count *= self.amount
        return count + sum(child.total_amount(count, only_services) for child in self.children if child.service or not only_services) 
            


class Host(BaseModel):
    name: str
    service_amount: int
    user: str
    path_to_itk_demo_runkey: str
    
class Runkey(BaseModel):
    name: str
    structure: Node

    def required_service_amount(self):
        services_required = self.structure.total_amount(1, True)
        return services_required
    
    def node_amount(self):
        nodes = self.structure.total_amount(1, False)
        return nodes

class Setup(BaseModel):
    runkey_manager_host: Host
    runkey: Runkey
    hosts: List[Host]
