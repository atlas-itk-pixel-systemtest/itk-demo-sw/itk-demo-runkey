from config import config, log, Config
from node import Setup, Host
import subprocess
import sys


def ssh_command(host: Host, config: Config, docker_cmd: str, capture_output=False):
    cmd = f'ssh {host.user}@{host.name} "cd {host.path_to_itk_demo_runkey}/fullscale/fullscale && {config.as_env()} ~/.local/bin/uv run docker.py {docker_cmd}"'
    log.debug(cmd)
    return subprocess.run(
        cmd, shell=True, check=True, text=True, capture_output=capture_output
    )


if __name__ == "__main__":
    docker_command: str = " ".join(sys.argv[1:])

    log.info(f"Reading setup from {config.setup_dir()}...")
    with open(config.setup_dir() + "/setup.json", "r") as f:
        setup = Setup.model_validate_json(f.read())

    if (config.host is None or config.host == setup.runkey_manager_host.name) and config.run_runkey_stack:
        log.info(
            f"Performing {docker_command} on runkey stack for host {setup.runkey_manager_host.name}..."
        )
        ssh_command(setup.runkey_manager_host, config.model_copy(update={"run_dummy_stack": False, "host": setup.runkey_manager_host.name}, deep=True), docker_command)

    hosts = setup.hosts
    if config.host is not None:
        hosts = [h for h in hosts if h.name == config.host]
    if not config.run_dummy_stack:
        hosts = []
    for host in hosts:
        log.info(
            f"Performing {docker_command} on dummy stack for host {host.name}..."
        )
        ssh_command(host, config.model_copy(update={"run_runkey_stack": False, "host": host.name}, deep=True), docker_command)
