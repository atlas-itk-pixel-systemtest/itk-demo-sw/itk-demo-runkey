from config import config, log
from node import Setup, makedirs
import subprocess
import shutil
import os


def build_runkey(setup: Setup, path: str, compose_path: str):
    service_amount = sum([host.service_amount for host in setup.hosts])
    services_required = setup.runkey.required_service_amount()
    if services_required > service_amount:
        log.error(
            f"Not enough services available on hosts to run the runkey {setup.runkey.name}:\n{service_amount} from {services_required} available"
        )
        exit(1)
    makedirs(path)
    makedirs(compose_path)
    for host in setup.hosts:
        with open(
            compose_path + f"{host.name}_{setup.runkey.name}_compose.yaml", "w"
        ) as f:
            f.write("")
        with open(
            compose_path + f"{host.name}_{setup.runkey.name}_compose.yaml", "+a"
        ) as f:
            f.write("""
networks:
  default:
    name: deminet
    external: true

services:
  registrator:
    image: gitlab-registry.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-registry/registrator:latest
    command: python3 itk_demo_registrator/registrator.py
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    extra_hosts:
      - "host.docker.internal:host-gateway"
      - "{host.name}:host-gateway"
    environment:
      - SR_URL=${SR_URL}
""")
    directories = sum(1 for _ in os.walk(path)) - 1
    required_nodes = setup.runkey.node_amount()
    if directories != required_nodes:
      shutil.rmtree(path)
      makedirs(path)
      setup.runkey.structure.build_runkey(setup.hosts.copy(), "", {}, path)
    else:
      log.info(f"Runkey '{setup.runkey.name}' already exists.")
    log.info("Writing compose files...")
    setup.runkey.structure.build_compose(setup.hosts.copy(), {}, compose_path, setup.runkey.name)


def run_setup(setup: Setup):
    runkey_path = f"../runkey/{setup.runkey.name}/"
    compose_path = f"{config.setup_dir()}/compose/"
    log.info(f"Building runkey '{setup.runkey.name}'...")
    build_runkey(setup, runkey_path, compose_path)
    log.info("Setting up environment")
    subprocess.run(
        f"{('DOCKER_DIR=' + config.docker_dir) if config.docker_dir is not None else ''} {('CELERY_DIR=' + config.celery_dir) if config.celery_dir is not None else ''} STACK={config.stack} RUNKEY={setup.runkey.name} HOST={setup.runkey_manager_host.name} ../config",
        shell=True,
        check=True,
        text=True,
    )
    shutil.move("./.env", f"{config.setup_dir()}/compose/.env")


if __name__ == "__main__":
    log.info(f"Reading setup from {config.setup_dir()}...")
    with open(config.setup_dir() + "/setup.json", "r") as f:
        setup_json = Setup.model_validate_json(f.read())
    run_setup(setup_json)
