from config import log, config
from node import Setup
import subprocess
import shutil
import sys
import os
from setup import run_setup


def run_command(destination, cmd, capture_output=False):
    cmd = f"cd {destination} && {cmd} && cd -"
    log.debug(cmd)
    return subprocess.run(
        cmd, shell=True, check=True, text=True, capture_output=capture_output
    )


if __name__ == "__main__":
    # Prerequisites
    if not config.host:
        log.error("Set the HOST variable to define on which host to operate")
        exit(1)
    docker_command: str = " ".join(sys.argv[1:])
    log.info(f"Reading setup from {config.setup_dir()}...")
    with open(config.setup_dir() + "/setup.json", "r") as f:
        setup = Setup.model_validate_json(f.read())
    if not os.path.exists(f"{config.setup_dir()}/compose/.env"):
        log.warning(
            f"Setup has not been created. Running 'setup.py' for {config.setup_dir()}..."
        )
        run_setup(setup)
        
    # Runkey stack
    if config.run_runkey_stack and config.host == setup.runkey_manager_host.name:
        log.info(f"Performing {docker_command} on runkey manager stack...")
        if docker_command.startswith("up"):
            log.info("Building runkey manager...")
            run_command("../../api/", "docker compose build")
            log.info("Loading environment...")
            shutil.copy2(f"{config.setup_dir()}/compose/.env", "../runkeymanager/.env")

        run_command("../runkeymanager", f"docker compose {docker_command}")

    # Dummy stack
    compose_file = f"{config.host}_{setup.runkey.name}_compose.yaml"
    compose_path = f"{config.setup_dir()}/compose/{compose_file}"
    if config.run_dummy_stack and os.path.exists(compose_path):
        log.info(f"Performing {docker_command} for dummy stack on {config.host}...")
        if docker_command.startswith("up"):
            log.info("Building dummy service...")
            run_command("../dummy", "docker compose build")

        run_command(
            f"{config.setup_dir()}/compose/",
            f"source ./.env  && docker compose -f {compose_file} {docker_command}",
        )
