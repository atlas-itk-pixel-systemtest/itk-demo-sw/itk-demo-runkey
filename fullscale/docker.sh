#!/usr/bin/env bash

EXECUTING_HOST=${HOST}
RUNKEY=${RUNKEY-localhost}
OPERATIONS=false

if [ "$1" == "up" ]; then
  OPERATIONS=("up -d")
elif [ "$1" == "upattach" ]; then
  OPERATIONS=("up")
elif [ "$1" == "down" ]; then
  OPERATIONS=("down")
elif [ "$1" == "restart" ]; then
  OPERATIONS=("down" "up -d")
else
  echo "Usage: $0 [up|down|restart]"
  exit 1
fi

for OPERATION in "${OPERATIONS[@]}"; do
if [ "$OPERATION" == "up -d" ]; then
  cd dummy
  docker compose build
  cd ..
fi
cd runkeymanager
source .env 
if [ "$HOST" == "$EXECUTING_HOST" ]; then
   cd ../../api 
   docker compose build
   cd -
   docker compose $OPERATION
fi
cd ..
source .env 
docker compose -f ${EXECUTING_HOST}_${RUNKEY}_compose.yaml $OPERATION
done