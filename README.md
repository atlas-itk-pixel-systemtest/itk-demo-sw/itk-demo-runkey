# itk-demo-runkey

This repo includes two containers and an example. The ui allows for viewing and editing of runkeys in the configdb and interaction with the api server. The api server functions as a runkey manager that can apply the configurations of one runkey to the entire system. The example shows hot to setup these containers and the containers they depend on.

## Runkey UI
For full functionality the runkey ui needs to communicate with one configdb and one runkey manager instance. In the intended use case their urls will be fetched from the service registry. All the necessary information for this can be specified with environment variables:
- `HOST`: the host on which the service registry is running
- `STACK`: used to generate the service registry keys used to fetch the configdb and runkey manager urls
- `DEFAULT_CONFIGDB_URL`: optional default url used for the configdb if none can be fetched from the service registry
- `DEFAULT_API_URL`: optional default url used for the runkey manager if none can be fetched from the service registry

## Runkey Manager
To function the runkey manager depends on the configdb to fetch runkeys, the celery middleware to distribute the configuration requests to the services and the service registry to fetch their urls. All the necessary information for this can be specified with environment variables:
- `HOST`: the host on which the service registry is running
- `STACK`: used to generate the service registry keys used to fetch the configdb and celery middleware urls