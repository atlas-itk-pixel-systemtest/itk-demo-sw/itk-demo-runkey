# Runkey example

An minimal example setup for the runkey microservice. 

## Setup
Firstly run 
```
./config
```
to create a `.env` file with the necessary configuratuin values
- `HOST`: hostname of your system
- `PUID`: uid of your user
- `PGID`: gid of your user
- `PTZ`: timezone of your system
- `STACK`: used to group the containers
- `DOCKER_DIR`: a directory where docker can store persistent data, the docker user needs to have read and write access to the directory.

All necessary directories and files in the `DOCKER_DIR` will be automatically created except for the files in the `celery` subdirectory. These files are setup dependend and need to be supplied externaly. In this example they are located in `docker/celery/`. The `config` script will set the `DOCKER_DIR` to this `docker` directory as a default value. Such that the files are available with the default configuration. The `DOCKER_DIR` also includes configurations for the runkey ui in the `mod_configs` subdirectory.

The docker compose file includes the following services:
- `registry-compose.yaml`
    - registry: service registry that keeps track of running services and their metadata
    - etcd: backend for the registry
    - dashboard: overview gui for the registry
- `celeryservices-compose.yaml`
    - flower: rabbitmq monitoring
    - rabbitmq: message queue for celery tasks
    - redis: broker for celery tasks
    - redis-commander: redis monitoring
- `topology-compose.yaml`
    - topology-server: celery middleware
    - dummy1, dummy2, dummy3: services to simulate system components
    - worker, worker1: executing celery tasks
- `configdb-compose.yaml`
    - configdb-api: runkey database
    - mariadb: configdb backend
    - phpmyadmin
    - pyconfigdb: python wrapper around configdb endpoints, used to import runkeys from disk
- `runkey-compose.yaml`
    - runkey-manager: state machine that configures the system with configuration data from a runkey
    - runkey-ui: configdb and runkey manager gui
If you don't need all of these services you can remove or comment them out of the docker compose files.

## Starting the containers
You can start all containers with
```
docker compose up -d
```
or just a specific file
```
docker compose up -f filename.yaml -d
```

## Accessing the runkey manager
Once the containers are running the runkey ui will be accessible at http://localhost:5222/ or can be found over the dashboard at http://localhost/
